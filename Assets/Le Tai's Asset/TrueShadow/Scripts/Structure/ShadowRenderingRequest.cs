using System;
using UnityEngine;
using UnityEngine.UI;

namespace LeTai.TrueShadow
{
public readonly struct ShadowRenderingRequest
{
    const int DIMENSIONS_HASH_STEP = 1;

    public readonly TrueShadow shadow;
    public readonly float      shadowSize;
    public readonly Vector2    shadowOffset;
    public readonly Vector2    dimensions;
    public readonly Rect       rect;

    readonly int hash;

    public ShadowRenderingRequest(TrueShadow shadow)
    {
        this.shadow = shadow;

        float casterScale;
        try
        {
            casterScale = shadow.Graphic.canvas.scaleFactor;
        }
        catch (NullReferenceException)
        {
            // ?. chain will bypass GameObject lifetime check. Try catch is cleaner than 2 if
            casterScale = 1f;
        }

        shadowSize   = shadow.Size;
        shadowOffset = shadow.Offset.Rotate(-shadow.RectTransform.eulerAngles.z);

        switch (shadow.Graphic)
        {
        case Text _:
            rect = new Rect(
                shadow.RectTransform.rect.position,
                shadow.SpriteMesh.bounds.size
            );
            break;
        default:
            rect = shadow.RectTransform.rect;
            break;
        }

        dimensions = rect.size * casterScale;

        // Tiled type cannot be batched by similar size
        int dimensionHash = shadow.Graphic is Image image && image.type == Image.Type.Tiled
                                ? dimensions.GetHashCode()
                                : HashUtils.CombineHashCodes(
                                    Mathf.CeilToInt(dimensions.x / DIMENSIONS_HASH_STEP) * DIMENSIONS_HASH_STEP,
                                    Mathf.CeilToInt(dimensions.y / DIMENSIONS_HASH_STEP) * DIMENSIONS_HASH_STEP
                                );

        hash = HashUtils.CombineHashCodes(
            Mathf.CeilToInt(shadowSize * 100),
            dimensionHash,
            shadow.ContentHash
        );
    }

    public override bool Equals(object obj)
    {
        if (obj == null) return false;

        return GetHashCode() == obj.GetHashCode();
    }

    public override int GetHashCode()
    {
        return hash;
    }
}
}
