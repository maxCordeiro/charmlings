﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using static GameManager;

public class BattleLogViewer : EditorWindow
{
    public List<string> eLog = new List<string>();
    bool writing;

    BattleUI bController;

    Vector2 scrollPos;

    [MenuItem("Charmlings/Battle Log Viewer")]
    public static void ShowWindow()
    {
        GetWindow(typeof(BattleLogViewer));
    }

    void OnGUI()
    {
        if (!EditorApplication.isPlaying)
        {
            GUILayout.Label("Editor not active");
            return;
        }

        if (!gm.inBattle)
        {
            GUILayout.Label("Not in battle.");
            return;
        }

        bController = FindObjectOfType<BattleUI>();

        if (bController.log.Count <= 0)
        {
            GUILayout.Label("No messages logged.");
            return;
        }

        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, true, true);

        for (int i = 0; i < bController.log.Count; i++)
        {
            EditorGUILayout.LabelField(bController.log[i]);
        }

        EditorGUILayout.EndScrollView();

        if (GUILayout.Button("Log to txt"))
        {
            StreamWriter txt;
            string path = Application.persistentDataPath + "/BATTLE LOG " + string.Format("[{0}-{1}-{2}] [{3}-{4}-{5}]", DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second) + ".txt";
            Debug.Log(path);
            txt = File.CreateText(path);


            for (int i = 0; i < bController.log.Count; i++)
            {
                txt.WriteLine(bController.log[i]);
            }

            txt.Close();
        }

        eLog = bController.log;
    }


    public void OnInspectorUpdate()
    {
        // This will only get called 10 times per second.
        Repaint();
    }

}