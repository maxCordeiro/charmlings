﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor
{
    public string currTime;

    public override void OnInspectorGUI()
    {
        if (EditorApplication.isPlaying)
        {
            GameManager gm = (GameManager)target;

            //EditorGUILayout.LabelField(gm.PrintDate());
            //EditorGUILayout.LabelField(gm.PrintTime());
            //EditorGUILayout.LabelField(SaveSystem.loaded.game.inGameTime.ToString());
            Repaint();
        }
        DrawDefaultInspector();

    }
}