﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEditor;

public class SpriteSlicer
{
    [MenuItem("Assets/Slice Spritesheet/Battle", priority = 5)]
    public static void BattleSprites()
    {
        if (Selection.activeObject is Texture2D)
        {
            var textures = Selection.GetFiltered<Texture2D>(SelectionMode.Assets);

            foreach (var texture in textures)
            {
                ProcessTexture(texture, 1, 4, 96, 0.5f);
            }
        }
    }

    [MenuItem("Assets/Slice Spritesheet/Overworld", priority = 5)]
    public static void OverworldSprites()
    {
        if (Selection.activeObject is Texture2D)
        {
            var textures = Selection.GetFiltered<Texture2D>(SelectionMode.Assets);

            foreach (var texture in textures)
            {
                ProcessTexture(texture, 4, 4, 48, 0.333333343267441f);
            }
        }
    }

    static void ProcessTexture(Texture2D texture, int rowCount, int colCount, int spriteSize, float pivot)
    {
        List<SpriteMetaData> monsterSprites = new List<SpriteMetaData>();

        string path = AssetDatabase.GetAssetPath(texture);
        var importer = AssetImporter.GetAtPath(path) as TextureImporter;

        importer.spriteImportMode = SpriteImportMode.Multiple;

        Vector2 piv = Vector2.zero;
        piv.x = pivot;

        string tName = Selection.activeObject.name;

        for (int r = 0; r < rowCount; ++r)
        {
            for (int c = 0; c < colCount; ++c)
            {
                SpriteMetaData spr = new SpriteMetaData();
                spr.rect = new Rect(c * spriteSize, r * spriteSize, spriteSize, spriteSize);
                spr.name = tName + "_" + ((r * rowCount) + c);
                spr.alignment = 9;
                spr.pivot = piv;
                monsterSprites.Add(spr);
            }
        }

        importer.spritesheet = monsterSprites.ToArray();

        AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
    }
}