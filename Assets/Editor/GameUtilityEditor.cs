using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GameUtilityEditor : EditorWindow
{
    int category;
    string[] categoryNames = { "Charmlings", "Time", "Items", "Utility" };

    Vector2 scrollPos;

    // Charmlings
    CharmlingID g_mon;
    CharmlingID g_prevMon;
    int g_prevForm;
    int g_form;

    bool g_overrideColor;
    ColorID g_prevColor;
    ColorID g_color;


    bool g_overridePerk;
    PerkID g_perk;

    bool g_overrideTrait;
    TraitID g_trait;

    int g_level;

    bool g_overrideStat;
    int[] g_stats = new int[6];
    int[] g_wp = new int[6];
    bool g_setHP;
    int g_currHP;
    int g_affection;

    string g_nickname;

    GenderID g_gender;
    StatusID g_status = StatusID.NONE;

    bool g_overrideAccessory;
    ItemID g_accessory = ItemID.NONE;

    bool g_overrideCharm;
    ItemID g_charm = ItemID.NEUTRALCHARM;

    bool g_overrideSkills;
    SkillID[] g_skills = new SkillID[5];

    bool g_overrideExSkill;
    SkillID g_exSkill;
    
    bool g_overrideOT;
    string g_tamer;
    string g_tamerID;
    MapID g_obtainedMap = MapID.NONE;
    int g_obtainedLevel = 0;
    GameDate g_obtainedDate;

    Texture2D charmlingSprite;
    Sprite sprite;
    Sprite spriteNone;

    bool g_addToBag;

    // Time

    float timeScale;
    int fps;

    // Items
    ItemID i_id = ItemID.NONE;
    int i_quantity;

    int i_fillQuantity;
    // Utility

    int u_money;


    [MenuItem("Charmlings/Game Utility Editor")]
    public static void ShowWindow()
    {
        GetWindow(typeof(GameUtilityEditor));
    }

    [MenuItem("Charmlings/Reload")]
    public static void Paint()
    {
        EditorWindow view = GetWindow<SceneView>();
        view.Repaint();
    }

    private void OnGUI()
    {
        if (!EditorApplication.isPlaying)
        {
            GUILayout.Label("Editor not in playtest mode.");
            return;
        }

        GUIStyle styleBold = new GUIStyle
        {
            fontStyle = FontStyle.Bold,
            fontSize = 12
        };
        styleBold.normal.textColor = Color.white;

        GUIStyle styleCenter = new GUIStyle()
        {
            alignment = TextAnchor.MiddleCenter
        };

        GUILayout.BeginHorizontal();
        for (int i = 0; i < categoryNames.Length; i++)
        {
            if (GUILayout.Button(categoryNames[i]))
            {
                Repaint();
                category = i;
            }
        }
        GUILayout.EndHorizontal();

        EditorGUILayout.BeginVertical();
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

        switch (category)
        {
            // Charmlings
            case 0:
                if (spriteNone == null)
                {
                    spriteNone = Resources.Load<Sprite>("Charmlings/Battle/NONE");
                }

                if (g_mon != g_prevMon || g_form != g_prevForm || g_prevColor != g_color)
                {
                    Sprite[] cSprite = Resources.LoadAll<Sprite>($"Charmlings/Battle/{g_mon}_{g_form}");

                    if (cSprite.Length > 1)
                    {
                        sprite = cSprite[g_color == ColorID.GENERATE? 0 : (int)g_color];
                    } else
                    {
                        sprite = null;
                    }
                }

                if (sprite != null || spriteNone != null)
                {
                    var texture = AssetPreview.GetAssetPreview(sprite);
                    var textureNone = AssetPreview.GetAssetPreview(spriteNone);

                    GUILayout.Label(sprite != null? texture : textureNone, styleCenter);
                }

                // IDENTITY
                EditorGUILayout.LabelField("Identity", styleBold);

                g_prevMon = g_mon;
                g_mon = (CharmlingID)EditorGUILayout.EnumPopup(g_mon);

                g_prevForm = g_form;
                g_form = EditorGUILayout.IntField("Form", g_form);

                g_prevColor = g_color;
                g_overrideColor = EditorGUILayout.Toggle("Override Color?", g_overrideColor);
                if (g_overrideColor)
                {
                    g_color = (ColorID)EditorGUILayout.EnumPopup("Color", g_color);
                }

                g_overridePerk = EditorGUILayout.Toggle("Override Perk?", g_overridePerk);
                if (g_overridePerk)
                {
                    g_perk = (PerkID)EditorGUILayout.EnumPopup(g_perk);
                
                }
                g_overrideTrait = EditorGUILayout.Toggle("Override Trait?", g_overrideTrait);
                if (g_overrideTrait)
                {
                    g_trait = (TraitID)EditorGUILayout.EnumPopup(g_trait);
                }

                g_nickname = EditorGUILayout.TextField("Nickname", g_nickname);

                // STATS
                EditorGUILayout.Space(20);
                EditorGUILayout.LabelField("Stats", styleBold);

                g_level = EditorGUILayout.IntSlider("Level", g_level, 1, 100);

                g_overrideStat = EditorGUILayout.Toggle("Override Stats?", g_overrideStat);
                if (g_overrideStat)
                {
                    EditorGUILayout.LabelField("Stat Values");
                    for (int i = 0; i < g_stats.Length; i++)
                    {
                        g_stats[i] = EditorGUILayout.IntField(((StatID)i).ToString(), g_stats[i]);
                    }
                }

                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Wonder Points");
                for (int i = 0; i < g_wp.Length; i++)
                {
                    g_wp[i] = EditorGUILayout.IntSlider(((StatID)i).ToString(), g_wp[i], 0, 250);
                }

                // PROPERTIES
                EditorGUILayout.Space(20);
                EditorGUILayout.LabelField("Properties", styleBold);

                g_overrideCharm = EditorGUILayout.Toggle("Override Charm?", g_overrideCharm);
                if (g_overrideCharm)
                {
                    g_charm = (ItemID)EditorGUILayout.EnumPopup("Item", g_charm);
                }

                g_gender = (GenderID)EditorGUILayout.EnumPopup("Gender", g_gender);
                g_status = (StatusID)EditorGUILayout.EnumPopup("Status", g_status);
                g_affection = EditorGUILayout.IntSlider("Affection", g_affection, 0, 100);

                g_setHP = EditorGUILayout.Toggle("Set Current HP?", g_setHP);
                if (g_setHP)
                {
                    g_currHP = EditorGUILayout.IntSlider(g_currHP, 0, 50);
                }

                g_overrideAccessory = EditorGUILayout.Toggle("Override Accessory?", g_overrideAccessory);
                if (g_overrideAccessory)
                {
                    g_accessory = (ItemID)EditorGUILayout.EnumPopup("Accessory", g_accessory);
                }

                EditorGUILayout.Space(20);
                EditorGUILayout.LabelField("Skills", styleBold);

                g_overrideSkills = EditorGUILayout.Toggle("Override Skills?", g_overrideSkills);
                if (g_overrideSkills)
                {
                    for (int i = 0; i < g_skills.Length; i++)
                    {
                        g_skills[i] = (SkillID)EditorGUILayout.EnumPopup($"Skill ({i})", g_skills[i]);
                    }
                }

                g_overrideExSkill = EditorGUILayout.Toggle("Override EX Skill?", g_overrideExSkill);
                if (g_overrideExSkill)
                {
                    g_exSkill = (SkillID)EditorGUILayout.EnumPopup("EX Skill", g_exSkill);
                }

                EditorGUILayout.Space(20);
                EditorGUILayout.LabelField("Original Tamer", styleBold);

                g_overrideOT = EditorGUILayout.Toggle("Override OT?", g_overrideOT);
                if (g_overrideOT)
                {
                    g_tamer = EditorGUILayout.TextField("Tamer", g_tamer);
                    g_tamerID = EditorGUILayout.TextField("Tamer ID", g_tamerID);

                    g_obtainedMap = (MapID)EditorGUILayout.EnumPopup("Obtained Map", g_obtainedMap);
                    g_obtainedLevel = EditorGUILayout.IntSlider("Obtained Level", g_obtainedLevel, 1, 100);
                }

                EditorGUILayout.Space(20);
                EditorGUILayout.LabelField("Other", styleBold);

                g_addToBag = EditorGUILayout.Toggle("Add To Bag?", g_addToBag);
               
                if (GUILayout.Button("Add Charmling"))
                {
                    AddCharmling(false);
                }

                if (GUILayout.Button("Fill Bag"))
                {
                    for (int i = 0; i < SaveSystem.loaded.player.bagCharmlings.Length; i++)
                    {
                        AddCharmling(true);
                    }
                }

                EditorGUILayout.Space(20);

                for (int i = 0; i < SaveSystem.loaded.player.invGeneral.Count; i++)
                {
                    EditorGUILayout.LabelField($"ITEM: {SaveSystem.loaded.player.invGeneral[i].ID}" + (SaveSystem.loaded.player.invGeneral[i].isSelected ? "- [X]" : ""));
                }

                break;
            case 1:
                timeScale = EditorGUILayout.Slider(timeScale, 0, 1);

                if (GUILayout.Button("Apply TimeScale"))
                {
                    Time.timeScale = timeScale;
                }

                EditorGUILayout.Space(20);

                fps = EditorGUILayout.IntSlider(fps, 1, 144);

                if (GUILayout.Button("Apply FPS"))
                {
                    QualitySettings.vSyncCount = 0;
                    Application.targetFrameRate = fps;
                }
                break;
            case 2:
                EditorGUILayout.Space(20);
                EditorGUILayout.LabelField("Items", styleBold);

                i_id = (ItemID)EditorGUILayout.EnumPopup("Item ID", i_id);

                i_quantity = EditorGUILayout.IntSlider("Quantity", i_quantity, 1, 999);

                if (GUILayout.Button("Add Button"))
                {
                    ItemUtil.ObtainItem(i_id, i_quantity);
                }

                EditorGUILayout.Space(20);
                EditorGUILayout.LabelField("Item Utility", styleBold);

                i_fillQuantity = EditorGUILayout.IntSlider("Quantity", i_fillQuantity, 1, 999);

                if (GUILayout.Button("Fill Bag"))
                {
                    SaveSystem.loaded.player.invGeneral = new List<Item>();
                    SaveSystem.loaded.player.invHealing = new List<Item>();
                    SaveSystem.loaded.player.invBattle = new List<Item>();
                    SaveSystem.loaded.player.invTreasures = new List<Item>();
                    SaveSystem.loaded.player.invMaterials = new List<Item>();
                    SaveSystem.loaded.player.invCapsules = new List<Item>();
                    SaveSystem.loaded.player.invCharms = new List<Item>();
                    SaveSystem.loaded.player.invKeyItems = new List<Item>();

                    for (int i = 0; i < GameManager.gm.db.dbItem.Count; i++)
                    {
                        ItemUtil.ObtainItem(GameManager.gm.db.dbItem[i].ID, i_fillQuantity, 0);
                    }
                }

                break;

            case 3:
                EditorGUILayout.Space(20);
                EditorGUILayout.LabelField("Currency", styleBold);

                u_money = EditorGUILayout.IntField("Amount", u_money);

                if (GUILayout.Button("Add Gold"))
                {
                    MoneyUtil.AddMoney(u_money);
                } else if (GUILayout.Button("Subtract Gold"))
                {
                    MoneyUtil.AddMoney(-u_money);
                }

                EditorGUILayout.Space(20);
                EditorGUILayout.LabelField("UI", styleBold);
                if (GUILayout.Button("Print World Position"))
                {
                    if (Selection.activeTransform != null)
                    {
                        RectTransform rt = Selection.activeTransform.GetComponent<RectTransform>();

                        Debug.Log(rt == null ? "No RectTransform found." : $"({rt.position.x}f, {rt.position.y}f)");
                    }
                }

                break;
            default:
                break;
        }


        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndVertical();
    }

    public void AddCharmling(bool fillBag)
    {
        Charmling g_Charmling = new Charmling(new CharmlingIdentifier(g_mon, g_form, g_overrideColor ? g_color : ColorID.GENERATE));

        if (g_overridePerk) g_Charmling.perk = g_perk;
        if (g_overrideTrait) g_Charmling.trait = new Trait() { ID = g_trait };
        g_Charmling.level = g_level;

        if (g_overrideStat) g_Charmling.stats = (int[])g_stats.Clone();
        g_Charmling.wonderPoints = (int[])g_wp.Clone();

        if (g_setHP) g_Charmling.currentHP = g_currHP;

        g_Charmling.nickname = g_nickname != null ? g_nickname : "";

        g_Charmling.gender = g_gender;
        g_Charmling.status = g_status;

        if (g_overrideAccessory)
        {
            g_Charmling.accessory = new Item(g_accessory);
        } else
        {
            g_Charmling.accessory = null;
        }
        if (g_overrideCharm) g_Charmling.charm = new Item(g_charm);

        if (g_overrideSkills) g_Charmling.skills = new Skill[]{
                        new Skill(g_skills[0]),
                        new Skill(g_skills[1]),
                        new Skill(g_skills[2]),
                        new Skill(g_skills[3]),
                        new Skill(g_skills[4]),
                    };
        if (g_overrideExSkill) g_Charmling.exSkill = new Skill(g_exSkill);

        if (g_overrideOT)
        {
            g_Charmling.tamerName = g_tamer;
            g_Charmling.tamerID = g_tamerID;

            g_Charmling.obtainedMap = g_obtainedMap;
            g_Charmling.obtainedLevel = g_obtainedLevel;

            g_Charmling.obtainedDate = new GameDate();
        }

        if (!g_overrideStat) g_Charmling.CalculateAllStats(g_setHP ? false : true);

        CharmlingUtil.ObtainCharmling(g_Charmling, fillBag || g_addToBag);
    }

    void OnInspectorUpdate()
    {
        // Call Repaint on OnInspectorUpdate as it repaints the windows
        // less times as if it was OnGUI/Update
        Repaint();
    }
}
