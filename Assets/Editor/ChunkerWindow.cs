using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

public class ChunkerWindow : EditorWindow
{
	public string default_map_directory = "Assets" + Path.DirectorySeparatorChar.ToString() + "Maps" + Path.DirectorySeparatorChar.ToString() + "Chunks" + Path.DirectorySeparatorChar.ToString();
	public string default_tileset_directory = "Assets" + Path.DirectorySeparatorChar.ToString() + "Maps" + Path.DirectorySeparatorChar.ToString() + "";
	public Vector2Int chunk_size = new Vector2Int(16, 16);
	
	public struct object_info
	{
		public int object_group;
		public int line;
		public Vector2Int pos;
	}
	private Vector2Int map_size;
	private Vector2Int tile_size;
	private List<int> layers = new List<int>();
	private List<int> object_groups = new List<int>();
	private List<object_info> objects = new List<object_info>();
	private List<string> tileset_directories = new List<string>();
	private int index = 0;
	
	private List<string> file_text = new List<string>();
	private bool running = false;
	private int pos = 0;
	private int state = 0;
	
	/*
	state descriptions
	0: do nothing
	1: figure out where everything is
	2: writing tilesets
	3: writing layers
	4: writing object groups
	*/
	
	private Object map_file_object;
	private string map_path = "";
	private string file_name = "";
	private string target_directory = "";
	private string chunk_path = "";
	private bool is_valid_map = false;
	private StreamReader map_file;
	private StreamWriter chunk_file;
	private Vector2Int chunk_idx = new Vector2Int();
	private Vector2Int num_chunks = new Vector2Int();

	[MenuItem("Charmlings/Map Chunker")]
	public static void ShowWindow()
	{
		GetWindow<ChunkerWindow>("Map Chunker");
	}
	
	//UI code, mostly ingnorable
    void OnGUI()
	{
		{
			Object val = EditorGUILayout.ObjectField("Target Map", map_file_object, typeof(Object), false);
			map_file_object = !running ? val : map_file_object;
		}
		//input validation
		if(GUI.changed)
		{
			tileset_directories.Clear();
			map_path = AssetDatabase.GetAssetPath(map_file_object);
			is_valid_map = false;
			map_size = new Vector2Int();
			tile_size = new Vector2Int();
			if(Path.GetExtension(map_path) == ".tmx")
			{
				map_file = File.OpenText(map_path);
				
				//verify that the file is formatted correctly
				string map_info_text = "";
				if(map_file.ReadLine() == "<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
				{
					map_info_text = map_file.ReadLine();
					is_valid_map = map_info_text.StartsWith("<map ");
				}
				
				if(!is_valid_map)
				{
					map_file_object = null;
					map_file = null;
					return;
				}
				//automatically set target_directory
				file_name = Path.GetFileNameWithoutExtension(map_path);
				target_directory = default_map_directory;
				
				//set map_size and tile_size values
				int idx = 0;
				
				idx = map_info_text.IndexOf(" width=\"") + 8;
				map_size.x = int.Parse(map_info_text.Substring(idx, map_info_text.IndexOf("\"", idx) - idx));
				
				idx = map_info_text.IndexOf(" height=\"") + 9;
				map_size.y = int.Parse(map_info_text.Substring(idx, map_info_text.IndexOf("\"", idx) - idx));
				
				idx = map_info_text.IndexOf(" tilewidth=\"") + 12;
				tile_size.x = int.Parse(map_info_text.Substring(idx, map_info_text.IndexOf("\"", idx) - idx));
				
				idx = map_info_text.IndexOf(" tileheight=\"") + 13;
				tile_size.y = int.Parse(map_info_text.Substring(idx, map_info_text.IndexOf("\"", idx) - idx));
			}
			else
			{
				map_file_object = null;
				map_file = null;
			}
			
			while(map_file.ReadLine().StartsWith(" <tileset "))
			{
				tileset_directories.Add(default_tileset_directory);
			}
			
			map_file.Close();
			
			return;
		}
		
		{
			string val = EditorGUILayout.TextField("Target Directory", target_directory);
			target_directory = !running ? val : target_directory;
		}
		if(!Directory.Exists(target_directory) && target_directory != "")
		{
			GUILayout.Label("This directory doesn't exist, would you like to create it?");
			if(GUILayout.Button("Make Directory"))
			{
				Directory.CreateDirectory(target_directory);
			}
			return;
		}
		
		//if the file isn't a tmx file or is formatted incorrectly 
		if(!map_file_object || !is_valid_map)
		{
			GUILayout.Label("Please select a Tiled \".tmx\" file");
			return;
		}
		
		EditorGUILayout.Space();
		GUILayout.Label("Width: " + map_size.x.ToString() + "\nHeight: " + map_size.y.ToString());
		EditorGUILayout.Space();
		
		{
			bool directories_exist = true;
			for(int i = 0; i < tileset_directories.Count; i++)
			{
				string val = EditorGUILayout.TextField("Tileset " + (i + 1).ToString() + " Directory", tileset_directories[i]);
				tileset_directories[i] = !running ? val : tileset_directories[i];
				
				if(!Directory.Exists(tileset_directories[i]) && tileset_directories[i] != "")
				{
					GUILayout.Label("This directory doesn't exist, would you like to create it?");
					directories_exist = false;
					if(GUILayout.Button("Make Directory"))
					{
						Directory.CreateDirectory(tileset_directories[i]);
					}
				}
			}
			if(!directories_exist)
			{
				return;
			}
			EditorGUILayout.Space();
		}
		
		{
			Vector2Int val = EditorGUILayout.Vector2IntField("Chunk Size", chunk_size);
			chunk_size = !running ? val : chunk_size;
		}
		//input validation
		if(GUI.changed)
		{
			Mathf.Clamp(chunk_size.x, 1, map_size.x);
			Mathf.Clamp(chunk_size.y, 1, map_size.y);
		}
		num_chunks = new Vector2Int(Mathf.CeilToInt(map_size.x/(float)chunk_size.x), Mathf.CeilToInt(map_size.y/(float)chunk_size.y));
		EditorGUILayout.Space();
		GUILayout.Label("You will end up with " + (num_chunks.x * num_chunks.y).ToString() + " chunks." );
		EditorGUILayout.Space();
		
		if(!running)
		{
			if(GUILayout.Button("Start"))
			{
				running = true;
				state = 1;
				map_file = File.OpenText(map_path);
			}
		}
		else
		{
			GUILayout.Label("Running...");
		}
	}
	
	void Update()
    {
		if(running)
		{
			switch(state)
			{
				case 1:
				{
					string line = map_file.ReadLine();
					
					if(line.StartsWith(" <layer "))
					{
						layers.Add(pos);
					}
					
					if(line.StartsWith(" <objectgroup "))
					{
						object_groups.Add(pos);
					}
					
					if(line.StartsWith("  <object "))
					{
						int col = line.IndexOf(" x=\"") + 4;
						int x = int.Parse(line.Substring(col, line.IndexOf("\"", col) - col));
						col = line.IndexOf(" y=\"") + 4;
						int y = int.Parse(line.Substring(col, line.IndexOf("\"", col) - col));
						objects.Add(new object_info{
							object_group = object_groups.Count - 1,
							line = pos, 
							pos = new Vector2Int(x, y)
							});
					}
					
					file_text.Add(line);
					
					if(map_file.EndOfStream)
					{
						state = 2;
						pos = 0;
						map_file.Close();
						string s = target_directory;
						if(!target_directory.EndsWith(Path.DirectorySeparatorChar.ToString()))
						{
							s += Path.DirectorySeparatorChar;
						}
						
						chunk_idx = new Vector2Int();
						chunk_path = $"{s}{file_name}_{chunk_idx.x.ToString()}_{chunk_idx.y.ToString()}.tmx";
						if(File.Exists(chunk_path))
						{
							File.Delete(chunk_path);
						}
						chunk_file = File.CreateText(chunk_path);
						break;
					}
					pos++;
				}
				break;
				case 2:
				{
					int w = chunk_size.x;
					int h = chunk_size.y;
					if(chunk_idx.x == num_chunks.x - 1)
					{
						w = map_size.x - (chunk_idx.x * chunk_size.x);
					}
					if(chunk_idx.y == num_chunks.y - 1)
					{
						h = map_size.y - (chunk_idx.y * chunk_size.y);
					}
					chunk_file.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
					chunk_file.WriteLine(file_text[1].Substring(0, file_text[1].IndexOf(" width=\"") + 8)
						+w.ToString() + "\" height=\"" + h.ToString() + "\""
						+file_text[1].Substring(file_text[1].IndexOf(" tilewidth=\"")));
					
					for(int i = 0; i < tileset_directories.Count; i++)
					{
						chunk_file.WriteLine(file_text[i+2].Insert(file_text[i+2].IndexOf(" source=\"") + 9, GetRelativePath(target_directory,tileset_directories[i]).Replace('/', '\\')));
					}
					state = 3;
				}
				break;
				case 3:
				{
					int w = chunk_size.x;
					int h = chunk_size.y;
					if(chunk_idx.x == num_chunks.x - 1)
					{
						w = map_size.x - (chunk_idx.x * chunk_size.x);
					}
					if(chunk_idx.y == num_chunks.y - 1)
					{
						h = map_size.y - (chunk_idx.y * chunk_size.y);
					}
					int ln = layers[index];
					//Debug.Log($"\nChunk: {chunk_idx.x.ToString()}, {chunk_idx.y.ToString()}\nLayer: {index.ToString()}\n{file_text[ln]}");
					chunk_file.WriteLine(file_text[ln].Substring(0, file_text[ln].IndexOf(" width=\"") + 8)
						+ w.ToString() + "\" height=\"" + h.ToString()
						+ file_text[ln].Substring(file_text[ln].IndexOf("\"", file_text[ln].IndexOf(" height=\"") + 9)));
					
					do
					{
						chunk_file.WriteLine(file_text[++ln]);
					}
					while(!file_text[ln].StartsWith("  <data "));
					
					
					ln += 1 + (chunk_idx.y * chunk_size.y);
					for(int y = 0; y < h; y++)
					{
						int col = chunk_size.x * chunk_idx.x;
						string[] splits = file_text[ln].Split(',');
						string chunk_info = "";
						for(int x = 0; x < w; x++)
						{
							chunk_info += splits[col] + ',';
							col++;
						}
						
						if(y == h - 1 && chunk_info.EndsWith(","))
						{
							chunk_info = chunk_info.Remove(chunk_info.Length - 1);
						}
						chunk_file.WriteLine(chunk_info);
						ln++;
					}
					chunk_file.WriteLine("</data>");
					chunk_file.WriteLine(" </layer>");
					
					index ++;
					if(index == layers.Count)
					{
						state = 4;
						index = 0;
					}
				}
				break;
				case 4:
				{
					int ln = object_groups[index];
					//Debug.Log($"\nChunk: {chunk_idx.x.ToString()}, {chunk_idx.y.ToString()}\nObject Group: {index.ToString()}\n{file_text[ln]}");
					
					while(!file_text[ln].StartsWith("  <object ") && !file_text[ln].StartsWith(" </objectgroup>"))
					{
						chunk_file.WriteLine(file_text[ln++]);
					}
					
					for(int i = 0; i < objects.Count; i++)
					{
						if(objects[i].object_group == index)
						{
							int x_d = (objects[i].pos.x) - (chunk_idx.x * chunk_size.x * tile_size.x);
							int y_d = (objects[i].pos.y) - (chunk_idx.y * chunk_size.y * tile_size.y);
							
							if(x_d >= 0 && (x_d < chunk_size.x * tile_size.x || chunk_idx.x == num_chunks.x - 1))
							{
								if(y_d >= 0 && (y_d < chunk_size.y * tile_size.y || chunk_idx.y == num_chunks.y - 1))
								{
									chunk_file.WriteLine(file_text[objects[i].line].Substring(0, file_text[objects[i].line].IndexOf(" x=\"") + 4)
										+ x_d.ToString() + "\" y=\"" + y_d.ToString()
										+ file_text[ln].Substring(file_text[ln].IndexOf("\"", file_text[ln].IndexOf(" y=\"") + 4)));
									
									if(!file_text[objects[i].line + 1].StartsWith("  <object "))
									{
										ln = objects[i].line + 1;
										while(!file_text[ln].StartsWith("  <object ") && !file_text[ln].StartsWith(" </objectgroup>"))
										{
											chunk_file.WriteLine(file_text[ln++]);
										}
									}
								}
							}
						}
						if(objects[i].object_group > index)
						{
							break;
						}
					}
					chunk_file.WriteLine(" </objectgroup>");
					
					index ++;
					if(index == object_groups.Count)
					{
						state = 5;
						index = 0;
					}
				}
				break;
				case 5:
				{
					chunk_idx.x++;
					chunk_file.WriteLine("</map>");
					if(chunk_idx.x == num_chunks.x)
					{
						chunk_idx.x = 0;
						chunk_idx.y++;
						if(chunk_idx.y == num_chunks.y)
						{
							chunk_idx.y = 0;
							state = 0;
							running = false;
							layers.Clear();
							object_groups.Clear();
							file_text.Clear();
							chunk_file.Close();
							Repaint();
							break;
						}
					}
					chunk_file.Close();
					
					string s = target_directory;
					if(!target_directory.EndsWith(Path.DirectorySeparatorChar.ToString()))
					{
						s += Path.DirectorySeparatorChar;
					}
					chunk_path = $"{s}{file_name}_{chunk_idx.x.ToString()}_{chunk_idx.y.ToString()}.tmx";
					if(File.Exists(chunk_path))
					{
						File.Delete(chunk_path);
					}
					chunk_file = File.CreateText(chunk_path);
					state = 2;
				}
				break;
				default:
				break;
			}
		}
    }
	
	public void OnDestroy()
    {
		map_file_object = null;
		map_file = null;
    }
	
	//if these are broken, tough shit I guess, I didn't write it
	public int StreamReaderPosition(StreamReader s)
	{
		System.Int32 charpos = (System.Int32) s.GetType().InvokeMember("charPos", 
		BindingFlags.DeclaredOnly | 
		BindingFlags.Public | BindingFlags.NonPublic | 
		BindingFlags.Instance | BindingFlags.GetField
		 ,null, s, null); 

		System.Int32 charlen= (System.Int32) s.GetType().InvokeMember("charLen", 
		BindingFlags.DeclaredOnly | 
		BindingFlags.Public | BindingFlags.NonPublic | 
		BindingFlags.Instance | BindingFlags.GetField
		 ,null, s, null);

		return (System.Int32)s.BaseStream.Position - charlen + charpos;
	}
	
	public string GetRelativePath(string relativeTo, string path)
	{
		if(!path.EndsWith(Path.DirectorySeparatorChar.ToString()))
		{
			path += Path.DirectorySeparatorChar;
		}
		if(!relativeTo.EndsWith(Path.DirectorySeparatorChar.ToString()))
		{
			relativeTo += Path.DirectorySeparatorChar;
		}
		System.Uri file = new System.Uri(Path.GetFullPath(relativeTo));
		// Must end in a slash to indicate folder
		System.Uri folder = new System.Uri(Path.GetFullPath(path));
		//string relativePath = 
		return System.Uri.UnescapeDataString(
			file.MakeRelativeUri(folder)
				.ToString()
			);
	}
}
