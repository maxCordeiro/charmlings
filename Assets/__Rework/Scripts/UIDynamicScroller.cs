using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//TODO add a required component for a vertical layout
public class UIDynamicScroller : MonoBehaviour
{
    public int maxVisible;
    public int numVisible;
    public int count;
    public int selectedSlot;
    public int offset;

    //change this depending on the menu
    public GameObject slotPrefab;
    
    public void Refresh(int _count)
    {
        count = _count;

        if(count < numVisible)
        {
            foreach(Transform child in transform)
            {
                Destroy(child.gameObject);
                numVisible--;
                if(transform.childCount == count)
                {
                    break;
                }
            }
        }
        else
        {
            while(transform.childCount < Mathf.Min(maxVisible, count))
            {
                Instantiate(slotPrefab, transform);
                numVisible++;
            }
        }
        
        
        if(count > 0)
        {
            int i = 0;
            foreach(Transform child in transform)
            {
                child.GetComponent<UIDynamicSlot>().Refresh(i++);
                child.GetComponent<UIDynamicSlot>().ShowSelection(false);
            }
            transform.GetChild(0).GetComponent<UIDynamicSlot>().ShowSelection(true);
        }

        selectedSlot = 0;
        offset = 0;
    }

    public void MoveUp()
    {
        if(selectedSlot > 0)
        {
            transform.GetChild(selectedSlot).GetComponent<UIDynamicSlot>().ShowSelection(false);
            selectedSlot --;
            transform.GetChild(selectedSlot).GetComponent<UIDynamicSlot>().ShowSelection(true);
        }
        else if(offset > 0)
        {
            transform.GetChild(selectedSlot).GetComponent<UIDynamicSlot>().ShowSelection(false);
            offset --;

            transform.GetChild(numVisible-1).SetAsFirstSibling();
            transform.GetChild(0).GetComponent<UIDynamicSlot>().Refresh(selectedSlot+offset);

            transform.GetChild(selectedSlot).GetComponent<UIDynamicSlot>().ShowSelection(true);
        }
    }

    public void MoveDown()
    {
        if(selectedSlot < numVisible - 1)
        {
            transform.GetChild(selectedSlot).GetComponent<UIItemSlot>().ShowSelection(false);
            selectedSlot ++;
            transform.GetChild(selectedSlot).GetComponent<UIItemSlot>().ShowSelection(true);
        }
        else if(offset+selectedSlot < count - 1)
        {
            transform.GetChild(selectedSlot).GetComponent<UIItemSlot>().ShowSelection(false);
            offset ++;
            
            transform.GetChild(0).SetAsLastSibling();
            transform.GetChild(numVisible-1).GetComponent<UIItemSlot>().Refresh(selectedSlot+offset);
            
            transform.GetChild(selectedSlot).GetComponent<UIItemSlot>().ShowSelection(true);
        }
    }
}
