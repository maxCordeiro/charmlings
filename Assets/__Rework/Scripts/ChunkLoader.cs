using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GameManager;

public class ChunkLoader : MonoBehaviour
{
	const int chunkDistance = 3;
	//using the transform since we only need the position
	public Transform player;
	
	public int mapID;
	public bool loaded;
	//public bool[] loadChunk;
	public GameObject[,] chunks;

	public PolygonCollider2D confinerBox;
	
	public void Init()
	{
		loaded = true;
		//loadChunk = new bool[gm.mapList[mapID].chunkList.Count];
		chunks = new GameObject[gm.mapList[mapID].chunkWidth,gm.mapList[mapID].chunkHeight];
		for(int x = 0; x < chunks.GetLength(0); x++)
		{
			for(int y = 0; y < chunks.GetLength(1); y++)
			{
				//chunks[x,y] = Instantiate(gm.mapList[mapID].chunkList[(x * 3) + y], new Vector3(x * 16f, y * -16f, 0), new Quaternion(), transform);
			}
		}
		//should do a proper convex hull but this works for now I think
		float width = Mathf.Max(gm.mapList[mapID].pixelWidth/16f, 40.1f);
		float height = Mathf.Max(gm.mapList[mapID].pixelHeight/16f, 22.6f);
		confinerBox.SetPath(0, new Vector2[]{Vector2.zero, Vector2.right*width, new Vector2(width, -height), Vector2.down*height});
	}
	
	public void Update()
	{
		if(!loaded) return;
		
		for(int x = 0; x < chunks.GetLength(0); x++)
		{
			for(int y = 0; y < chunks.GetLength(1); y++)
			{
				if(Mathf.Abs(Mathf.Floor((player.position.x)/16) - x) <= chunkDistance && Mathf.Abs(Mathf.Floor((-player.position.y)/16) - y) <= chunkDistance)
				{
					if(!chunks[x,y])
					{
						chunks[x,y] = Instantiate(gm.mapList[mapID].chunkList[(x * 3) + y], new Vector3(x * 16f, y * -16f, 0), new Quaternion(), transform);
					}
				}
				else
				{
					if(chunks[x,y]) 
					{
						Destroy(chunks[x,y]);
					}
				}
			}
		}
		
	}
}
