using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDynamicSlot : MonoBehaviour
{
    public int dataIndex;

    public virtual void Refresh(int _dataIndex)
    {
        
    }
    public virtual void ShowSelection(bool selected)
    {
        
    }
}
