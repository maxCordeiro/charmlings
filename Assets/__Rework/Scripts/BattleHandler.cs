using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GameManager;

public class BattleHandler : MonoBehaviour
{
	public enum StateID
	{NONE,MAIN,SKILLS,ITEMS,PARTY,TARGETING,RESULTS}
	
	public BattleUI battleUI;
	public List<BattleEvent> eventQueue;
	public StateID currState;
	
	public void Awake()
	{
		battleUI = Instantiate(battleUI, gm.uiHandler.canvasUI.transform);
		battleUI.transform.SetAsFirstSibling();
	}
	
	public void Start()
	{
		StartCoroutine(BattleLoop());
	}
	
	//part state machine, part secondary update loop
	public IEnumerator BattleLoop()
	{
		while(gm.transitioning)
		{
			yield return null;
		}
		yield return battleUI.BattleMessage(string.Format(gm.GetText("battle_ready"), battleUI.unitsOpposing[0].battleCharmling.GetName()));
		currState = StateID.MAIN;
		bool run = true;
		while(run)
		{
			switch(currState)
			{
				case StateID.MAIN:
					yield return MainMenu();
					break;
				case StateID.SKILLS:
					yield return SkillMenu();
					break;
				case StateID.ITEMS:
					yield return ItemMenu();
					break;
				case StateID.PARTY:
					yield return PartyMenu();
					break;
				case StateID.RESULTS:
					ChooseAIAction();
					EventQueueProcess();
					yield return EventPlayback();
					battleUI.selectedOption = 0;
					break;
				case StateID.NONE:
					run = false;
					break;
				default:
					yield return null;
					break;
			}
		}
		yield break;
	}
	
	
	public IEnumerator EventPlayback()
	{
		for(int i = 0; i < eventQueue.Count;)
		{
			if(eventQueue[i] is SkillEvent)
			{
				SkillEvent se = eventQueue[i] as SkillEvent;
				if(se.msg != null)
				{
					yield return battleUI.BattleMessage(se.msg);
				}

				//i think this should be fine, KO's will be processed soon
				se.target.battleCharmling.currentHP = Mathf.Clamp(se.target.battleCharmling.currentHP - se.value, 0, se.target.battleCharmling.stats[0]);
				yield return battleUI.Anim(se.user, se.target, se.index);
				yield return battleUI.Anim_Flicker(se.target);
				yield return StartCoroutine(se.target.HUDRefreshHP());
			}
			eventQueue.RemoveAt(0);
		}
		currState = StateID.MAIN;
        yield break;
	}
	
	public IEnumerator MainMenu()
	{
		//initialize main menu
		battleUI.AddLog("SelectAction");
		yield return battleUI.BattleMessage(gm.GetText("battle_what"), false);
        gm.inputHandler.EnableCursor(false);
        battleUI.containerAction.SetActive(true);
        gm.inputHandler.es.SetSelectedGameObject(battleUI.buttonAction[battleUI.selectedOption].gameObject);
		
		//wait for player input
		while(!battleUI.confirmed)
		{
			if(battleUI.back) battleUI.back = false;
			yield return null;
		}
		//after player input
		
		//close main menu and open the selected menu
        battleUI.AddLog("ActionSelected");
		battleUI.containerAction.SetActive(false);
		switch(battleUI.selectedOption)
		{
			case 0:
				currState = StateID.SKILLS;
				break;
			case 1:
				currState = StateID.ITEMS;
				break;
			case 2:
				currState = StateID.PARTY;
				break;
			case 3:
				//run
				//TODO: add confirmation and denial in a trainer battle
				gm.EndBattle();
				Destroy(battleUI.gameObject);
				currState = StateID.NONE;
				break;
			default:
				break;
		}
		battleUI.confirmed = false;
        yield break;
	}
	
	public IEnumerator SkillMenu()
	{
		//initialize skill menu
		battleUI.containerSkill.SetActive(true);
		gm.inputHandler.es.SetSelectedGameObject(battleUI.buttonSkill[0].gameObject);
		gm.inputHandler.cursor.rectTransform.position = new Vector3(UIBase.GetCursorPos().x, UIBase.GetCursorPos().y, gm.inputHandler.cursor.rectTransform.position.z);
		gm.inputHandler.EnableCursor(true);
		
		//wait for player input
		wait:
		while(!battleUI.confirmed)
		{
			//close menu and return to the main menu
			if(battleUI.back)
			{
				currState = StateID.MAIN;
				
				battleUI.containerSkill.SetActive(false);
				gm.inputHandler.EnableCursor(false);
				battleUI.selectedOption = 0;
				battleUI.back = false;

				yield break;
			}
			
			yield return null;
		}
		//after player input
		
		//TODO:add ex move functionality
		if(battleUI.selectedOption == 5)
		{
			battleUI.confirmed = false;
			goto wait;
		}
		
		SkillID skillID = battleUI.unitsPlayer[0].battleCharmling.skills[battleUI.selectedOption].ID;
        int cooldown = gm.db.dbSkill[(int)skillID].cooldown;
		
		//if move is still on cooldown, continue waiting for a valid input
        if (battleUI.unitsPlayer[0].cooldowns[battleUI.selectedOption] != cooldown)
		{
			//move is still on cooldown, continue waiting for a valid input
			battleUI.confirmed = false;
			goto wait;
		}
		
		//adds the skill to the eventQueue
		SkillEvent se = new SkillEvent();
		se.msg = string.Format(gm.GetText("battle_use"), "", battleUI.unitsPlayer[0].battleCharmling.GetName(), gm.GetText(skillID.ToString() + "_NAME"));
		se.anim = "HIT";
		se.user = battleUI.unitsPlayer[0];
		se.target = battleUI.unitsOpposing[0];
		se.index = battleUI.selectedOption;
		eventQueue.Add(se);
		
		currState = StateID.RESULTS;
		battleUI.containerSkill.SetActive(false);
		gm.inputHandler.EnableCursor(false);
		battleUI.confirmed = false;
		
        yield break;
	}
	
	public IEnumerator ItemMenu()
	{
		UIInventory inv = gm.uiHandler.LoadMenu(UIVariables.UIINVENTORY).GetComponent<UIInventory>();
		/*
		inv.onUseItemInBattle += () =>
		{
			tempItemIndex = inv.prevSelectedItemIndex;
			tempPocketIndex = inv.activePocket;
			StartCoroutine(Battle_SpeedCheck());
		};
		*/
		inv.Refresh(true);
		while(!battleUI.confirmed)
		{
			//close menu and return to the main menu
			if(battleUI.back)
			{
				currState = StateID.MAIN;
				
				battleUI.containerSkill.SetActive(false);
				gm.inputHandler.EnableCursor(false);
				battleUI.selectedOption = 1;
				battleUI.back = false;
		
				yield break;
			}
			
			yield return null;
		}
	}
	
	public IEnumerator PartyMenu()
	{
		UIParty party = gm.uiHandler.LoadMenu(UIVariables.UIPARTY).GetComponent<UIParty>();
		party.inBattle = true;
		/*
		party.onSendOutSelected += () =>
		{
			tempSendOutIndex = party.prevSelected;
			StartCoroutine(Battle_SpeedCheck());
		};
		*/
		party.Refresh(true);
		
		while(!battleUI.confirmed)
		{
			//close menu and return to the main menu
			if(battleUI.back)
			{
				currState = StateID.MAIN;
				
				battleUI.containerSkill.SetActive(false);
				gm.inputHandler.EnableCursor(false);
				battleUI.selectedOption = 2;
				battleUI.back = false;
		
				yield break;
			}
			
			yield return null;
		}
		
		currState = StateID.RESULTS;
		party.inBattle = false;
		
        yield break;
	}
	
	public void ChooseAIAction()
	{
		SkillEvent se = new SkillEvent();
		se.msg = string.Format(gm.GetText("battle_use"), "", battleUI.unitsOpposing[0].battleCharmling.GetName(), gm.GetText(battleUI.unitsOpposing[0].battleCharmling.skills[battleUI.selectedOption].ID.ToString() + "_NAME"));
		se.anim = "HIT";
		se.user = battleUI.unitsOpposing[0];
		se.target = battleUI.unitsPlayer[0];
		se.index = battleUI.selectedOption;
		eventQueue.Add(se);
	}

	[System.Serializable]
	public class BattleEvent
	{
		public string msg;
		public string anim;
		//how many turns to wait
		public int turnCount;
		public BattleUnit user;
		public int index;
	}
	
	[System.Serializable]
	public class SkillEvent : BattleEvent
	{
		public int value;
		public string endMsg;
		public BattleUnit target;
	}
	
	public void EventQueueProcess()
	{
		EventSort();
		//I'm using a temporary list to iterate through so I can add things to the original without any errors
		List<BattleEvent> tempQueue = new List<BattleEvent>(eventQueue);
		foreach(BattleEvent be in tempQueue)
		{
			if(be is SkillEvent)
			{
				ProcessSkill(be as SkillEvent);
			}
		}
	}
	
	public void EventSort()
	{
		//sort events by speed and priority first
		for(int i = 0; i < eventQueue.Count; i++)
		{
			for(int u = 0; u < eventQueue.Count; u++)
			{
				//TODO: add MYTURN perk check as well as priority check
				if(eventQueue[i].user.GetStat((int)StatID.AGILITY) < eventQueue[u].user.GetStat((int)StatID.AGILITY))
				{
					BattleEvent temp = eventQueue[i];
					eventQueue[i] = eventQueue[u];
					eventQueue[u] = temp;
				}
			}
		}
	}
	
	public void ProcessSkill(SkillEvent se)
	{	
		//assign neccesarry info for damage calulation
		SkillData skill = gm.db.dbSkill[(int)se.user.battleCharmling.skills[se.index].ID];
		int level = se.user.battleCharmling.level;
		int power = skill.power;
		int attack = 1;
		int guard = 1;
		float multi = 1;
		if(skill.skillType == SkillTypeID.STRENGTH)
		{
			attack = se.user.GetStat((int)StatID.STRENGTH);
			guard = se.user.GetStat((int)StatID.DEFENSE);
		}
		if(skill.skillType == SkillTypeID.MAGIC)
		{
			attack = se.user.GetStat((int)StatID.MAGIC);
			guard = se.user.GetStat((int)StatID.RESISTANCE);
		}

		switch(skill.ID)
		{
			case SkillID.TRIPLEFIST:
			{
				SkillEvent new_event = new SkillEvent();
				for(int i = 0; i < 2; i++)
				{
					new_event.anim = "HIT";
					new_event.user = battleUI.unitsPlayer[0];
					new_event.target = battleUI.unitsOpposing[0];
					new_event.index = battleUI.selectedOption;
					if(Random.value <= 0.12f)
					{
						//new_event.status |= Status.CRIT;
						multi *= 2f;
					}
					new_event.value = Mathf.CeilToInt((((level * attack * power/3) / (250f * guard)) + 2f) * multi);
					eventQueue.Insert(eventQueue.IndexOf(se) + 1,new_event);
				}
				goto default;
			}
			
			case SkillID.MINDBEND:
			{
				//swaps the attack power stat to strength
				attack = se.user.GetStat((int)StatID.STRENGTH);
				goto default;
			}

			default:
			{
				if(Random.value <= 0.12f)
				{
					//se.status |= Status.CRIT;
					multi *= 2f;
				}
				se.value = Mathf.CeilToInt((((level * attack * power) / (250f * guard)) + 2f) * multi);
				break;
			}
		}
	}
}
