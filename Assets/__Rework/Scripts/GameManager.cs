using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using Cinemachine;
using Translation;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //the active GameManager instance
    //named gm for brevity
    //must add the "using static GameManager" directive to use it
    public static GameManager gm;
	
	public Camera mainCam;
	public InputHandler inputHandler;
	public UIHandler uiHandler;
	public TransitionHandler transitionHandler;
    public CinemachineVirtualCamera playerCam;
    public CinemachineVirtualCamera battleCam;
    public AudioSource music;
	
	//NOTE: this should probably be changed to be consistent with the database items
	public List<MapInfo> mapList;
	
	public bool inWorld;
	public bool inBattle;
	public bool inMenu => uiHandler.openMenus.Count > 0;
    public bool transitioning;

    #region Data
	public const int MAXITEMSTACK = 999;

    public const float CHANCECOMMON = 0.95f;
    public const float CHANCEUNCOMMON = 0.04f;
    public const float CHANCERARE = 0.01999f;
    public const float CHANCEULTRARARE = 0.00001f;

    [Header("Database")]
    public bool loadedDB;
	
    public class LocalGameDatabase
	{
		public List<CharmlingData> dbCharmling;
		public List<ItemData> dbItem;
		public List<SkillData> dbSkill;
		public List<PerkData> dbPerk;
		public List<MapData> dbMap;
		public List<SkillsetData> dbSkillset;
		public List<TraitData> dbTrait;
		public List<AttributeData> dbAttr;
		/*public static List<FamilyData> dbFamily;
		public static List<TamerData> dbTamer;
		public static List<AttrData> dbAttr;*/
	}
	
	public LocalGameDatabase db;
	
	public LocalGameDatabase Load(string path)
    {
        var serializer = new XmlSerializer(typeof(LocalGameDatabase));
        using (var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as LocalGameDatabase;
        }
    }

    //Loads the xml directly from the given string. Useful in combination with www.text.
    public LocalGameDatabase LoadFromText(string text)
    {
        var serializer = new XmlSerializer(typeof(LocalGameDatabase));
        return serializer.Deserialize(new StringReader(text)) as LocalGameDatabase;
    }
	
	public void InitDatabase()
    {
        db = new LocalGameDatabase();
        TextAsset dbXML = Resources.Load<TextAsset>("db");
        db = LoadFromText(dbXML.text);
    }

	public CharmlingData GetCharmlingData(CharmlingIdentifier _charmling)
    {
        return db.dbCharmling.Find(x => x.ID == _charmling.ID && x.form == _charmling.formID);
    }

    public ItemData GetItemData(ItemID _item, int _secID = 0)
    {
        return db.dbItem.Find(x => x.ID == _item);
    }

    public SkillsetData GetSkillsetData(CharmlingIdentifier _charmling)
    {
        return db.dbSkillset.Find(x => x.charmling == _charmling);
    }

	#endregion
	
    #region Utility
    public string ToXML(object obj)
    {
        using (var stringwriter = new StringWriter())
        {
            var serializer = new XmlSerializer(obj.GetType());
            serializer.Serialize(stringwriter, obj);
            return stringwriter.ToString();
        }
    }

    public float Choose(params float[] probs)
    {
        float total = 0;

        foreach (float elem in probs)
        {
            total += elem;
        }

        float randomPoint = UnityEngine.Random.value * total;

        for (int i = 0; i < probs.Length; i++)
        {
            if (randomPoint < probs[i])
            {
                return i;
            }
            else
            {
                randomPoint -= probs[i];
            }
        }

        return probs.Length - 1;
    }

    public int GetFirstNull<T>(T[] array)
    {
        int openSlot = -1;

        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] == null)
            {
                openSlot = i;
                break;
            }
        }

        return openSlot;
    }

    public void ChangeSpriteAlpha(SpriteRenderer spr, float val)
    {
        Color col = spr.color;
        col.a = val;
        spr.color = col;
    }
    #endregion

    #region Localization  
    [HideInInspector]
    public string[] langSelection;
    public Translator lang;
	
    public void InitLocale()
    {
        try
        {
            lang = new Translator(ELangFormat.Csv, "Locale/languages", ",");
            langSelection = lang.GetAvailableLanguages();

            lang.LoadDictionary(langSelection[0]);
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
            lang = null;
        }
    }

    public string GetText(string id)
    {
        string text = lang[id];

        return text.Length > 0 ? text : "*" + id;
    }
    #endregion

	#region Scenes
    //if the object that called StartCoroutine is destroyed the coroutine won't finish
    //so x starts the x_C corutine when loading a new scene
    //probably not as necceisary for the battle scene since it's additive but no reason to not be careful
    
    public void LoadMap(int mapID, Vector3 position = new Vector3())
    {
        StartCoroutine(LoadMap_C(mapID, position));
    }

    //should take parameters like map index, player rotation, etc.
    //we can most likely add animations too, no clue how they work with coroutines though
    public IEnumerator LoadMap_C(int mapID, Vector3 position)
    {
        transitioning = true;
        //loads in the overworld scene
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(3);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        playerCam.gameObject.SetActive(true);
        FindObjectOfType<ChunkLoader>().mapID = mapID;
        FindObjectOfType<ChunkLoader>().Init();
        //too verbose, make it simpler
        playerCam.GetComponent<CinemachineConfiner2D>().m_BoundingShape2D = FindObjectOfType<ChunkLoader>().confinerBox;
        playerCam.m_Follow = FindObjectOfType<PlayerEntity>().transform;
        FindObjectOfType<PlayerEntity>().transform.SetPositionAndRotation(position, new Quaternion());
        
        transitioning = false;
        yield break;
    }
    
    public void StartBattle()
    {
        StartCoroutine(StartBattle_C());
    }
    //needs to take params
    //should only be used when in the overworld scene
    public IEnumerator StartBattle_C()
    {
        transitioning = true;
        yield return transitionHandler.SimpleFadeOut(UITransitionType.LINES);

        playerCam.gameObject.SetActive(false);
        PlaySong("Temporary");
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(2, LoadSceneMode.Additive);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        battleCam.gameObject.SetActive(true);

        yield return transitionHandler.SimpleFadeIn(UITransitionType.WIPE);
        transitioning = false;

        inBattle = true;
        SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(2));
        yield break;
    }
    
    public void EndBattle()
    {
        StartCoroutine(EndBattle_C());
    }
    
    public IEnumerator EndBattle_C()
    {
        transitioning = true;
        yield return transitionHandler.SimpleFadeOut(UITransitionType.SOLID);

        battleCam.gameObject.SetActive(false);
        AsyncOperation asyncUnload = SceneManager.UnloadSceneAsync(2);
        while (!asyncUnload.isDone)
        {
            yield return null;
        }
        PlaySong("");
        playerCam.gameObject.SetActive(true);

        yield return transitionHandler.SimpleFadeIn(UITransitionType.SOLID);
        transitioning = false;

        inBattle = false;
        yield break;
    }
	#endregion

    public void PlaySong(string path)
    {
        if(path == null || path == "")
        {
            music.Stop();
        }
        Debug.Log(Resources.Load<AudioClip>(path));
        music.clip = Resources.Load<AudioClip>(path);
        music.Play();
    }
	
    private void Awake()
    {
        if (gm != null && gm != this)
        {
            Destroy(gm.gameObject);
			gm = this;
			return;
        }

		gm = this;

        DontDestroyOnLoad(gameObject);

		InitDatabase();
		InitLocale();

		SceneManager.LoadScene(1);
        //SceneManager.sceneLoaded += OnSceneLoad;
        //currMap = GetMapData(SaveSystem.loaded.player.activeMap);
        //hideLighting = currMap.hideLighting;
    }

    private void Update()
    {
        if (inMenu || inBattle || SaveSystem.loaded == null) return;
    }
}

	//TODO seperate clock so it only appears in overworld scene
	/*
	#region Time
    [Header("Time")]
    public float timeMultiplier = 1f;
    public const float inGameMinuteTime = 0.8335f;

    public readonly int[] monthLengths = new int[] { 30, 27, 30, 29, 30, 29, 30, 30, 29, 30, 29, 30 };

    public static string[] monthIDs = new string[]
	{
        "month_jan_abbr",
        "month_feb_abbr",
        "month_mar_abbr",
        "month_apr_abbr",
        "month_may_abbr",
        "month_june_abbr",
        "month_july_abbr",
        "month_aug_abbr",
        "month_sep_abbr",
        "month_oct_abbr",
        "month_nov_abbr",
        "month_dec_abbr",
	};

    public static string[] dayIDs = new string[]
    {
        "day_sun_abbr",
        "day_mon_abbr",
        "day_tue_abbr",
        "day_wed_abbr",
        "day_thu_abbr",
        "day_fri_abbr",
        "day_sat_abbr",
    };

    // Time, in hours, when each day phase starts
    int timeMorning = 5;
    int timeDay = 8;
    int timeEvening = 17;
    int timeNight = 22;

    [Header("Time UI")]
    public SuperTextMesh uiDate;
    public SuperTextMesh uiTime;
    public SuperTextMesh uiDay;

    // Day/Night transition
    float transitionTime = 0.0025f;
    MapData currMap;
    bool hideLighting;

    public event Action onMinutePass;
    public event Action onHourPass;
    public event Action onDayPass;
    public event Action onWeekPass;
    public event Action onMonthPass;
    public event Action onYearPass;
	
    public void MinutePass() => onMinutePass.Invoke();
    public void HourPass() => onHourPass?.Invoke();
    public void DayPass() => onDayPass?.Invoke();
    public void WeekPass() => onWeekPass?.Invoke();
    public void MonthPass() => onMonthPass?.Invoke();
    public void YearPass() => onYearPass?.Invoke();

    public void UpdateText()
    {
        uiDate.text = PrintDate();
        uiTime.text = PrintTime();
        uiDay.text = PrintDay();
    }

    public void ClockInit()
    {
        Transform clockObj = UIHandler.instance.canvasUI.transform.Find("Clock");

        uiDate = clockObj.Find("Date").GetChild(0).GetComponent<SuperTextMesh>();
        uiTime = clockObj.Find("Time").GetChild(0).GetComponent<SuperTextMesh>();
        uiDay = clockObj.Find("Day").GetChild(0).GetComponent<SuperTextMesh>();
        UpdateText();
    }

    public void UpdateLighting()
    {
    }
	
	public void ClockUpdate()
	{
		SaveSystem.loaded.game.inGameTime += Time.deltaTime * timeMultiplier;
		if(SaveSystem.loaded.game.inGameTime > inGameMinuteTime)
		{
            SaveSystem.loaded.game.inGameTime %= inGameMinuteTime;
			if(++SaveSystem.loaded.game.currMinute == 60)
			{
                SaveSystem.loaded.game.currMinute = 0;
				if(++SaveSystem.loaded.game.currHour == 24)
				{
                    SaveSystem.loaded.game.currHour = 0;
                    SaveSystem.loaded.game.currDayIndex = ((SaveSystem.loaded.game.currDayIndex + 1) % 7);
					if(SaveSystem.loaded.game.currDayInMonth++ == monthLengths[SaveSystem.loaded.game.currMonth])
					{
                        SaveSystem.loaded.game.currDayInMonth = 1;
						if(++SaveSystem.loaded.game.currMonth == 12)
						{
                            SaveSystem.loaded.game.currMonth = 0;
                            SaveSystem.loaded.game.currYear++;
							YearPass();
						}
						MonthPass();
					}
					DayPass();
                    SaveSystem.loaded.game.currDayID = (DayID)SaveSystem.loaded.game.currDayIndex;
                }
				HourPass();

                if (SaveSystem.loaded.game.inGameTime >= timeMorning && SaveSystem.loaded.game.inGameTime <= timeDay)
                {
                    SaveSystem.loaded.game.currDaytime = DaytimeID.MORNING;
                }
                else if (SaveSystem.loaded.game.inGameTime >= timeEvening && SaveSystem.loaded.game.inGameTime <= timeNight)
                {
                    SaveSystem.loaded.game.currDaytime = DaytimeID.EVENING;
                }
                else if (SaveSystem.loaded.game.inGameTime > timeDay && SaveSystem.loaded.game.inGameTime < timeEvening)
                {
                    SaveSystem.loaded.game.currDaytime = DaytimeID.DAY;
                }
                else if (SaveSystem.loaded.game.inGameTime > timeNight && SaveSystem.loaded.game.inGameTime < timeMorning)
                {
                    SaveSystem.loaded.game.currDaytime = DaytimeID.NIGHT;
                }
            }
			MinutePass();
		}
    }
	
    public string PrintDate() => string.Format("{0} {1}, Y{2}", SaveSystem.loaded.game.currDayInMonth + 1, lang[monthIDs[SaveSystem.loaded.game.currMonth]], SaveSystem.loaded.game.currYear);
    public string PrintTime()
    {
        return string.Format("{0}:{1} {2}", (((SaveSystem.loaded.game.currHour + 11) % 12) + 1).ToString("00"), ((SaveSystem.loaded.game.currMinute / 10) * 10).ToString("00"), lang[(SaveSystem.loaded.game.currHour > 12 & SaveSystem.loaded.game.currHour < 24)? "time_pm" : "time_am"]);
    }
    public string PrintDay() => string.Format("{0}", lang[dayIDs[SaveSystem.loaded.game.currDayIndex]]);

    #endregion
	*/
