using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GameManager;

public class ChooseScene : MonoBehaviour
{
    void Update()
    {
		if (Input.GetKeyDown(KeyCode.Alpha1))
        {
			gm.LoadMap(0, new Vector3(20, -20));
		}
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
			gm.LoadMap(0, new Vector3(30, -25));
		}
    }
}
