using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Map", menuName = "ScriptableObjects/MapInfo ScriptableObject", order = 1)]
public class MapInfo : ScriptableObject
{
	//just fill in the neccisarry info
	//we can add more later if we need to
	public int chunkWidth;
	public int pixelWidth;
	public int chunkHeight;
	public int pixelHeight;
	public List<GameObject> chunkList;
}
