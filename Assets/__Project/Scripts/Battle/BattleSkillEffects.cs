using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleSkillEffects : MonoBehaviour
{
	//we might be able to remove this script
    public BattleUI bController;

    public IEnumerator HIT()
    {
        yield return StartCoroutine(bController.Battle_ProcessDamage());

        yield return StartCoroutine(bController.Battle_Hurt());

        //yield return StartCoroutine(bController.Anim());

        StartCoroutine(bController.Anim_Flicker(new BattleUnit()));

        yield return StartCoroutine(bController.GetTarget().HUDRefreshHP());

        yield return StartCoroutine(bController.Battle_ContactCheck());
        yield break;
    }

    public IEnumerator HALFHP()
    {
        yield return StartCoroutine(bController.Battle_ProcessDamage());

        //yield return StartCoroutine(bController.AnimMotion());

        //yield return StartCoroutine(bController.PlayAnim());

        int prevHP = bController.GetTarget().battleCharmling.currentHP;

        int healAmt = (int)((bController.GetActiveBattler().currInflictingDmg / 2) * (bController.GetActiveBattler().battleCharmling.perk == PerkID.SOOTHINGHEAL ? 1.2f : 1));

        string msg;

        // PERKCHECK: ZOMBIE
        if (bController.GetTarget().battleCharmling.perk == PerkID.ZOMBIE)
        {
            bController.GetActiveBattler().battleCharmling.Hurt(healAmt);

            //yield return StartCoroutine(bnh.PlayAnim(bnh.currAttacker));

            //StartCoroutine(bnh.AnimFlicker(bnh.currAttacker));

            yield return StartCoroutine(bController.GetActiveBattler().HUDRefreshHP());
            
            // msg = string.Format(SystemVariables.trans["battle_perk_zombie"], bnh.GetPrefix(true), bnh.GetOpposing().GetName(), PerkSystem.GetPerkName(bnh.GetOpposing().perk), bnh.GetPrefix(false), bnh.GetAttacker().GetName());

            //yield return StartCoroutine(bnh.BattleMessage(msg));
        }
        else
        {
            yield return StartCoroutine(bController.Battle_Hurt());

            StartCoroutine(bController.Anim_Flicker(new BattleUnit()));

            yield return StartCoroutine(bController.GetTarget().HUDRefreshHP());

            if (bController.GetActiveBattler().battleCharmling.Heal(healAmt))
            {
                //yield return StartCoroutine(bnh.PlayAnim(bnh.currAttacker, false));

                yield return StartCoroutine(bController.GetActiveBattler().HUDRefreshHP());
                //yield return StartCoroutine(bnh.UpdateHealth(HealthType.HEAL));

                //msg = string.Format(SystemVariables.trans["battle_skill_halfhp_drain"], bnh.GetPrefix(false), bnh.GetAttacker().GetName(), bnh.currDMG.ToString(), bnh.GetPrefix(true), bnh.GetOpposing().GetName());

                //yield return StartCoroutine(bnh.BattleMessage(msg));
            }
        }

        //yield return StartCoroutine(bnh.AnimMotionAfter());

        yield return StartCoroutine(bController.Battle_ContactCheck());

        yield break;
    }

}