﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
//O for obsolete
public class BattleUnit : MonoBehaviour
{
	//TODO: figure out what to do with this
    public BattleHUDUnit battleHUD;

    public SpriteRenderer battleSprite;

    [Header("Battle")]
    public Charmling battleCharmling;
    public BattleUI battleController;
    
    [Header("Logic")]
    public bool itemUsed;
    public bool isOpposing;
    public bool turnOver;
    public bool activeBattler;
    public bool currSkillCrit;
    public bool isTarget;

    public int selectedSkill;
    public int prevHP;

    public int currInflictingDmg;
    
    [Header("Charmling Info")]
    public int[] cooldowns = new int[5] { 0, 0, 0, 0, 0 };
    public int[] statChanges = new int[5] { 0, 0, 0, 0, 0 };

    public float accuracy = 1.0f;
    public int accChanges;

    public string skillEffectID;
    public int exSkillCharge;

    public int sleepTimer;
    public int swappingOutIndex;

    public void Init()
    {
        itemUsed = false;
        turnOver = false;
        activeBattler = false;

        battleSprite = transform.GetChild(1).GetComponent<SpriteRenderer>();
        battleSprite.sprite = battleCharmling.GetSprite();
        battleSprite.flipX = !isOpposing;

        for (int i = 0; i < battleCharmling.skills.Length; i++)
        {
            cooldowns[i] = GameManager.gm.db.dbSkill[(int)battleCharmling.skills[i].ID].cooldown;
        }

        statChanges = new int[5] { 0, 0, 0, 0, 0 };

        accChanges = 0;

        skillEffectID = "";

        sleepTimer = 0;
    }

    public void HUDInit()
    {
        battleHUD = Instantiate(Resources.Load<GameObject>("Battle/Prefabs/HUDUnit"), battleController.transform).GetComponent<BattleHUDUnit>();
        //uses the postion of the battle controller just in case
        battleHUD.GetComponent<RectTransform>().SetPositionAndRotation(battleController.transform.position + transform.position + Vector3.up*7, new Quaternion());
        StartCoroutine(HUDRefreshHP(false));
        HUDRefreshStatus();
    }

    public void HUDRefreshInfo()
    {
        battleHUD.textName.text = battleCharmling.GetName();
        battleHUD.textHP.text = battleCharmling.currentHP.ToString() + "/" + battleCharmling.GetStatValue(StatID.HEALTH).ToString();
        battleHUD.textLevel.text = "<size=20>Lv</s>" + battleCharmling.level.ToString();
        StartCoroutine(HUDRefreshHP());
        HUDRefreshStatus();
    }

    public IEnumerator HUDRefreshHP(bool animate = true)
    {
        int currHP = battleCharmling.currentHP;
        float minWidth = Mathf.Floor(232 * (float)((float)currHP / (float)battleCharmling.stats[(int)StatID.HEALTH]));
        int hpAmt = (int)Mathf.Abs(prevHP - currHP);

        RectTransform hpRect = battleHUD.barHealth.rectTransform;

        battleHUD.textHP.text = battleCharmling.currentHP.ToString() + "/" + battleCharmling.GetStatValue(StatID.HEALTH).ToString();

        if (animate)
        {
            LeanTween.size(hpRect, new Vector2(minWidth, 24), 0.25f);
            yield return new WaitForSeconds(0.45f);
        } else
        {
            hpRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, minWidth);
            yield break;
        }
    }

    public void HUDRefreshStatus()
    {
        if (battleCharmling.status != StatusID.KO)
        {
            Color b = UIVariables.statusColors[battleCharmling.status];
            battleHUD.imageBox.color = new Color(b.r, b.g, b.b, 0.8274f);

            if (battleCharmling.status != StatusID.NONE)
            {
                battleHUD.iconStatus.enabled = true;
                battleHUD.iconStatus.sprite = battleHUD.spriteStatus[(int)battleCharmling.status];
            } else
            {
                battleHUD.iconStatus.enabled = false;
            }
        }
    }
	
	public int GetStat(int index)
	{
        if(index == 0)
        {
            return battleCharmling.stats[index];
        }
		return statChanges[index-1] + battleCharmling.stats[index];
	}

    public void PassSleepCounter()
    {
        sleepTimer -= 1;

        if (sleepTimer <= 0)
        {
            battleCharmling.status = StatusID.NONE;
            HUDRefreshStatus();
        }
    }

    private void OnDestroy()
    {
        if (battleHUD != null) Destroy(battleHUD.gameObject);
    }
}