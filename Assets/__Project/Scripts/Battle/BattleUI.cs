﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using UnityEditor;
using static GameManager;

public class BattleUI : MonoBehaviour
{
    [Header("UI")]
    public GameObject containerAction;
    public GameObject containerSkill;
    public GameObject containerMessage;

    public Button[] buttonAction;
    public UISkillButton[] buttonSkill;
    public Button buttonBack;

    public SuperTextMesh textBattle;

    [Header("Battle")]
    public GameObject battleObject;

    public GameObject unitObject;
    public GameObject unit_L;
    public GameObject unit_R;

    public List<BattleUnit> unitsPlayer;
    public List<BattleUnit> unitsOpposing;

    [Header("Logic")]
	public bool confirmed;
	public bool back;
    public int selectedOption = 0;
	
	
    public int selectedAction;
    public int unitPlayerIndex;

    public int tempSendOutIndex = -1;
    public int tempItemIndex = -1;
    public BagPockets tempPocketIndex;

    bool selectingSkill;

    public BattleSkillEffects skillEffects;

    public List<string> log;
    string charmlingDebug;
    bool drawDebug;

    private void Awake()
    {
        battleObject = GameObject.FindGameObjectWithTag("Battle");

        unit_L = battleObject.transform.Find("Unit_L").gameObject;
        unit_R = battleObject.transform.Find("Unit_R").gameObject;

        unitsPlayer = new List<BattleUnit>();
        unitsOpposing = new List<BattleUnit>();

        log = new List<string>();
		
		/*
        for (int i = 0; i < buttonSkill.Length; i++)
        {
            int ii = i;
            buttonSkill[i].button.onClick.AddListener(() => {
                Battle_OnSkillSelect(ii);
            });
        }
		*/
		Charmling b = new Charmling(new CharmlingIdentifier(CharmlingID.GUBBERS, 0));

		b.skills[0] = new Skill(SkillID.SLAM);
		b.skills[1] = new Skill(SkillID.SLAM);
		b.skills[2] = new Skill(SkillID.SLAM);
		b.skills[3] = new Skill(SkillID.SLAM);
		b.skills[4] = new Skill(SkillID.SLAM);
		b.level = 50;
		b.CalculateAllStats();

		AddUnit(1, b);
		
		
		Charmling c = new Charmling(new CharmlingIdentifier(CharmlingID.JILO, 0));

		c.skills[0] = new Skill(SkillID.TRIPLEFIST);
		c.skills[1] = new Skill(SkillID.MINDBEND);
		c.skills[2] = new Skill(SkillID.SLAM);
		c.skills[3] = new Skill(SkillID.SLAM);
		c.skills[4] = new Skill(SkillID.SLAM);
		c.level = 50;
		c.CalculateAllStats();

		AddUnit(0, c);


        if (unitsPlayer.Count < 1 || unitsOpposing.Count < 1)
        {
            ClearUnits();
            throw new SystemException("Not enough units to start a battle.");
        }
		
        containerAction.SetActive(true);
        containerMessage.SetActive(true);

        UnitInit();
        HUDInit();
        unitPlayerIndex = 0;

        //StartCoroutine(Battle_Begin());
    }

    private void Update()
    {
		if (gm.inputHandler.rPlayer.GetButtonDown("B"))
		{
			back = true;
		}
        if (gm.uiHandler.inMenu) return;

        if (selectingSkill)
        {
            if (gm.inputHandler.rPlayer.GetButtonDown("B"))
            {
                back = true;
            }
        }

        if (gm.inputHandler.cursor.IsActive())
        {
            UIBase.ReposCursor();
        }

        if (Input.GetKeyDown(KeyCode.F1))
        {
            drawDebug = !drawDebug;
        }
    }

    public void AddLog(string msg) => log.Add(msg);

    #region Init
    public void UnitInit()
    {
        foreach (BattleUnit unit in unitsPlayer)
        {
            unit.Init();
        }

        foreach (BattleUnit unit in unitsOpposing)
        {
            unit.Init();
        }
    }

    public void HUDInit()
    {
        foreach (BattleUnit unit in unitsPlayer)
        {
            unit.HUDInit();
            unit.HUDRefreshInfo();
        }

        foreach (BattleUnit unit in unitsOpposing)
        {
            unit.HUDInit();
            unit.HUDRefreshInfo();
        }

        for (int i = 0; i < buttonSkill.Length; i++)
        {
            buttonSkill[i].id = unitsPlayer[unitPlayerIndex].battleCharmling.skills[i].ID;
            buttonSkill[i].Refresh(i, unitsPlayer[unitPlayerIndex]);
        }
    }
    #endregion
	
	
    public IEnumerator BattleMessage(string msg, bool pause = true)
    {
        textBattle.text = msg;

        if (pause)
        {
            while (textBattle.reading)
            {
                yield return new WaitForEndOfFrame();
            }

            float counter = 0;

            while (counter <= 1.2f)
            {
                counter += Time.deltaTime;

                if (gm.inputHandler.rPlayer.GetButtonDown("A"))
                {
                    counter = 1.2f;
                }

                yield return new WaitForEndOfFrame();
            }
        }

        yield break;
    }
	
	public void Confirm(int index)
    {
		confirmed = true;
        selectedOption = index;
    }
	
	

    #region Animation

    // Attacker = 0 if playing an animation on the player's side. 1 if enemy
    public IEnumerator Anim(BattleUnit user, BattleUnit target, int index, string animSuffix = "", float pauseLength = 0.1f)
    {
        GameObject loadedAnim = Resources.Load<GameObject>("Battle/Animations/Anim_" + user.battleCharmling.skills[index].ID.ToString().ToUpper() + (animSuffix.Length != 0 ? animSuffix : ""));

        if (loadedAnim == null) loadedAnim = Resources.Load<GameObject>("Battle/Animations/Anim_DEBUG");

        GameObject anim = Instantiate(loadedAnim, battleObject.transform.position, Quaternion.identity);
        BattleAnimation animComp = anim.GetComponent<BattleAnimation>();
		animComp.user = user;
		animComp.target = target;
		animComp.id = user.battleCharmling.skills[index].ID;

        animComp.LoadAnim(this);

        yield return StartCoroutine(animComp.AnimProcess());
        
        yield return new WaitForSeconds(pauseLength);
    }

    public IEnumerator Anim_Flicker(BattleUnit unit)
    {
        for (int i = 0; i < 2; i++)
        {
            gm.ChangeSpriteAlpha(unit.battleSprite, 0);

            yield return new WaitForSeconds(0.05f);
            
            gm.ChangeSpriteAlpha(unit.battleSprite, 1);

            yield return new WaitForSeconds(0.1f);
        }
    }

    public IEnumerator Anim_MotionClose(BattleUnit unit)
    {
        LeanTween.moveLocalX(unit.gameObject, unit == unitsPlayer[0] ? 10 : -10, 0.5f).setEaseOutExpo();

        yield return new WaitForSeconds(0.5f);
    }

    public IEnumerator Anim_MotionReturn(BattleUnit unit)
    {
        LeanTween.moveLocalX(unit.gameObject, 0, 0.5f).setEaseOutSine();

        yield return new WaitForSeconds(0.5f);
    }

    public IEnumerator Anim_Shake(BattleUnit unit, bool selfAnim = false)
    {
        int mult = selfAnim == true ? 1 : -1;
        float waitTime = 0.025f;
        //mult *= isRanged ? -1 : 1;

        LeanTween.moveLocalX(unit.gameObject, (0.5f * mult), waitTime);
        yield return new WaitForSeconds(waitTime);

        LeanTween.moveLocalX(unit.gameObject, 0, waitTime);
        yield return new WaitForSeconds(waitTime);

        LeanTween.moveLocalX(unit.gameObject, -(0.5f * mult), waitTime);
        yield return new WaitForSeconds(waitTime);

        LeanTween.moveLocalX(unit.gameObject, 0, waitTime);
        yield return new WaitForSeconds(waitTime);
    }

    #endregion

    public void OnGUI()
    {
        if (gm.uiHandler.inBattle && EditorApplication.isPlaying && drawDebug)
        {
            charmlingDebug = GUI.TextArea(new Rect(10, 150, 200, 450), unitsPlayer[0].battleCharmling.ToString());
            charmlingDebug = GUI.TextArea(new Rect(1070, 150, 200, 450), unitsOpposing[0].battleCharmling.ToString());
        }
    }
    
    public void AddUnit(int unitIndex, Charmling charmling)
    {
        BattleUnit bu = Instantiate<GameObject>(unitObject, unitIndex == 0 ? unit_L.transform : unit_R.transform).GetComponent<BattleUnit>();

        bu.battleCharmling = charmling;
        bu.battleController = this;
        bu.isOpposing = unitIndex != 0;
        bu.name = bu.isOpposing ? $"BattleUnit Enemy [{unitIndex}]" : $"BattleUnit Player [{unitIndex}]";

        if (unitIndex == 0)
        {
            unitsPlayer.Add(bu);
        }
        else
        {
            unitsOpposing.Add(bu);
        }
    }
//-------------------------------TODO: when we no longer need to help from these functions, remove them-------------------------------//

    public void ClearUnits()
    {
        foreach (BattleUnit unit in unitsPlayer)
        {
            Destroy(unit.gameObject);
        }

        foreach(BattleUnit unit in unitsOpposing)
        {
            Destroy(unit.gameObject);
        }

        unitsPlayer.Clear();
        unitsOpposing.Clear();
    }

    public IEnumerator Battle_BeginAttack()
    {
        AddLog("BeginAttack");

        yield return StartCoroutine(Battle_AI_SkillChoose());
        yield return StartCoroutine(Battle_SpeedCheck());

        if (tempSendOutIndex == -1 || tempItemIndex == -1) yield return StartCoroutine(Battle_PreAttack());
    }

    public IEnumerator Battle_AI_SkillChoose()
    {
        AddLog("AI: SkillChoose");
        // TODO: properly check for a skill

        unitsOpposing[0].selectedSkill = 0;

        yield break;
    }

    public IEnumerator Battle_SpeedCheck()
    {
        AddLog("SpeedCheck");

        int playerSpeed = unitsPlayer[0].battleCharmling.stats[(int)StatID.AGILITY];
        int oppSpeed = unitsOpposing[0].battleCharmling.stats[(int)StatID.AGILITY];

        int playerPriority = gm.db.dbSkill[(int)unitsPlayer[0].battleCharmling.skills[unitsPlayer[0].selectedSkill].ID].priorityLevel;
        int oppPriority = gm.db.dbSkill[(int)unitsOpposing[0].battleCharmling.skills[unitsOpposing[0].selectedSkill].ID].priorityLevel;

        ResetUnits();

        // If the player is sending out a Charmling. Needs an opposing check here as well
        if (tempSendOutIndex > -1)
        {
            unitsPlayer[0].activeBattler = true;
            unitsOpposing[0].isTarget = true;

            StartCoroutine(Battle_SwapOut());
        } else if (tempItemIndex > -1)
        {
            unitsPlayer[0].activeBattler = true;
            unitsOpposing[0].isTarget = true;

            StartCoroutine(Battle_UseItem());
        } 
        else
        {
            if (playerPriority > oppPriority)
            {
                unitsPlayer[0].activeBattler = true;
                unitsOpposing[0].isTarget = true;

            }
            else if (playerPriority < oppPriority)
            {
                unitsOpposing[0].activeBattler = true;
                unitsPlayer[0].isTarget = true;
            }
            else
            {
                if (playerSpeed == oppSpeed || unitsPlayer[0].battleCharmling.perk == PerkID.MYTURN && unitsOpposing[0].battleCharmling.perk == PerkID.MYTURN)
                {
                    int randomTurn = Random.Range(0, 2);

                    if (randomTurn == 0)
                    {
                        unitsPlayer[0].activeBattler = true;
                        unitsOpposing[0].isTarget = true;
                    }
                    else
                    {
                        unitsOpposing[0].activeBattler = true;
                        unitsPlayer[0].isTarget = true;
                    }
                }
                else
                {
                    if (unitsPlayer[0].battleCharmling.perk == PerkID.MYTURN)
                    {
                        unitsPlayer[0].activeBattler = true;
                        unitsOpposing[0].isTarget = true;
                    }
                    else if (unitsOpposing[0].battleCharmling.perk == PerkID.MYTURN)
                    {
                        unitsOpposing[0].activeBattler = true;
                        unitsPlayer[0].isTarget = true;
                    }
                    else
                    {
                        if (playerSpeed > oppSpeed)
                        {
                            unitsPlayer[0].activeBattler = true;
                            unitsOpposing[0].isTarget = true;
                        }
                        else
                        {
                            unitsOpposing[0].activeBattler = true;
                            unitsPlayer[0].isTarget = true;
                        }
                    }
                }
            }
        }


        yield break;
    }


    public IEnumerator Battle_PreAttack()
    {
        AddLog("PreAttack");

        if (GetActiveBattler().battleCharmling.status == StatusID.DROWSY)
        {
            GetActiveBattler().PassSleepCounter();
        }

        yield return StartCoroutine(Battle_ProcessDamage());

        yield return StartCoroutine(Battle_Attack());
    }

    public IEnumerator Battle_Attack()
    {
        AddLog("Attack");

        BattleUnit bu = GetActiveBattler();

        if (AccuracyCheck())
        {
            bu.skillEffectID = gm.db.dbSkill[(int)bu.battleCharmling.skills[bu.selectedSkill].ID].effectID;

            AddLog("Skill Effect: " + bu.skillEffectID);

            string wild = gm.GetText("battle_prefix_wild");
            string enemy = gm.GetText("battle_prefix_enemy");

            yield return StartCoroutine(BattleMessage(string.Format(gm.GetText("battle_use"), !ActiveIsPlayer() ? wild : "", GetActiveBattler().battleCharmling.GetName(), gm.GetText(bu.battleCharmling.skills[bu.selectedSkill].ID.ToString() + "_NAME"))));

            if (skillEffects.GetType().GetMethod("bu.skillEffectID.ToUpper()") != null)
            {
                yield return skillEffects.StartCoroutine(bu.skillEffectID.ToUpper());   
            } else
            {
                yield return skillEffects.StartCoroutine("HIT");   
            }


        }
        else
        {
            // Attack missed!
        }

        yield return StartCoroutine(Battle_AfterAttack());
    }

    public IEnumerator Battle_ProcessDamage()
    {
        AddLog("Process Damage");

        GetActiveBattler().currSkillCrit = false;

        int dmg = CalcDamage();

        bool targetHasItem = GetTarget().battleCharmling.accessory != null;

        if (targetHasItem && gm.GetItemData(GetTarget().battleCharmling.accessory.ID).funcHold == ItemEffects_Hold.DAMAGECALC)
        {
            switch (GetTarget().battleCharmling.accessory.ID)
            {
                case ItemID.DAMAGEDSHIELD:
                    dmg = Mathf.RoundToInt(dmg * 0.5f);

                    // Message: Guarding with their broken shield!
                    break;
                default:
                    break;
            }

            GetTarget().itemUsed = true;
        }

        GetActiveBattler().currInflictingDmg = dmg;

        yield break;
    }

    public IEnumerator Battle_Hurt()
    {

        int currHP = GetTarget().battleCharmling.currentHP;
        GetTarget().prevHP = currHP;

        currHP -= GetActiveBattler().currInflictingDmg;

        if (currHP < 0) currHP = 0;

        GetTarget().battleCharmling.currentHP = currHP;

        AddLog("Hurt: " + GetActiveBattler().currInflictingDmg);
        AddLog("Prev: " + GetTarget().prevHP + ", Curr: " + currHP);
        yield break;
    }

    public IEnumerator Battle_ContactCheck()
    {
        // PERKCHECK: REDHOT

        SkillData sD = gm.db.dbSkill[(int)GetActiveBattler().battleCharmling.skills[GetActiveBattler().selectedSkill].ID];

        if (!sD.ranged)
        {
            if (GetTarget().battleCharmling.perk == PerkID.REDHOT && GetTarget().battleCharmling.status != StatusID.BURN)
            {
                GetActiveBattler().battleCharmling.status = StatusID.BURN;
                GetActiveBattler().HUDRefreshStatus();

                string wild = gm.GetText("battle_prefix_wild");
                string enemy = gm.GetText("battle_prefix_enemy");


                yield return StartCoroutine(BattleMessage(string.Format(
                    gm.GetText("battle_perk_redhot"),
                    ActiveIsPlayer() ? "" : wild,
                    GetActiveBattler().battleCharmling.GetName(),
                    ActiveIsPlayer() ? wild : "",
                    GetTarget().battleCharmling.GetName(),
                    gm.GetText(PerkID.REDHOT.ToString() + "_NAME")
                    )));
            }
        }

        yield break;
    }

    public IEnumerator Battle_AfterAttack()
    {
        AddLog("After Attack");

        if (GetActiveBattler().currSkillCrit)
        {
            // message
        }

        // Perk: second chance

        // do status damage;

        if (GetActiveBattler().selectedSkill != -1)
        {
            GetActiveBattler().cooldowns[GetActiveBattler().selectedSkill] = 0;

            // EX Skill
        }

        // Pass cooldowns
        for (int i = 0; i < GetActiveBattler().cooldowns.Length; i++)
        {
            BattleUnit active = GetActiveBattler();

            int maxcooldown = gm.db.dbSkill[(int)active.battleCharmling.skills[i].ID].cooldown;

            if (i != active.selectedSkill && active.cooldowns[i] != maxcooldown)
            {
                active.cooldowns[i]++;
            }

            if (ActiveIsPlayer()) buttonSkill[i].Refresh(i, active);
        }

        if (GetActiveBattler().battleCharmling.currentHP <= 0 || GetTarget().battleCharmling.currentHP <= 0)
        {
            // if opposing and last unit, end battle
            // else send new unit

            // if player and last unit, lose
            // else send new unit

            if (ActiveIsPlayer())
            {

            } else
            {
                yield return StartCoroutine(Battle_EndBattle());
            }

        }
        else
        {
            yield return StartCoroutine(Battle_EndTurn());
        }
    }

    public IEnumerator Battle_EndTurn()
    {
        BattleUnit active = GetActiveBattler();
        BattleUnit target = GetTarget();

        // Reset variables now that turn has passed

        active.currSkillCrit = false;

        active.turnOver = true;
        active.activeBattler = false;
        active.isTarget = true;

        target.activeBattler = true;
        target.isTarget = false;
        
        if (GetAttackersRemaining() == 0)
        {
            ResetUnits();

            //yield return StartCoroutine(Battle_SelectAction());
        }
        else
        {
            yield return StartCoroutine(Battle_PreAttack());
        }

    }

    public IEnumerator Battle_SwapOut()
    {
        BattleUnit active = GetActiveBattler();
        
        active.battleCharmling = SaveSystem.loaded.player.party[tempSendOutIndex];
        active.Init();
        active.HUDRefreshHP(false);
        active.HUDRefreshInfo();
        active.HUDRefreshStatus();

        active.activeBattler = true;

        tempSendOutIndex = -1;

        StartCoroutine(Battle_EndTurn());

        yield break;
    }

    public IEnumerator Battle_UseItem()
    {
        AddLog("Using Item: " + SaveSystem.loaded.player.GetInventoryList(tempPocketIndex)[tempItemIndex].ID.ToString());

        ItemData iD = gm.GetItemData(SaveSystem.loaded.player.GetInventoryList(tempPocketIndex)[tempItemIndex].ID);
        bool itemSuccess = false;

        switch (iD.funcBattle)
        {
            case ItemEffects_Battle.HEALING:
                switch (iD.ID)
                {
                    case ItemID.POTION:
                    case ItemID.POTIONHIGH:
                    case ItemID.POTIONULTRA:

                        if (GetActiveBattler().battleCharmling.currentHP == GetActiveBattler().battleCharmling.stats[(int)StatID.HEALTH])
                        {
                            UIMessage msg = gm.uiHandler.LoadMenu("UIMessage").GetComponent<UIMessage>();
                            msg.AddMessage("Already healed!");
                            msg.Refresh(true);

                            yield return new WaitForMessageClose(msg);

                            containerAction.SetActive(true);
                            gm.inputHandler.es.SetSelectedGameObject(buttonAction[1].gameObject);
                        }
                        else
                        {
                            GetActiveBattler().battleCharmling.Heal((int)iD.param);
                            StartCoroutine(GetActiveBattler().HUDRefreshHP(true));
                            GetActiveBattler().HUDRefreshInfo();
                            GetActiveBattler().HUDRefreshStatus();
                            itemSuccess = true;
                        }
                        break;
                    default:
                        break;
                }

                break;
            case ItemEffects_Battle.STATBOOST:
                break;
            case ItemEffects_Battle.TAME:
                yield return StartCoroutine(Battle_TameCharmling());
                break;
            default:
                break;
        }

        tempItemIndex = -1;

        if (itemSuccess)
        {
            yield return StartCoroutine(Battle_EndTurn());
        }

        yield break;
    }

    public IEnumerator Battle_TameCharmling()
    {
        Item selectedCharm = SaveSystem.loaded.player.GetInventoryList(tempPocketIndex)[tempItemIndex];
        float tameChance = 0.8f;

        if (Random.Range(0f, 1f) <= tameChance)
        {
            yield return StartCoroutine(BattleMessage(string.Format(gm.GetText("battle_tame_success"), SaveSystem.loaded.player.playerName, GetTarget().battleCharmling.GetName())));

            GetTarget().battleCharmling.charm = selectedCharm;
            CharmlingUtil.ObtainCharmling(GetTarget().battleCharmling);

            ItemUtil.ConsumeItem(tempPocketIndex, tempItemIndex, 1);

            yield return StartCoroutine(Battle_EndBattle());

        } else
        {
            yield return StartCoroutine(BattleMessage(string.Format(gm.GetText("battle_tame_fail"), GetTarget().battleCharmling.GetName())));

            yield return StartCoroutine(Battle_EndTurn());
        }

        yield break;
    }

    public IEnumerator Battle_EndBattle()
    {
        yield return StartCoroutine(gm.transitionHandler.FadeOut(UITransitionType.CIRCLE, () =>
        {
            ClearUnits();
            tempSendOutIndex = -1;

            containerAction.SetActive(false);
            containerMessage.SetActive(false);
        }, null, true));

        yield break;
    }

    public IEnumerator Battle_Faint()
    {
        yield break;
    }

    #region Utility
    public BattleUnit GetActiveBattler()
    {
        List<BattleUnit> activeUnits = new List<BattleUnit>();

        foreach (BattleUnit player in unitsPlayer)
        {
            if (player.activeBattler) activeUnits.Add(player);
        }

        foreach (BattleUnit opp in unitsOpposing)
        {
            if (opp.activeBattler) activeUnits.Add(opp);
        }

        if (activeUnits.Count > 1)
        {
            throw new Exception("More than one active battler at a time.");
        }

        if (activeUnits.Count == 0)
        {
            throw new Exception("No active battlers.");
        }

        return activeUnits[0];
    }

    public BattleUnit GetTarget()
    {
        List<BattleUnit> activeUnits = new List<BattleUnit>();

        foreach (BattleUnit player in unitsPlayer)
        {
            if (player.isTarget) activeUnits.Add(player);
        }

        foreach (BattleUnit opp in unitsOpposing)
        {
            if (opp.isTarget) activeUnits.Add(opp);
        }

        if (activeUnits.Count > 1)
        {
            throw new Exception("More than one target at a time.");
        }

        if (activeUnits.Count == 0)
        {
            throw new Exception("No targets.");
        }

        return activeUnits[0];
    }

    public void ResetUnits()
    {
        foreach (BattleUnit player in unitsPlayer)
        {
            player.isOpposing = false;
            player.isTarget = false;
            player.turnOver = false;
            player.activeBattler = false;
        }

        foreach (BattleUnit opp in unitsOpposing)
        {
            opp.isOpposing = false;
            opp.isTarget = false;
            opp.turnOver = false;
            opp.activeBattler = false;
        }
    }

    public bool ActiveIsPlayer()
    {
        return unitsPlayer.Contains(GetActiveBattler());
    }

    public int CalcDamage()
    {
        float multi = 1;

        SkillData sData = gm.db.dbSkill[(int)GetActiveBattler().battleCharmling.skills[GetActiveBattler().selectedSkill].ID];

        float attack;
        if (sData.ID == SkillID.MINDBEND)
        {
            attack = GetTarget().battleCharmling.GetStatValue(sData.skillType == SkillTypeID.STRENGTH ? StatID.STRENGTH : StatID.MAGIC);
        }
        else
        {
            attack = GetActiveBattler().battleCharmling.GetStatValue(sData.skillType == SkillTypeID.STRENGTH ? StatID.STRENGTH : StatID.MAGIC);
        }

        float guard = GetTarget().battleCharmling.GetStatValue(sData.skillType == SkillTypeID.STRENGTH ? StatID.DEFENSE : StatID.RESISTANCE);

        float level = GetActiveBattler().battleCharmling.level;
        float power = sData.power;

        int dmgnew = (int)(((2 * level / 5) + 2) * power * attack / guard / 50 + 2);

        int dmg = (int)Mathf.Ceil((((level * attack * power) / (250 * guard)) + 2) * multi);

        return dmgnew;
    }

    public bool AccuracyCheck()
    {
        float a = GetActiveBattler().accuracy;

        if (GetActiveBattler().battleCharmling.status == StatusID.DROWSY) a *= 0.7f;

        AddLog("Accuracy Check: " + a.ToString());

        return Random.Range(0f, 1f) <= a;
    }

    public int GetAttackersRemaining()
    {
        int remaining = 0;

        foreach (BattleUnit player in unitsPlayer)
        {
            if (!player.turnOver)
            {
                remaining++;
            }
        }

        foreach (BattleUnit opp in unitsOpposing)
        {
            if (!opp.turnOver)
            {
                remaining++;
            }
        }

        return remaining;
    }

    #endregion
}