using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static GameManager;

public class BattleButtonSkill : MonoBehaviour
{
    public Image iconAttr;
    public Image iconCooldown;

    public SuperTextMesh textSkill;
    public SuperTextMesh textCooldown;

    public Button button;

    private void Start()
    {
        button = GetComponent<Button>();
    }

    public void Refresh(int index, BattleUnit unit)
    {
        SkillData sd = gm.db.dbSkill[(int)unit.battleCharmling.skills[index].ID];

        textSkill.text = sd.ID.ToString();
        //textCooldown.text = (unit.battleCharmling.cooldowns[index] != sd.cooldown? (unit.battleCharmling.cooldowns[index].ToString() + "/") : "" ) + sd.cooldown.ToString();
    }
}
