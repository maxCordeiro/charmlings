using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleHUDUnit: MonoBehaviour
{
    [Header("Text")]
    public SuperTextMesh textName;
    public SuperTextMesh textLevel;
    public SuperTextMesh textHP;

    [Header("Icons")]
    public Image iconTamed;
    public Image iconGender;
    public Image iconColor;
    public Image iconStatus;

    [Header("Other")]
    public Image barHealth;
    public Image[] chargeBars;

    public Image imageBox;

    public Sprite[] spriteStatus;
}
