﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class BattleAnimation : MonoBehaviour
{
    public int playCount = 1;
	public BattleUI bCont;
    public BattleUnit user;
    public BattleUnit target;
	public SkillID id;
    public Animator animComp;

    private void Awake()
    {
        animComp = GetComponent<Animator>();
        animComp.StopPlayback();
        SetChildrenActive(false);
    }


    public void LoadAnim(BattleUI _bCont)
    {
        bCont = _bCont;
        bCont.AddLog("Playing Anim: BattleAnims/Anim_" + id);
    }

    public IEnumerator PlayAnim()
    {
        int p = playCount;

        while (p > 0)
        {
            animComp.SetTrigger("animStart");
            SetChildrenActive(true);
            yield return new WaitForFixedUpdate();

            float clipLength = animComp.GetCurrentAnimatorStateInfo(0).length;// + animComp.GetCurrentAnimatorStateInfo(0).normalizedTime;

            --p;

            yield return new WaitForSeconds(clipLength + 0.25f);
        }

        SetChildrenActive(false);
    }

    public virtual IEnumerator AnimProcess()
    {
        yield break;
    }

    public void SetOrientation(bool selfAnim)
    {
        transform.localScale = new Vector3(selfAnim ? user.transform.localScale.x : target.transform.localScale.x, 1, 1);
    }

    public void SetPosition(bool selfAnim)
    {
        transform.position = selfAnim ? user.transform.position : target.transform.position;
    }

    public void Event_Shake()
    {
        //StartCoroutine(bCont.Anim_Shake(false));
    }

    public void SetChildrenActive(bool active = true)
    {
        foreach (Transform obj in transform)
        {
            obj.gameObject.SetActive(active);
        }
    }
}