﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Anim_DEFAULT : BattleAnimation
{
    public void Start()
    {
        SetPosition(false);
    }

    public override IEnumerator AnimProcess()
    {
        yield return StartCoroutine(bCont.Anim_MotionClose(user));

        yield return StartCoroutine(PlayAnim());

        yield return StartCoroutine(bCont.Anim_MotionReturn(user));

        Destroy(gameObject);
    }
}