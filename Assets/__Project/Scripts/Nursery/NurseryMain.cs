﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public class NurseryDen
{
    public Charmling parentM, parentF, egg;
    public int eggStreak;
    public GameDate startDate;

    public bool hasEgg => egg != null;
    public bool isUnlocked = false;

    public void AddParent(int indexCharmling, bool inParty, int indexParent)
    {
        Charmling tempCharmling = inParty ? SaveSystem.loaded.player.party[indexCharmling] : SaveSystem.loaded.player.bagCharmlings[indexCharmling];

        AddParent(tempCharmling, indexParent);
    }

    public void AddParent(Charmling charmling, int indexParent)
    {
        int prevTotal = (parentF == null ? 0 : 1) + (parentM == null ? 0 : 1);

        if ((indexParent == 0 && parentF == null) && (indexParent == 1 && parentM == null))
        {
        }

        if (indexParent == 0)
        {
            parentF = charmling;
        }
        else
        {
            parentM = charmling;
        }

        int currTotal = (parentF == null ? 0 : 1) + (parentM == null ? 0 : 1);

        startDate = (prevTotal < 2 && currTotal == 2)? new GameDate() : null;
    }

    /*public void SwapCharmlings(int indexCharmling, bool inParty, int indexParent)
    {
        Charmling tempCharmling = inParty ? SaveSystem.loaded.player.party[indexCharmling] : SaveSystem.loaded.player.bagCharmlings[indexCharmling];

        if (inParty)
        {
            SaveSystem.loaded.player.party[indexCharmling] = indexParent == 0 ? parentM : parentF;
        } else
        {
            SaveSystem.loaded.player.bagCharmlings[indexCharmling] = indexParent == 0 ? parentM : parentF;
        }

        if (indexParent == 0)
        {
            parentM = tempCharmling;
        }
        else
        {
            parentF = tempCharmling;
        }
    }*/
}

[Serializable]
public class EggIncubator
{
    public Charmling parentM, parentF, egg;

    public GameDate dateHatch, dateCreate;
    public List<Item> itemsHatch, itemsBred;

    public bool readyToHatch;
}