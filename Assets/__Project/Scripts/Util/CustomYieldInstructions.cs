﻿using UnityEngine;

public class WaitForMessageClose : CustomYieldInstruction
{
    UIMessage msg;

    public override bool keepWaiting
    {
        get
        {
            return msg != null;
        }
    }

    public WaitForMessageClose(UIMessage _msg)
    {
        msg = _msg;
    }
}

public class WaitForMessageRead : CustomYieldInstruction
{
    UIMessage _m;

    public override bool keepWaiting
    {
        get
        {
            return !_m.isDoneReading;
        }
    }

    public WaitForMessageRead(UIMessage m)
    {
        _m = m;
    }
}

public class WaitForChoiceSelected : CustomYieldInstruction
{
    public override bool keepWaiting
    {
        get
        {
            return UIVariables.tempChoiceIndex == -1;
        }
    }
}

public class WaitForChoiceClose : CustomYieldInstruction
{
    UIChoices c;

    public WaitForChoiceClose(UIChoices _c)
    {
        c = _c;
    }

    public override bool keepWaiting
    {
        get
        {
            return c != null;
        }
    }

}

public class WaitForMenuClose : CustomYieldInstruction
{
    UIBase ui;

    public WaitForMenuClose(UIBase _ui)
    {
        ui = _ui;
    }

    public override bool keepWaiting
    {
        get
        {
            return ui != null;
        }
    }
}