﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public static class MoneyUtil
{
    // Returns true if there is a remainder when ADDING
    public static bool AddMoney(int amt)
    {
        int remainder = SaveSystem.loaded.player.money + amt > 999999 ? (SaveSystem.loaded.player.money + amt) - 999 : 0;

        SaveSystem.loaded.player.money = Mathf.Clamp(SaveSystem.loaded.player.money + amt, 0, 999999);

        return remainder > 0;
    }
}