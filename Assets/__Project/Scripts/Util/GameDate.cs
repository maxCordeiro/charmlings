﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Translation;
using UnityEngine;
using Random = UnityEngine.Random;
using static GameManager;

[Serializable]
public class GameDate
{
    public int dayInWeek, dayInMonth, month, year, hour, minute;
    public float time;

    public GameDate(int _dayInWeek = -1, int _dayInMonth = -1, int _month = -1, int _year = -1, int _hour = -1, int _minute = -1, bool addToCurrent = true)
    {
        dayInWeek = _dayInWeek == -1 ? SaveSystem.loaded.game.currDayIndex : _dayInWeek + (addToCurrent ? SaveSystem.loaded.game.currDayIndex : 0);
        dayInMonth = _dayInMonth == -1 ? SaveSystem.loaded.game.currDayInMonth : _dayInMonth + (addToCurrent ? SaveSystem.loaded.game.currDayInMonth : 0);
        month = _month == -1 ? SaveSystem.loaded.game.currMonth : _month + (addToCurrent ? SaveSystem.loaded.game.currMonth : 0);
        year = _year == -1 ? SaveSystem.loaded.game.currYear : _year + (addToCurrent ? SaveSystem.loaded.game.currYear : 0);

        hour = _hour == -1 ? SaveSystem.loaded.game.currHour : _hour + (addToCurrent ? SaveSystem.loaded.game.currHour : 0);
        minute = _minute == -1 ? SaveSystem.loaded.game.currMinute : _minute + (addToCurrent ? SaveSystem.loaded.game.currMinute : 0);
    }
	
    public override string ToString()
    {
        return string.Format("{0} {1}, Y{2}", dayInMonth + 1, ((MonthID)month).ToString().Substring(0, 3), year);
    }

}