using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using ScreenTransitionsPro;
using UnityEngine.SceneManagement;
using static GameManager;

public class TransitionHandler : MonoBehaviour
{
    public ScreenTransitionSimpleSoft sts;
    public float transitionTime;

    public List<Texture2D> textureTransition;

    private void Awake()
    {
        sts = Camera.main.GetComponent<ScreenTransitionSimpleSoft>();
        sts.SetMaterial(Instantiate(sts.transitionMaterial));

        if (gm.transitioning)
        {
            sts.cutoff = 1;
            StartCoroutine(FadeIn(UITransitionType.CIRCLE));
        }
    }

    //these 2 functions are jank, I think
    public IEnumerator SimpleFadeOut(UITransitionType transitionType)
    {
        sts.transitionMaterial.SetTexture("_Transition", textureTransition[(int)transitionType]);
        sts.falloff = (transitionType==UITransitionType.SOLID) ? 1f : 0.05f;
        yield return Transition(-0.9f, 1f);
    }
    
    public IEnumerator SimpleFadeIn(UITransitionType transitionType)
    {
        sts.transitionMaterial.SetTexture("_Transition", textureTransition[(int)transitionType]);
        sts.falloff = (transitionType==UITransitionType.SOLID) ? 1f : 0.05f;
        yield return Transition(1f, -0.9f);
    }

    public IEnumerator FadeOut(UITransitionType transitionType, Action onComplete = null, Action<float> onUpdate = null, bool autoFade = false)
    {
        sts.transitionMaterial.SetTexture("_Transition", textureTransition[(int)transitionType]);

        gm.transitioning = true;

        yield return StartCoroutine(Transition(0f, 1f, onUpdate));

        onComplete?.Invoke();

        if (autoFade) yield return StartCoroutine(FadeIn(transitionType));
    }

    public IEnumerator FadeOut(UITransitionType transitionType, string sceneToLoad = "", Action<float> onUpdate = null)
    {
        sts.transitionMaterial.SetTexture("_Transition", textureTransition[(int)transitionType]);

        gm.transitioning = true;

        yield return StartCoroutine(Transition(0f, 1f, onUpdate));

        if (sceneToLoad.Length > 0)
        {
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneToLoad);

            while (!asyncLoad.isDone)
            {
                yield return null;
            }
        }
    }

    public IEnumerator FadeIn(UITransitionType transitionType, Action onComplete = null)
    {
        sts.transitionMaterial.SetTexture("_Transition", textureTransition[(int)transitionType]);

        yield return StartCoroutine(Transition(1f, 0f));
        sts.transitioning = false;

        onComplete?.Invoke();

        gm.transitioning = false;
    }

    public IEnumerator Transition(float from, float to, Action<float> onUpdate = null)
    {
        sts.transitioning = true;
        LeanTween.value(from, to, transitionTime).setOnUpdate((float val) => {
            sts.cutoff = val;
            onUpdate?.Invoke(val);
        });
        yield return new WaitForSeconds(transitionTime + 0.15f);
    }

}
