﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

[Serializable]
public class SkillData
{
    [XmlAttribute("ID")]
    public SkillID ID;

    public int power;
    public int cooldown;

    public bool ranged;
    public SkillTypeID skillType;

    public CharmlingIdentifier exclusiveCharmling;
    public AttributeID attribute;

    public string effectID;

    public int priorityLevel;
    public RarityID rarity;
    public int sellPrice;

    public bool isEX;

    public SkillData()
    {

    }
}