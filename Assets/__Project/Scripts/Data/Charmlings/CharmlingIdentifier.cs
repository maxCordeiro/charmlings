﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

[Serializable]
#pragma warning disable CS0660 // Type defines operator == or operator != but does not override Object.Equals(object o)
#pragma warning disable CS0661 // Type defines operator == or operator != but does not override Object.GetHashCode()
public struct CharmlingIdentifier
#pragma warning restore CS0661 // Type defines operator == or operator != but does not override Object.GetHashCode()
#pragma warning restore CS0660 // Type defines operator == or operator != but does not override Object.Equals(object o)
{
    [XmlAttribute("ID")]
    public CharmlingID ID;
    public int formID;
    public ColorID charmlingColor;

    public CharmlingIdentifier(CharmlingID _ID, int _formID, ColorID _charmlingColor = ColorID.GENERATE)
    {
        ID = _ID;
        formID = _formID;
        charmlingColor = _charmlingColor;
    }

    public static bool operator == (CharmlingIdentifier a, CharmlingIdentifier b)
    {
        bool checkID = a.ID == b.ID;
        bool checkForm = a.formID == b.formID;
        bool checkColor = (a.charmlingColor == ColorID.GENERATE || b.charmlingColor == ColorID.GENERATE);

        return checkID && checkForm && (checkColor || a.charmlingColor == b.charmlingColor);
    }

    public static bool operator != (CharmlingIdentifier a, CharmlingIdentifier b)
    {
        return (a.ID != b.ID && a.formID != b.formID && a.charmlingColor != b.charmlingColor);
    }

}