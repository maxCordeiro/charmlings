using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public class Trait
{
    public TraitID ID;
    public bool plus;

    public Trait()
    {
        ID = (TraitID)Random.Range(0, 24);

        plus = Random.Range(0f, 1f) <= 0.15f ? true : false;
    }
}