﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

[Serializable]
public struct AttributeData
{
    [XmlAttribute("ID")]
    public AttributeID ID;

    public List<AttributeID> weak;
    public List<AttributeID> strong;
    public List<AttributeID> resist;
    public List<AttributeID> immune;
}