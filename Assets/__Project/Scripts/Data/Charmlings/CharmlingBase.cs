﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using static GameManager;
using Random = UnityEngine.Random;

public class CharmlingBase
{
	public CharmlingIdentifier ID;

	public PerkID perk;
	public Trait trait;

	public int[] stats = new int[6];
	public int[] wonderPoints = new int[6];
	public int[] baseStats = new int[6];
	public int currentHP;
	public int affection;

	public ColorID color;

	public string nickname;
	public bool hasNickname => nickname.Length > 0;

	public int level;

	public GenderID gender;
	public StatusID status;

	public Item accessory;

	public Skill[] skills;
	public Skill exSkill;

	public void GenerateBaseStats()
    {
		for (int i = 0; i < baseStats.Length; i++)
        {
			baseStats[i] = Random.Range(1, 50);
        }
    }

	public void CalculateStat(int statIndex)
	{
		CharmlingData tempData = gm.GetCharmlingData(ID);
		TraitData td = gm.db.dbTrait[(int)trait.ID];

		int stat = 0;

		stat = (2 * tempData.baseStats.baseStatArray[statIndex] + baseStats[statIndex] + (wonderPoints[statIndex]/4)) * level / 100 + 5;
		
		if ((int)td.statUp == statIndex)
        {
			stat = Mathf.RoundToInt(stat * (trait.plus ? 1.25f : 1.15f));
        } else if ((int)td.statDown == statIndex)
        {
			stat = Mathf.RoundToInt(stat * 0.85f);
        }

		stats[statIndex] = stat;

		// Old Formula
		// Mathf.FloorToInt(((2.82f * (tempData.baseStats.baseStatArray[statIndex] * level * (1 + (wonderPoints[statIndex] * 0.0062f))) + 5.5f) / 100 + 2.5f) * 1);
	}

	public void CalculateAllStats(bool heal = true)
	{
		for (int i = 0; i < stats.Length; i++)
		{
			CalculateStat(i);
		}

		if (heal) HealFull();
	}


	public int GetStatValue(StatID stat)
	{
		int val = stats[(int)stat];

		/*TraitData td = gm.db.dbTrait[(int)trait.ID];

		if (td.statUp == stat)
		{
			val = Mathf.RoundToInt(val * (trait.plus? 1.25f : 1.15f));
		}
		else if (td.statDown == stat)
		{
			val = Mathf.RoundToInt(val * 0.85f);
		}*/

		return val;
	}

	public int GetTotalWP()
    {
		int total = 0;

		foreach (int wp in wonderPoints)
        {
			total += wp;
        }

		return total;
    }

	public bool Heal(int amt)
	{
		if (currentHP == GetStatValue(StatID.HEALTH) || status == StatusID.KO)
		{
			return false;
		}

		currentHP = Mathf.Clamp(currentHP + amt, 1, GetStatValue(StatID.HEALTH));
		return true;
	}

	public void HealHalf()
    {
		currentHP = Mathf.RoundToInt(GetStatValue(StatID.HEALTH) * 0.5f);
    }
	public void HealFull()
	{
		currentHP = GetStatValue(StatID.HEALTH);
	}

	public void Hurt(int amt)
	{
		currentHP -= amt;

		if (currentHP < 0) currentHP = 0;
	}

	public int GetHighestStat()
	{
		int maxStat = stats[0];

		for (int i = 0; i < stats.Length; i++)
		{
			if (stats[i] > maxStat)
			{
				maxStat = stats[i];
			}
		}

		return maxStat;
	}

	public virtual Sprite GetIcon()
	{
		Sprite[] s = Resources.LoadAll<Sprite>("Charmlings/Icons/" + ID.ID.ToString() + "_" + ID.formID.ToString());
		Sprite[] debug = Resources.LoadAll<Sprite>("Charmlings/Icons/DEBUG");

		return s.Length == 0? debug[(int)color] : s[(int)color];
	}

	public Sprite GetSprite()
    {
		Sprite[] s = Resources.LoadAll<Sprite>("Charmlings/Battle/" + ID.ID.ToString() + "_" + ID.formID.ToString());
		Sprite debug = Resources.Load<Sprite>("Charmlings/Battle/NONE");

		return s.Length == 0 ? debug : s[(int)color];
    }

	public string GetHeight()
    {
		CharmlingData cD = gm.GetCharmlingData(ID);

		int inches = Mathf.FloorToInt(cD.baseHeight % 1);
		int feet = Mathf.FloorToInt(cD.baseHeight);

		return $"{feet}' {inches}\"";
    }

	public string GetWeight()
    {
		CharmlingData cD = gm.GetCharmlingData(ID);

		return $"{cD.baseWeight} lbs.";
    }

	public string GetName() => hasNickname ? nickname : GetSpeciesName();
	public string GetSpeciesName() => gm.GetText($"{ID.ID}_{ID.formID}_NAME");

	public string PrintGender() => gender == GenderID.FEMALE ? "F" : gender == GenderID.UNKNOWN ? "" : "M";

	public override string ToString()
	{
		string output = "";

		output += GetName() + " [" + PrintGender() + "]\n";

		output += status.ToString() + "\n";
		output += string.Format("Item: {0}\n", accessory != null ? accessory.ToString() : "NONE");

		output += perk + "\n";
		output += string.Format("{0}{1}\n", trait.ID.ToString(), trait.plus ? "+" : "");

		output += "Color: " + color.ToString() + "\n";

		for (int i = 0; i < stats.Length; i++)
		{
			output += string.Format("{0}: {1} [{2}]\n", ((StatID)i).ToString(), stats[i], wonderPoints[i]);
		}

		output += "Current HP: " + currentHP + "\n";

		for (int i = 0; i < skills.Length; i++)
		{
			output += string.Format("Skill: {0} [{1}]\n", skills[i].ID.ToString(), skills[i].cooldownMultiplier.ToString());
		}

		output += "EX: " + (exSkill != null ? exSkill.ID.ToString() : "NONE") + "\n";

		return output;
	}
}