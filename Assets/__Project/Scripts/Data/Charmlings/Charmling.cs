using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GameManager;

public class Charmling : CharmlingBase
{
	public float expTotal;
	public float expCurrent;
	public float expNext;

	public bool isEgg;
	public int daysToHatch;

	public int weight;
	public float height;
	public int weightMulti;
	public int heightMulti;


	// To be defined in obtain method
	public Item charm;
	public string tamerName = "";
	public string tamerID = "";

	public MapID obtainedMap = MapID.NONE;
	public int obtainedLevel = 0;
	public GameDate obtainedDate = null;

	public Charmling(CharmlingIdentifier _id)
    {
		ID = _id;

		CharmlingData tempData = gm.GetCharmlingData(_id);

		nickname = "";

		perk = tempData.uniquePerk != PerkID.NONE? tempData.uniquePerk : PerkUtil.GeneratePerk(tempData.family);
				
		color = _id.charmlingColor == ColorID.GENERATE? (ColorID)(int)gm.Choose(GameManager.CHANCECOMMON, GameManager.CHANCEUNCOMMON, GameManager.CHANCERARE, GameManager.CHANCEULTRARARE) : _id.charmlingColor;

		level = 1;

		trait = new Trait();

		gender = Random.Range(0f, 1f) <= 0.5f ? GenderID.FEMALE : GenderID.MALE;

		status = StatusID.NONE;

		GenerateBaseStats();
		CalculateAllStats();

		skills = SkillsetUtil.GenerateMoveset(ID);
		exSkill = new Skill(SkillID.NONE);

		charm = new Item(ItemID.NEUTRALCHARM);

		//Set OT
		obtainedMap = SaveSystem.loaded.player.activeMap;
		obtainedLevel = level;
		obtainedDate = new GameDate();
    }

	public void GainEXP(float expAmount, bool levelUp = true)
	{
		expCurrent += expAmount;

		if (!levelUp) return;

		while (expCurrent >= expNext)
		{
			LevelUp();
		}
	}

	public void LevelUp()
	{
		expCurrent -= expNext;
		level++;

		CalculateAllStats(false);

		CalculateEXP();
	}

	public void CalculateEXP()
	{
		int nextLevel = (int)(Mathf.FloorToInt((level + 3) * Mathf.Pow(level + 1, 0.85f) * (Mathf.Sqrt(75) / 12) * 0.3f) * Mathf.Pow(level + 1, 1.35f));
		int curr = (int)(Mathf.FloorToInt((level + 2) * Mathf.Pow(level + 1, 0.85f) * (Mathf.Sqrt(75) / 12) * 0.3f) * Mathf.Pow(level, 1.35f));

		expNext = nextLevel - curr;
		expTotal = curr;
	}

	// Called on loading DB
	public void RecalculateBase()
	{
		for (int i = 0; i < stats.Length; i++)
		{
			CalculateStat(i);
			CalculateEXP();
		}
	}

	public override Sprite GetIcon()
    {
		if (isEgg)
        {
			// need to select the proper egg sprite based off family. returns 0 for now
			Sprite[] s = Resources.LoadAll<Sprite>("Charmlings/eggs");

			return s[0];
		}
		else
        {
			return base.GetIcon();
        }
    }

	public override string ToString()
    {
		string output = "";

		output += base.ToString();

		if (isEgg)
        {
			output += "Egg\n";
			output += "Days to hatch: " + daysToHatch.ToString();
        } else
        {
			output += string.Format("Lv {0}", level + "\n");
			output += string.Format("XP: {0}/{1} ({2})\n", expCurrent, expNext, expTotal);
        }

		output += "Charm: " + (charm != null? charm.ID.ToString() : "NONE") + "\n";
		output += "Weight: " + weight.ToString() + "\n";
		output += "Height: " + height.ToString() + "\n";

		output += string.Format("Tamed by: {0} [{1}]\n", tamerName, tamerID);
		output += string.Format("Met in {0} at Lv{1} on {2}\n", obtainedMap.ToString(), obtainedLevel.ToString(), obtainedDate.ToString());
		
		return output;
    }
}