﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

[Serializable]
public struct TraitData
{
    [XmlAttribute("ID")]
    public TraitID ID;

    public StatID statUp;
    public StatID statDown;
}