﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using static GameManager;

public static class CharmlingUtil
{
    public static bool ObtainCharmling(Charmling c, bool toBag = false)
    {
        c.tamerName = SaveSystem.loaded.player.playerName;
        c.tamerID = SaveSystem.loaded.player.playerID;

        c.obtainedMap = SaveSystem.loaded.player.activeMap;
        c.obtainedDate = new GameDate();
        c.obtainedLevel = c.level;

        if (SaveSystem.loaded.player.party.Count == 4 || toBag)
        {
            int index = gm.GetFirstNull(SaveSystem.loaded.player.bagCharmlings);

            if (index == -1) return false;

            SaveSystem.loaded.player.bagCharmlings[index] = c;
            return true;
        } else
        {
            SaveSystem.loaded.player.party.Add(c);
            return true;
        }
    }
}