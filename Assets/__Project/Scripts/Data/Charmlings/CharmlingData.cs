﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

[Serializable]
public class CharmlingData
{
    public BaseStatsData baseStats = new BaseStatsData();

    [XmlAttribute("ID")]
    public CharmlingID ID;
    public int form;

    public FamilyID family;

    public float baseHeight;
    public int baseWeight;

    public AttributeID attr;
    public AttributeID secAttr;

    public GrowthRateID growthRate;

    public List<ItemID> dropsCommon;
    public List<ItemID> dropsUncommon;
    public List<ItemID> dropsRare;

    public float dropRate;
    public float tameRate;

    public PerkID uniquePerk = PerkID.NONE;
}