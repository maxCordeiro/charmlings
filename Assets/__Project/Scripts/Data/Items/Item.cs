﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using static GameManager;

public class Item
{
    public ItemID ID;
    public int secID;
    public int currQuantity = 1;
    public int currUses = -1;

    public object uniqueParam;

    // To check if it's currently selected in the Inventory scroller
    public bool isSelected;

    public Item(ItemID _id, int _secID = 0)
    {
        ID = _id;
        secID = _secID;
        ItemData tempData = gm.GetItemData(_id, secID);

        if (tempData.uses != -1)
        {
            currUses = tempData.uses;
        }
    }
    public Item(ItemID _id, object _uniqueParam, int _secID = 0)
    {
        ID = _id;
        secID = _secID;
        ItemData tempData = gm.GetItemData(_id, secID);

        if (tempData.uses != -1)
        {
            currUses = tempData.uses;
        }

        uniqueParam = _uniqueParam;
    }

    Item()
    {

    }

    public Sprite GetIcon()
    {
        Sprite s = Resources.Load<Sprite>("Items/Icons/" + ID.ToString() + "_" + secID.ToString());
        Sprite debug = Resources.Load <Sprite>("Items/Icons/debug");

        return s == null ? debug : s;
    }

    public string GetName()
    {
        return gm.GetText(ID.ToString() + "_NAME");
    }
    public string GetDesc()
    {
        return gm.GetText(ID.ToString() + "_DESC");
    }
}