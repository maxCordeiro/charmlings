﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using static GameManager;

public static class ItemEffects
{
    public static void DoMenuItemEffect(Item item, UIInventory inv)
    {
        switch (item.ID)
        {
            case ItemID.NONE:
                break;
            case ItemID.POTION:
            case ItemID.POTIONHIGH:
            case ItemID.POTIONULTRA:
                HealingEffect(item, inv);
                break;
            case ItemID.REPEL:
                break;
            case ItemID.LUREHEAVY:
                break;
            case ItemID.LURELIGHT:
                break;
            case ItemID.LUREMAGNETIC:
                break;
            case ItemID.LURE:
                break;
            case ItemID.CHARACTERINCENSE:
                break;
            case ItemID.TRAITINCENSE:
                break;
            case ItemID.POWERINCENSE:
                break;
            case ItemID.DAMAGEDSHIELD:
                break;
            case ItemID.OVERCLOCKMODULE:
                break;
            case ItemID.AMNESIAELIXIR:
                break;
            case ItemID.DENSEDEW:
                break;
            case ItemID.GLISTENINGPETAL:
                break;
            case ItemID.HEATEDCOAL:
                break;
            case ItemID.HEXCARD:
                break;
            case ItemID.LOOSECABLE:
                break;
            case ItemID.SILVERFEATHER:
                break;
            case ItemID.MYSTERIOUSCLOAK:
                break;
            case ItemID.POLISHEDGEAR:
                break;
            case ItemID.SMOOTHSTONE:
                break;
            case ItemID.ORDINARYBELT:
                break;
            case ItemID.COOLINGLEAF:
            case ItemID.SERENEFLUTE:
            case ItemID.DIVINETOME:
            case ItemID.RADIANTBELL:
                StatusEffect(item, inv);
                break;
            case ItemID.REVIVE:
                ReviveEffect(item, inv);
                break;
            case ItemID.EXPGOLD:
                break;
            case ItemID.SKILLCAPSULE:
                break;
            case ItemID.LOCKEDCHARM:
                break;
            case ItemID.GOODDRINK:
                break;
            case ItemID.NEUTRALCHARM:
                break;
            case ItemID.SHARPTOOTH:
                break;
            case ItemID.WATERCAPVOUCHER:
                break;
            case ItemID.PLANTCAPVOUCHER:
                break;
            case ItemID.FIRECAPVOUCHER:
                break;
            case ItemID.COSMICCAPVOUCHER:
                break;
            case ItemID.ELECTRICCAPVOUCHER:
                break;
            case ItemID.WINDCAPVOUCHER:
                break;
            case ItemID.SHADOWCAPVOUCHER:
                break;
            case ItemID.TECHCAPVOUCHER:
                break;
            case ItemID.EARTHCAPVOUCHER:
                break;
            case ItemID.NORMALCAPVOUCHER:
                break;
            case ItemID.COMMONCAPVOUCHER:
                break;
            case ItemID.UNCOMMONCAPVOUCHER:
                break;
            case ItemID.RARECAPVOUCHER:
                break;
            case ItemID.ULTRARARECAPVOUCHER:
                break;
            case ItemID.BATTERYPACK:
                break;
            case ItemID.GRASSLANDSTIERAITEM:
                break;
            case ItemID.GRASSLANDSTIERBITEM:
                break;
            case ItemID.GRASSLANDSTIERCITEM:
                break;
            case ItemID.GUARDIANSHARDS:
                break;
            case ItemID.HOTSNOT:
                break;
            case ItemID.PINBERRY:
                break;
            case ItemID.TAYABERRY:
                break;
            case ItemID.LOKUBERRY:
                break;
            case ItemID.ROWBERRY:
                break;
            case ItemID.NOCOBERRY:
                break;
            case ItemID.SIMMBERRY:
                break;
            case ItemID.WATTOMBERRY:
                break;
            case ItemID.MAROBERRY:
                break;
            case ItemID.JAKBERRY:
                break;
            case ItemID.ANGABERRY:
                break;
            case ItemID.CLEMBERRY:
                break;
            case ItemID.WOGBERRY:
                break;
            case ItemID.PINCANDY:
            case ItemID.TAYACANDY:
            case ItemID.LOKUCANDY:
            case ItemID.ROWCANDY:
            case ItemID.NOCOCANDY:
            case ItemID.SIMMCANDY:
                WonderPointEffect(1, item, inv);
                break;
            case ItemID.WATTOMCANDY:
            case ItemID.MAROCANDY:
            case ItemID.JACKCANDY:
            case ItemID.ANGACANDY:
            case ItemID.CLEMCANDY:
            case ItemID.WOGCANDY:
                WonderPointEffect(-1, item, inv);
                break;
            case ItemID.PINSEED:
                break;
            case ItemID.TAYASEED:
                break;
            case ItemID.LOKUSEED:
                break;
            case ItemID.ROWSEED:
                break;
            case ItemID.NOCOSEED:
                break;
            case ItemID.SIMMSEED:
                break;
            case ItemID.WATTOMSEED:
                break;
            case ItemID.MAROSEED:
                break;
            case ItemID.JAKSEED:
                break;
            case ItemID.ANGASEED:
                break;
            case ItemID.CLEMSEED:
                break;
            case ItemID.WOGSEED:
                break;
        }
    }

    public static void HealingEffect(Item item, UIInventory inv)
    {
        Charmling partyMon = SaveSystem.loaded.player.party[UIVariables.tempCharmlingInventoryIndex];

        if (partyMon.currentHP == partyMon.GetStatValue(StatID.HEALTH))
        {
            ItemNoEffect();
        } else
        {
            ItemData iD = gm.GetItemData(item.ID);
            partyMon.Heal((int)iD.param);

            inv.RefreshParty();
            UIVariables.itemEffectReturn = UIVariables.ITEMEFFECT_SUCCESS;
        }
    }

    public static void StatusEffect(Item item, UIInventory inv)
    {
        ItemData iD = gm.GetItemData(item.ID);
        Charmling partyMon = SaveSystem.loaded.player.party[UIVariables.tempCharmlingInventoryIndex];

        if (partyMon.status == StatusID.NONE || partyMon.status != (StatusID)iD.param)
        {
            ItemNoEffect();
        } else
        {
            partyMon.status = StatusID.NONE;
            inv.RefreshParty();
            UIVariables.itemEffectReturn = UIVariables.ITEMEFFECT_SUCCESS;
        }
    }

    public static void ReviveEffect(Item item, UIInventory inv)
    {
        Charmling partyMon = SaveSystem.loaded.player.party[UIVariables.tempCharmlingInventoryIndex];

        if (partyMon.status != StatusID.KO)
        {
            ItemNoEffect();
        } else
        {
            partyMon.status = StatusID.NONE;
            partyMon.HealHalf();
            inv.RefreshParty();

            UIMessage msg = gm.uiHandler.LoadMenu(UIVariables.UIMESSAGE).GetComponent<UIMessage>();
            msg.AddMessage(gm.GetText("inv_revive"));
            msg.onClose += () =>
            {
                UIVariables.itemEffectReturn = UIVariables.ITEMEFFECT_SUCCESS;
            };
            msg.Refresh(true);

        }
    }

    public static void WonderPointEffect(int amt, Item item, UIInventory inv)
    {
        UIMessage msg = gm.uiHandler.LoadMenu(UIVariables.UIMESSAGE).GetComponent<UIMessage>();

        ItemData iD = gm.GetItemData(item.ID);
        Charmling partymon = SaveSystem.loaded.player.party[UIVariables.tempCharmlingInventoryIndex];

        StatID stat = (StatID)iD.param;

        int wpBefore = partymon.wonderPoints[(int)stat];
        int wpAfter;
        int wpTotal = partymon.GetTotalWP();
        int wpRemainder;

        if (amt > 0)
        {
            if (partymon.wonderPoints[(int)stat] >= 250)
            {
                msg.AddMessage(string.Format(gm.GetText("inv_wpFull"), partymon.GetName(), gm.GetText("sys_" + stat.ToString())));
                msg.onClose += () =>
                {
                    UIVariables.itemEffectReturn = UIVariables.ITEMEFFECT_CANCEL;
                };
                msg.Refresh(true);
                return;
            }
            else if (wpTotal == 500)
            {
                msg.AddMessage(string.Format(gm.GetText("inv_wpFullTotal"), partymon.GetName(), gm.GetText("sys_" + stat.ToString())));
                msg.onClose += () =>
                {
                    UIVariables.itemEffectReturn = UIVariables.ITEMEFFECT_CANCEL;
                };
                msg.Refresh(true);
                return;
            }
        }

        partymon.wonderPoints[(int)stat] += amt;
        partymon.CalculateAllStats(false);

        wpAfter = partymon.wonderPoints[(int)stat];

        wpRemainder = wpAfter - wpBefore;

        if (wpRemainder > 0)
        {
            msg.AddMessage(string.Format(gm.GetText("inv_wpIncrease"), partymon.GetName(), gm.GetText("sys_" + stat.ToString()), wpRemainder));
        }
        else if (wpRemainder < 0)
        {
            msg.AddMessage(string.Format(gm.GetText("inv_wpDecrease"), partymon.GetName(), gm.GetText("sys_" + stat.ToString()), Mathf.Abs(wpRemainder)));
        } else
        {
            msg.AddMessage(string.Format(gm.GetText("inv_wpNoChange"), partymon.GetName(), gm.GetText("sys_" + stat.ToString())));
        }

        msg.AddMessage(string.Format(gm.GetText("inv_wpResult"), partymon.GetName(), wpAfter, gm.GetText("sys_" + stat.ToString()), partymon.GetStatValue(stat)));
            
        msg.onClose += () =>
        {
            UIVariables.itemEffectReturn = UIVariables.ITEMEFFECT_SUCCESS;
        };

        inv.RefreshParty();
            
        msg.Refresh(true);
    }

    public static void ItemNoEffect()
    {
        UIMessage msg = gm.uiHandler.LoadMenu(UIVariables.UIMESSAGE).GetComponent<UIMessage>();
        msg.AddMessage(gm.GetText("inv_noEffect"));
        msg.onClose += () =>
        {
            UIVariables.itemEffectReturn = UIVariables.ITEMEFFECT_CANCEL;
        };
        msg.Refresh(true);
    }

    public static int RepelEffect(Item item, UIInventory inv)
    {
        return UIVariables.ITEMEFFECT_ERROR;
    }
}