﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

[Serializable]
public class ItemData
{
    [XmlAttribute("ID")]
    public ItemID ID;
    public int secID;

    public BagPockets pocket;

    public bool stackable = true;
    public int stackAmount = GameManager.MAXITEMSTACK;

    public int price;

    public ItemEffects_Hold funcHold;
    public ItemEffects_Menu funcMenu;
    public ItemEffects_Battle funcBattle;

    public int uses;

    public object param;
}