﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using static GameManager;

public static class ItemUtil
{
    public static bool ObtainItem(ItemID item, int quantity = 1, int secID = 0)
    {
        Item addItem = new Item(item, secID);

        return ObtainItem(addItem, quantity, secID);
    }

    public static bool ObtainItem(Item item, int quantity = 1, int _secID = 0)
    {
        if (quantity > 999 || item.currQuantity > 999) return false;

        item.currQuantity = quantity;

        ItemData tempData = gm.GetItemData(item.ID, _secID);

        if (tempData.stackable)
        {
            for (int i = 0; i < SaveSystem.loaded.player.GetInventoryList(tempData.pocket).Count; i++)
            {
                Item currentItem = SaveSystem.loaded.player.GetInventoryList(tempData.pocket)[i]; // Current item in iteration
                ItemData currentData = gm.GetItemData(currentItem.ID, currentItem.secID);

                if (currentItem.ID == item.ID && currentItem.secID == item.secID && Equals(currentItem.uniqueParam, item.uniqueParam))
                {
                    if (currentItem.currQuantity + item.currQuantity > 999)
                    {
                        return false;
                    } else
                    {
                        currentItem.currQuantity += item.currQuantity;
                        return true;
                    }
                }
            }

            SaveSystem.loaded.player.GetInventoryList(tempData.pocket).Add(item);
            return true;
        } else
        {
            if (SaveSystem.loaded.player.GetInventoryList(tempData.pocket).Where(x => x.ID == item.ID && x.secID == item.secID && Equals(x.uniqueParam, item.uniqueParam)).Count() > 0)
            {
                return false;
            } else
            {
                SaveSystem.loaded.player.GetInventoryList(tempData.pocket).Add(item);
                return true;
            }
        }
    }

    public static int CountItem(ItemID item, int secID = 0)
    {
        ItemData tempData = gm.GetItemData(item, secID);
        Item foundItem = SaveSystem.loaded.player.GetInventoryList(tempData.pocket).Find(x => x.ID == item && x.secID == secID);

        return foundItem == null? 0 : foundItem.currQuantity;
    }

    /* 1 = Success
     * 2 = Not enough money
     * 3 = Not enough room
     */
    public static int BuyItem(ItemID item, int quantity = 1, int secID = 0)
    {
        ItemData tempData = gm.GetItemData(item, secID);

        if (SaveSystem.loaded.player.money >= tempData.price)
        {
            if (ObtainItem(item, quantity, secID))
            {
                MoneyUtil.AddMoney(-tempData.price);
                return 1;
            }
            else
            {
                return 3;
            }
        }
        else
        {
            return 2;
        }
    }

    // 1 = Consumed
    // 2 = Tossed
    public static int ConsumeItem(BagPockets pocket, int index, int amount)
    {
        SaveSystem.loaded.player.GetInventoryList(pocket)[index].currQuantity -= amount;

        if (SaveSystem.loaded.player.GetInventoryList(pocket)[index].currQuantity <= 0)
        {
            TossItem(pocket, index);
            return 2;
        }

        return 1;
    }

    public static void TossItem(BagPockets pocket, int index)
    {
        SaveSystem.loaded.player.GetInventoryList(pocket).RemoveAt(index);
    }

    public static Sprite GetIcon(ItemID ID, int secID = 0)
    {
        Sprite s = Resources.Load<Sprite>("Items/Icons/" + ID.ToString() + "_" + secID.ToString());
        Sprite debug = Resources.Load<Sprite>("Items/Icons/debug");

        return s == null ? debug : s;
    }
    public static string GetName(ItemID ID)
    {
        return gm.GetText(ID.ToString() + "_NAME");
    }
    public static string GetDesc(ItemID ID)
    {
        return gm.GetText(ID.ToString() + "_DESC");
    }
}