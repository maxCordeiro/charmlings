﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GameManager;

public static class SkillsetUtil
{
    public static Skill[] GenerateMoveset(CharmlingIdentifier _charmling)
    {
		Skill[] tempSet = new Skill[5];
		SkillsetData tempData = gm.GetSkillsetData(_charmling);

		if (tempData == null || tempData.canLearn.Count < 5)
		{
			tempSet[0] = new Skill(SkillID.BOUNDINGZAP);
			tempSet[1] = new Skill(SkillID.BOUNDINGZAP);
			tempSet[2] = new Skill(SkillID.BOUNDINGZAP);
			tempSet[3] = new Skill(SkillID.BOUNDINGZAP);
			tempSet[4] = new Skill(SkillID.BOUNDINGZAP);
		}
		else
		{
			tempSet[0] = new Skill(tempData.canLearn[0]);
			tempSet[1] = new Skill(tempData.canLearn[1]);
			tempSet[2] = new Skill(tempData.canLearn[2]);
			tempSet[3] = new Skill(tempData.canLearn[3]);
			tempSet[4] = new Skill(tempData.canLearn[4]);
		}

		return tempSet;
	}
}