﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class SkillsetData
{
    public CharmlingIdentifier charmling;
    public List<SkillID> canLearn;

    public List<SkillsetSkill> canEquip;
}

public struct SkillsetSkill
{
    public int level;
    public SkillID skill;
}