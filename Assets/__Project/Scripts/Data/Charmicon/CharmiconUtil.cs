﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GameManager;

public static class CharmiconUtil
{
    public static bool HasSeenCharmling(CharmlingIdentifier _charmling)
    {
        return SaveSystem.loaded.game.charmiconCharmlings.Any(x => x.charmling == _charmling && x.seen);
    }

    public static bool HasTamedCharmling(CharmlingIdentifier _charmling)
    {
        return SaveSystem.loaded.game.charmiconCharmlings.Any(x => x.charmling == _charmling && x.tamed);
    }

    public static void SeeCharmling(CharmlingIdentifier _charmling)
    {
        if (!HasSeenCharmling(_charmling))
        {
            CharmiconCharmling cc = new CharmiconCharmling()
            {
                charmling = _charmling,
                seen = true
            };

            SaveSystem.loaded.game.charmiconCharmlings.Add(cc);
        }
    }

    public static void TameCharmling(CharmlingIdentifier _charmling)
    {
        SeeCharmling(_charmling);

        if (!HasTamedCharmling(_charmling))
        {
            SaveSystem.loaded.game.charmiconCharmlings.SingleOrDefault(x => x.charmling == _charmling).tamed = true;
        }
    }

    public static bool HasDiscoveredMap(MapID _map)
    {
        return SaveSystem.loaded.game.charmiconMaps.Any(x => x.map == _map);
    }

    public static void DiscoverMap(MapID _map)
    {
        if (!HasDiscoveredMap(_map))
        {
            CharmiconMap cm = new CharmiconMap()
            {
                map = _map,
                discovered = true
            };

            SaveSystem.loaded.game.charmiconMaps.Add(cm);
        }
    }
}