﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class CharmiconCharmling
{
    public CharmlingIdentifier charmling;
    public bool seen, tamed;
}

public class CharmiconMap
{
    public MapID map;
    public bool discovered;
}