﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Random = UnityEngine.Random;
using static GameManager;

public class PerkUtil
{
    public static PerkID GeneratePerk(FamilyID family = FamilyID.NONE)
    {
        List<PerkID> perkPool = new List<PerkID>();

        foreach(PerkData p in gm.db.dbPerk)
        {
            bool famCheck = (family != FamilyID.NONE && p.family == family) || family == FamilyID.NONE;

            if (famCheck)
            {
                perkPool.Add(p.ID);
                continue;
            }
        }

        int ind = Random.Range(0, perkPool.Count);

        return perkPool[ind];
    }
}