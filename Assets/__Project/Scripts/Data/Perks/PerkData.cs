﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[Serializable]
public struct PerkData
{
    public PerkID ID;
    public RarityID rarity;
    public FamilyID family;

}