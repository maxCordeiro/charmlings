﻿public enum StatID
{
    HEALTH,
    AGILITY,
    STRENGTH,
    DEFENSE,
    MAGIC,
    RESISTANCE
}

public enum StatusID
{
    NONE = -1,
    BURN,
    DROWSY,
    CURSE,
    TRAP,
    KO
}

public enum FamilyID
{
    NONE,
    AERIAL,
    AQUATIC,
    BEAST,
    DARK,
    FAIRY,
    FIELD,
    MACHINE,
    MYSTERIOUS,
    NATURE,
    ORIGIN,
    UNDERGROUND
}

public enum AttributeID
{
    NONE = -1,
    WATER,
    PLANT,
    FIRE,
    COSMIC,
    ELECTRIC,
    WIND,
    SHADOW,
    TECH,
    EARTH,
    NORMAL
}

public enum ColorID
{
    GENERATE = -1, // If a color ID is set to this, Charmling.cs will generate a new color
    COMMON,
    UNCOMMON,
    RARE,
    ULTRARARE
}

public enum GrowthRateID
{
    LIGHT,
    STANDARD,
    TOUGH
}

public enum GenderID
{
    MALE,
    FEMALE,
    UNKNOWN
}

public enum SkillTypeID
{
    STRENGTH,
    MAGIC,
    SPECIAL
}

public enum RarityID
{
    NONE,
    COMMON,
    UNCOMMON,
    RARE,
    ULTRARARE,
    SPECIAL
}

public enum DayID
{
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY
}

public enum DaytimeID
{
    MORNING,
    DAY,
    EVENING,
    NIGHT
}

public enum MonthID
{
    JANUARY,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER
}

public enum TraitID
{
    BRAVE,
    CALM,
    COMPLEX,
    CURIOUS,
    DAINTY,
    DETERMINED,
    ENERGETIC,
    EXCITED,
    FEISTY,
    FORGIVING,
    FRIENDLY,
    GENTLE,
    HUMBLE,
    JOYFUL,
    LOYAL,
    MERRY,
    OUTGOING,
    PROUD,
    RELAXED,
    RESERVED,
    SERIOUS,
    STABLE,
    STUDIOUS,
    WILD,
    WISE
}

public enum HairID
{

}

public enum OverworldDirection
{
    DOWN,
    RIGHT,
    UP,
    LEFT
}


#region Map

public enum MapTierID
{
    NONE,
    ALL,
    A,
    B,
    C,
    D,
    E
}

public enum EncounterTypeID
{
    NONE,
    DAY,
    MORNING,
    NIGHT,
    CAVE,
    FISHING,
    FISHINGH,
    FISHINGL,
    BOSS,
}

#endregion

#region Inventory

public enum BagPockets
{
    GENERAL,
    CARE,
    BATTLE,
    TREASURES,
    CAPSULES,
    CHARMS,
    KEYITEMS
}

public enum ItemEffects_Hold
{
    NONE,
    HEALING,
    DAMAGECALC, // Triggered in battle during damage calculation
    WP
}

public enum ItemEffects_Menu
{
    NONE,
    HEALING,
    EQUIP,
    WEARTOP,
    WEARBOTTOM,
    VOUCHER,
    WP,
    BERRYSEED
}

public enum ItemEffects_Battle
{
    NONE,
    HEALING,
    STATBOOST,
    TAME
}

#endregion

public enum UITypeID
{
    NONE,
    MENU,
    MESSAGE,
    CHOICE,
    QUANTITY
}

public enum UITween
{
    EXPO,
    BACK
}

public enum UITransitionType
{
    SOLID,
    SWIRL,
    CIRCLE,
    LINES,
    WIPE
}