﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class CharmlingSpawn
{
    public CharmlingIdentifier charmling;
    public float spawnChance;
    public MapTierID tier;
    public EncounterTypeID encType;

    public CharmlingSpawn()
    {

    }
}