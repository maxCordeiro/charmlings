﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

[Serializable]
public class MapData
{
    [XmlAttribute("ID")]
    public MapID ID;
    public MapTierID defaultTier;

    public bool hideLighting;
    public bool isDungeon;

    public string battleBackground;

    public List<CharmlingSpawn> charmlingSpawns;
    public List<CharmlingSpawn> charmlingBosses;

    public List<ItemSpawn> itemSpawns;

    public MapData()
    {

    }
}