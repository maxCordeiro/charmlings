﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Rewired;
using UnityEngine.EventSystems;
using Rewired.Integration.UnityUI;

public class InputHandler : MonoBehaviour
{
    public Player rPlayer;
    public Image cursor;

    public EventSystem es;
    public RewiredStandaloneInputModule rsim;

    private void Awake()
    {
        rPlayer = ReInput.players.GetPlayer(0);

        es = FindObjectOfType<EventSystem>();
        rsim = es.GetComponent<RewiredStandaloneInputModule>();
        rsim.RewiredInputManager = FindObjectOfType<InputManager>();

        cursor = GameObject.Find("CursorCanvas").transform.GetChild(0).GetComponent<Image>();
    }

    public void EnableCursor(bool _enabled)
    {
        cursor.gameObject.SetActive(_enabled);
    }

    public void PositionCursor(Vector3 pos, bool overrideZ = false)
    {
        cursor.transform.position = new Vector3(pos.x, pos.y, overrideZ ? pos.z : cursor.transform.position.z);
    }
}