﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class GameVariables
{
    public string gameVersion = "0.1";

    #region Time

    public int currHour = 0;
    public int currMinute = 0;
    public int currMonth  = 0; // Starts at 0 [Jan]
    public int currYear = 1; // Stats at 1
    public int currDayIndex = 0; // Current int index of days in week
    public int currDayInMonth = 0; // Current int index of total days in month

    public DayID currDayID = DayID.SUNDAY; // Current day
    public DaytimeID currDaytime = DaytimeID.DAY; // DayNight time of day

    public float inGameTime = 0.5f;

    #endregion

    #region Charmicon
    
    public List<CharmiconCharmling> charmiconCharmlings;
    public List<CharmiconMap> charmiconMaps;

    #endregion

    #region Nursery

    public NurseryDen[] nurseryDens = new NurseryDen[24];
    public EggIncubator[] eggIncubators = new EggIncubator[24];

    #endregion

    public GameVariables()
    {
        for (int i = 0; i < nurseryDens.Length; i++)
        {
            nurseryDens[i] = new NurseryDen();
        }
    }
}