﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using UnityEngine;

public static class SaveSystem
{
    public static Game loaded;

    public static void NewGame()
    {
        loaded = new Game();
    }

    public static void SaveGame(int saveIndex)
    {
        var serializer = new XmlSerializer(typeof(Game));
        using (var stream = new FileStream(Application.persistentDataPath + "/game_" + saveIndex + ".sav", FileMode.Create))
        {
            serializer.Serialize(stream, loaded);
        }
    }

    public static void LoadGame(int saveIndex)
    {
        if (File.Exists(Application.persistentDataPath + "/" + saveIndex + ".sav"))
        {
            var serializer = new XmlSerializer(typeof(Game));
            using (var stream = new FileStream(Application.persistentDataPath + "/game" + saveIndex + ".sav", FileMode.Open))
            {
                loaded = serializer.Deserialize(stream) as Game;
            }
        }
        else
        {
            Debug.LogWarning("Save file <b>" + saveIndex + "</b> doesn't exist! Clearing loaded data.");
        }
    }
}

public class Game
{
    public PlayerVariables player;
    public GameVariables game;

    public Game()
    {
        player = new PlayerVariables();
        game = new GameVariables();
    }
}