﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class PlayerVariables
{
    public List<Charmling> party = new List<Charmling>(4);
    public Charmling[] bagCharmlings = new Charmling[400];
    public string[] bagPouchNames = new string[20];

    public List<Item> invGeneral = new List<Item>();
    public List<Item> invHealing = new List<Item>();
    public List<Item> invBattle = new List<Item>();
    public List<Item> invTreasures = new List<Item>();
    public List<Item> invMaterials = new List<Item>();
    public List<Item> invCapsules = new List<Item>();
    public List<Item> invCharms = new List<Item>();
    public List<Item> invKeyItems = new List<Item>();

    //public List<TierInfo> unlockedTiers = new List<TierInfo>();

    public bool wearingDress;
    public bool coverEars;

    public HairID pHair;
    public ItemID pAccessory;
    public ItemID pShirt;
    public ItemID pPant;

    public string playerName;
    public string playerID;

    public int money = 0;

    public MapID activeMap;
    public MapTierID activeMapTier;

    public List<Item> GetInventoryList(BagPockets pocket)
    {
        switch (pocket)
        {
            case BagPockets.GENERAL:
                return invGeneral;
            case BagPockets.CARE:
                return invHealing;
            case BagPockets.BATTLE:
                return invBattle;
            case BagPockets.TREASURES:
                return invTreasures;
            case BagPockets.CAPSULES:
                return invCapsules;
            case BagPockets.CHARMS:
                return invCharms;
            case BagPockets.KEYITEMS:
                return invKeyItems;
            default:
                return null;
        }
    }
}