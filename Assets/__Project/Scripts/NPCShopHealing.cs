using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GameManager;

public class NPCShopHealing : NPCEntity
{
    public override IEnumerator Interact(PlayerEntity p)
    {
        UIMessage msg = gm.uiHandler.LoadMenu(UIVariables.UIMESSAGE).GetComponent<UIMessage>();
        msg.AddMessage(gm.GetText("shop_greeting_healing"));
        msg.SetName(gm.GetText("npc_shop_healing_name"));
        msg.KeepOpen(true);
        msg.Refresh(true);

        yield return new WaitForMessageRead(msg);

        UIChoices ch = gm.uiHandler.LoadMenu(UIVariables.UICHOICES).GetComponent<UIChoices>();
        ch.HideCursor();
        ch.AddChoice(0, gm.GetText("shop_buy"));
        ch.AddChoice(1, gm.GetText("shop_sell"));
        ch.AddChoice(2, gm.GetText("shop_cancel"));
        ch.messageRef = msg;
        ch.SetPos(UIVariables.posChoicesMsg);
        ch.Refresh(true);

        yield return new WaitForChoiceClose(ch);

        msg.Close();

        yield return new WaitForMessageClose(msg);

        int selected = UIVariables.tempChoiceIndex;

        switch (selected)
        {
            case 0:
                UIShop shop = gm.uiHandler.LoadMenu(UIVariables.UISHOP).GetComponent<UIShop>();
                shop.items = GetShopInventory();
                shop.Refresh(true);

                yield return new WaitForMenuClose(shop);
                break;
            default:
                break;
        }

        p.isInteracting = false;
        yield break;
    }

    public List<ItemID> GetShopInventory()
    {
        return new List<ItemID>()
        {
            ItemID.POTION,
            ItemID.POTIONHIGH,
            ItemID.POTIONULTRA,
            ItemID.REPEL,
            ItemID.COOLINGLEAF,
            ItemID.SERENEFLUTE,
            ItemID.DIVINETOME,
            ItemID.RADIANTBELL,
            ItemID.REVIVE
        };
    }

    public override void InteractEnter(PlayerEntity p)
    {

    }

    public override void InteractExit(PlayerEntity p)
    {

    }
}
