﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public abstract class NPCEntity : Entity
{
    public abstract void InteractEnter(PlayerEntity p);
    public abstract IEnumerator Interact(PlayerEntity p);
    public abstract void InteractExit(PlayerEntity p);
}