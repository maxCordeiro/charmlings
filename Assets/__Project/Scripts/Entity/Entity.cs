﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Prime31;
using UnityEditor;

[RequireComponent(typeof(CharacterController2D))]
public class Entity : MonoBehaviour
{
    [Header("Components")]
    public CharacterController2D charContr;
    public SpriteRenderer spRender, shadowRender;
    public BoxCollider2D box;
    public SimpleAnimator simpleAnim;

    [Header("Movement")]
    public Vector2 velocity;

    public bool canMove;
    public float moveSpeed;

    public OverworldDirection dir;

    [Header("Anim")]
    public string animationPrefix;

    Vector2 tile;

    public virtual void Awake()
    {
        charContr = GetComponent<CharacterController2D>();
        box = GetComponent<BoxCollider2D>();
        spRender = transform.GetChild(0).GetComponent<SpriteRenderer>();
        shadowRender = transform.GetChild(1).GetComponent<SpriteRenderer>();
        simpleAnim = GetComponent<SimpleAnimator>();
    }

    public void Move()
    {
        if (!canMove) return;

        charContr.move(velocity * Time.deltaTime);
    }

    public void CalcDirection()
    {
        if (velocity == Vector2.zero) return;

        if (Mathf.Abs(velocity.x) > Mathf.Abs(velocity.y))
        {
            dir = Mathf.Sign(velocity.x) == 1 ? OverworldDirection.RIGHT : OverworldDirection.LEFT;
        }
        else //if (Mathf.Abs(inp.x) < Mathf.Abs(inp.y))
        {
            dir = Mathf.Sign(velocity.y) == 1 ? OverworldDirection.UP : OverworldDirection.DOWN;
        }
    }

#if UNITY_EDITOR
    [ContextMenu("Create anim from spritesheet")]
    public void SplitSpritesheet()
    {
        SimpleAnimator sa = GetComponent<SimpleAnimator>();

        sa.animations.Clear();

        string spritePath = AssetDatabase.GetAssetPath(transform.GetChild(0).GetComponent<SpriteRenderer>().sprite);
        Sprite[] animSpriteSheet = AssetDatabase.LoadAllAssetsAtPath(spritePath).OfType<Sprite>().ToArray();

        SimpleAnimator.Anim down = new SimpleAnimator.Anim()
        {
            name = $"{animationPrefix}_Down",
            frames = new Sprite[] { animSpriteSheet[0], animSpriteSheet[1], animSpriteSheet[2], animSpriteSheet[3] }
        };

        SimpleAnimator.Anim right = new SimpleAnimator.Anim()
        {
            name = $"{animationPrefix}_Right",
            frames = new Sprite[] { animSpriteSheet[4], animSpriteSheet[5], animSpriteSheet[6], animSpriteSheet[7] }
        };

        SimpleAnimator.Anim up = new SimpleAnimator.Anim()
        {
            name = $"{animationPrefix}_Up",
            frames = new Sprite[] { animSpriteSheet[8], animSpriteSheet[9], animSpriteSheet[10], animSpriteSheet[11] }
        };

        SimpleAnimator.Anim left = new SimpleAnimator.Anim()
        {
            name = $"{animationPrefix}_Left",
            frames = new Sprite[] { animSpriteSheet[12], animSpriteSheet[13], animSpriteSheet[14], animSpriteSheet[15] }
        };

        sa.animations = new List<SimpleAnimator.Anim>()
        {
            down, right, left, up
        };
    }
#endif
}