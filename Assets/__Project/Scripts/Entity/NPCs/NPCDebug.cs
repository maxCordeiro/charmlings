﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using static GameManager;

public class NPCDebug : NPCEntity
{
    public override IEnumerator Interact(PlayerEntity p)
    {
        UIMessage msg = gm.uiHandler.LoadMenu(UIVariables.UIMESSAGE).GetComponent<UIMessage>();
        msg.AddMessage("Hey! I noticed you have a <c=cc3a80><b>Gubbers</b></c>. Would you be interested in trading it for my <c=cc3a80><b>Fresnose</b></c>? They’re both the same level!");
        msg.SetName("D. Bugger");
        msg.KeepOpen(true);
        msg.Open();

        yield return new WaitForMessageRead(msg);

        UIChoices c = gm.uiHandler.LoadMenu(UIVariables.UICHOICES).GetComponent<UIChoices>();
        c.HideCursor();
        c.AddChoice(0, "Sure!");
        c.AddChoice(1, "No, thanks.");
        c.AddChoice(2, "I don't know...");
        c.messageRef = msg;
        c.Refresh(true);

        yield return new WaitForChoiceClose(c);

        if (UIVariables.tempChoiceIndex == 0 || UIVariables.tempChoiceIndex == 2)
        {
            msg.AddMessage("Alright, let's do this!");
        } else
        {
            msg.AddMessage("OK. Rude.");
        }

        msg.SetName("D. Bugger");
        msg.Open();

        yield return new WaitForMessageClose(msg);

        p.isInteracting = false;
    }

    public override void InteractEnter(PlayerEntity p)
    {

    }

    public override void InteractExit(PlayerEntity p)
    {

    }
}
