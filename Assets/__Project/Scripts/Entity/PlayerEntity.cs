﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using static GameManager;

public class PlayerEntity : Entity
{
    RaycastHit2D prevHit;
    public bool isInteracting;
    private new void Awake()
    {
        SaveSystem.NewGame();

        SaveSystem.loaded.player.activeMap = MapID.VILLAGE;

        //GameUtil.InitLocale();
        gm.lang.LoadDictionary(gm.langSelection[0]);
        //DatabaseUtil.InitDatabase();
        
        base.Awake();
    }


    void Update()
    {
		if (gm.inBattle || gm.inMenu || isInteracting || gm.transitioning) return;

        if (gm.inputHandler.rPlayer.GetButtonDown("Start"))
        {
            UIPause pause = gm.uiHandler.LoadMenu(UIVariables.UIPAUSE).GetComponent<UIPause>();

            pause.Refresh(true);
        }
		
        MoveInput();
        Move();
        CalcDirection();
        Interact();

        if (Input.GetKeyDown(KeyCode.B))
        {
			//TODO replace
			/*
            Charmling b = new Charmling(new CharmlingIdentifier(CharmlingID.FRESNOSE, 0));

            b.skills[0] = new Skill(SkillID.SLAM);
            b.skills[1] = new Skill(SkillID.SLAM);
            b.skills[2] = new Skill(SkillID.SLAM);
            b.skills[3] = new Skill(SkillID.SLAM);
            b.skills[4] = new Skill(SkillID.SLAM);
            b.level = 50;
            b.CalculateAllStats();

            BattleController.instance.AddUnit(1, b);

            BattleController.instance.StartBattle();
			*/
			gm.StartBattle();
        }
    }

    public void MoveInput()
    {
        velocity.x = gm.inputHandler.rPlayer.GetAxisRaw("HorizontalL") * moveSpeed;
        velocity.y = gm.inputHandler.rPlayer.GetAxisRaw("VerticalL") * moveSpeed;
    }

    public void Interact()
    {
        Vector2 origin = box.bounds.center;
        Vector2 vecDir = Vector2.zero;
        int layerMask = LayerMask.GetMask("Interactable");

        switch (dir)
        {
            case OverworldDirection.DOWN:
                origin.y -= box.bounds.extents.y;
                vecDir = Vector2.down;
                break;
            case OverworldDirection.RIGHT:
                origin.x += box.bounds.extents.x;
                vecDir = Vector2.right;
                break;
            case OverworldDirection.UP:
                origin.y += box.bounds.extents.y;
                vecDir = Vector2.up;
                break;
            case OverworldDirection.LEFT:
                origin.x -= box.bounds.extents.x;
                vecDir = Vector2.left;
                break;
        }

        RaycastHit2D hit = Physics2D.Raycast(origin, vecDir, 0.2f, layerMask);

        Debug.DrawLine(origin, origin + vecDir * 0.2f, Color.magenta);

        if (hit)
        {
            NPCEntity npcHit = hit.collider.GetComponent<NPCEntity>();

            if (npcHit)
            {
                if (hit != prevHit)
                {
                    npcHit.InteractEnter(this);
                }

                if (gm.inputHandler.rPlayer.GetButtonDown("A"))
                {
                    isInteracting = true;
                    npcHit.StartCoroutine(npcHit.Interact(this));
                }
            }

            prevHit = hit;
        } else
        {
            if (!prevHit) return;

            NPCEntity npcExit = prevHit.collider.GetComponent<NPCEntity>();
            npcExit?.InteractExit(this);

            prevHit = new RaycastHit2D();
        }
    }
}