﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class UIHandler : MonoBehaviour
{
    public bool inBattle;

    public Canvas canvasUI;
    public GameObject uiMenus;
    public GameObject uiPopups;

    public List<UIBase> openMenus = new List<UIBase>();
    public bool inMenu => openMenus.Count > 0;

    private void Awake()
    {
        //canvasUI = GameObject.FindGameObjectWithTag("GameCanvas").GetComponent<Canvas>();
        //canvasUI.worldCamera = Camera.main;

        //uiMenus = canvasUI.transform.GetChild(2).gameObject;
        //uiPopups = canvasUI.transform.GetChild(3).gameObject;
    }

    public GameObject LoadMenu(string menuName)
    {
        GameObject menu = Instantiate(Resources.Load<GameObject>("UI/" + menuName), uiMenus.transform);

        return menu;
    }

    public void CloseAllMenus()
    {
        foreach (UIBase ui in openMenus)
        {
            ui.Close();
        }
    }
}