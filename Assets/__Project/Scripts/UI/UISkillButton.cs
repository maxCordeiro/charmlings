using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISkillButton : MonoBehaviour
{
    public SkillID id;

    [Header("Resources")]
    public Sprite[] imageButtons;
    public Sprite[] imageButtonsSelected;
    public Sprite imageButtonDisabled;
    public Sprite[] imageAttributes;

    [Header("Components")]
    public Button button;

    public SuperTextMesh textName;
    public SuperTextMesh textPower;
    public SuperTextMesh textCooldown;

    public Image imageSkillAttribute;
    public Image imagePowerBG;
    public Image imageClock;

    public void Refresh(int cooldownIndex = -1, BattleUnit unit = null)
    {
        bool active = id != SkillID.NONE;

        textName.enabled = active;
        textPower.enabled = active;
        textCooldown.enabled = active;
        imageSkillAttribute.enabled = active;
        imagePowerBG.enabled = active;
        imageClock.enabled = active;

        if (active)
        {
            SkillData sd = GameManager.gm.db.dbSkill[(int)id];
            int attId = (int)sd.attribute;

            textName.text = GameManager.gm.GetText(sd.ID.ToString() + "_NAME");
            textPower.text = sd.power == 0? "--" : sd.power.ToString();
            int maxCooldown = sd.cooldown;
            textCooldown.text = unit == null ? maxCooldown.ToString() : $"{unit.cooldowns[cooldownIndex]}/{maxCooldown}";

            textName.color = UIVariables.attributeTextColors[sd.attribute];
            textPower.color = UIVariables.attributeTextColors[sd.attribute];
            textCooldown.color = UIVariables.attributeTextColors[sd.attribute];

            textName.Rebuild(false);
            textPower.Rebuild(false);
            textCooldown.Rebuild(false);
        
            imageSkillAttribute.sprite = imageAttributes[attId];

            SpriteState buttonState = new SpriteState()
            {
                highlightedSprite = imageButtonsSelected[attId],
                pressedSprite = imageButtonsSelected[attId],
                selectedSprite = imageButtonsSelected[attId],
                disabledSprite = imageButtons[attId]
            };

            button.spriteState = buttonState;
            button.image.sprite = imageButtons[attId];
            button.image.type = Image.Type.Sliced;
        } else
        {
            SpriteState buttonStateNone = new SpriteState()
            {
                disabledSprite = imageButtonDisabled
            };

            button.spriteState = buttonStateNone;
            button.image.type = Image.Type.Tiled;
        }

        button.interactable = active;
    }
}
