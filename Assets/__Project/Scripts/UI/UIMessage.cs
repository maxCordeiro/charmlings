﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;
using static GameManager;

public class UIMessage : UIBase
{
    public SuperTextMesh msg;
    public SuperTextMesh msgName;

    public Image msgWindow;
    public Image nameWindow;

    List<string> msgList = new List<string>();

    public bool keepOpen = false;
    public bool isDoneReading;

    int msgIndex;
    string leftoverText;

    public void SetName(string _name) => msgName.text = _name;
    public void AddMessage(string _msg) => msgList.Add(_msg);
    public void KeepOpen(bool _keepOpen) => keepOpen = _keepOpen;

    public event Action onMessageClose;
    public void OnMessageClose() => onMessageClose?.Invoke();

    public event Action onMessageFinishReading;
    public void OnMessageFinishReading()
    {
        if (onCompleteTriggered) return;

        onMessageFinishReading?.Invoke();
    }
    bool onCompleteTriggered;

    public event Action onMessageComplete;
    public void OnMessageComplete()
    {
        onMessageComplete?.Invoke();
    }

    private void Start()
    {
        clearEvents = false;

        onOpen += () =>
        {
            msg.Read();
        };

        onClose += () =>
        {
            OnMessageClose();
        };
    }

    public void Refresh(bool openAfter = false)
    {
        gm.inputHandler.EnableCursor(false);
        EnableChildren();
        transform.SetAsLastSibling();

        ResetMessageQueue();

        msgName.transform.parent.gameObject.SetActive(msgName.text.Length > 0);

        if (openAfter) Open();
    }

    public void ResetMessageQueue(bool read = false)
    {
        isDoneReading = false;

        msgIndex = 0;
        msg.text = msgList[msgIndex];
        msg.Rebuild(read);
    }

    // Call this before adding new messages
    public void ClearMessageQueueAndEvents()
    {
        msgList.Clear();
        onMessageFinishReading = null;
        onCompleteTriggered = false;
    }

    private void OnDestroy()
    {
        onMessageClose = null;
        onMessageFinishReading = null;
        onMessageComplete = null;
    }

    private void Update()
    {
        if (!canInteract || isAnimating) return;

        if (!msg.reading)
        {
            if (msg.leftoverText.Length == 0)
            {
                OnMessageFinishReading();
                onCompleteTriggered = true;
            }

            if (gm.inputHandler.rPlayer.GetButtonDown("A"))
            {
                if (msg.leftoverText.Length > 0)
                {
                    leftoverText = msg.leftoverText.TrimStart();
                    msg.text = leftoverText;
                    msg.Rebuild();
                } else
                {
                    if (msgList.ElementAtOrDefault(msgIndex + 1) != null)
                    {
                        msgIndex++;
                        msg.text = msgList[msgIndex];
                        msg.Rebuild();
                        return;
                    }

                    if (!keepOpen)
                    {
                        Close();
                    } else
                    {
                        keepOpen = false;
                        isDoneReading = true;
                        OnMessageComplete();
                        msgList.Clear();
                    }
                }
            }
        }
    }
}