﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using static GameManager;

public class UIBase : MonoBehaviour
{
    public CanvasGroup cGroup;

    [HideInInspector]
    public float transTime;

    public UIBase referrer;

    public Action onOpen;
    public Action onClose;
    public Action onShow;
    public Action onHide;

    public bool deselectObject;
    
    public bool canInteract;
    public bool isAnimating;
    public bool clearEvents;
    public bool isVisible;

    float startY;

    public Animator animCont;

    public enum UIState
    {
        OPEN,
        CLOSE,
        SHOW,
        HIDE
    }

    public UIState uiState;

    private void Awake()
    {
        EnableChildren(false);
        animCont = GetComponent<Animator>();
        cGroup = GetComponent<CanvasGroup>();
        cGroup.alpha = 0;
        canInteract = false;

        transTime = 0.75f;
    }

    public void Open()
    {
        canInteract = false;
        gm.inputHandler.es.sendNavigationEvents = false;
        uiState = UIState.OPEN;
        StartCoroutine(PlayAnim());
    }

    public void Close()
    {
        gm.inputHandler.EnableCursor(false);
        canInteract = false;
        uiState = UIState.CLOSE;
        StartCoroutine(PlayAnim());
    }

    public void OnOpen()
    {
        isVisible = true;
        canInteract = true;
        gm.inputHandler.es.sendNavigationEvents = true;

        onOpen?.Invoke();
    }

    public void OnClose()
    {
        gm.uiHandler.openMenus.Remove(this);

        if (referrer != null && !referrer.isVisible)
        {
            referrer.Show();
        }

        if (gm.uiHandler.openMenus.Count <= 0)
        {
            gm.inputHandler.EnableCursor(false);
        }

        onClose?.Invoke();

        gm.inputHandler.es.sendNavigationEvents = true;

        Destroy(gameObject);
    }

    private void OnDisable()
    {
        ClearEvents();
    }

    public void Hide()
    {
        canInteract = false;
        uiState = UIState.HIDE;
        StartCoroutine(PlayAnim());
    }

    public void Show()
    {
        uiState = UIState.SHOW;
        StartCoroutine(PlayAnim());
    }

    public void OnShow()
    {
        isVisible = true;
        canInteract = true;
        gm.inputHandler.es.sendNavigationEvents = true;

        onShow?.Invoke();
    }

    public void OnHide()
    {
        isVisible = false;

        onHide?.Invoke();
    }

    public void Freeze(bool frozen = true)
    {
        canInteract = !frozen;
    }

    public void GlobalFreeze(bool frozen = true)
    {
        gm.inputHandler.es.sendNavigationEvents = !frozen;
    }

    public IEnumerator PlayAnim()
    {
        isAnimating = true;

        string animName = "";

        switch (uiState)
        {
            case UIState.OPEN:
                gm.uiHandler.openMenus.Add(this);
                animName = "anim_UIBase_Open";
                break;
            case UIState.SHOW:
                animName = "anim_UIBase_Open";
                break;
            case UIState.CLOSE:
            case UIState.HIDE:
                animName = "anim_UIBase_Close";
                if (deselectObject) gm.inputHandler.EnableCursor(false);
                if (deselectObject) gm.inputHandler.es.SetSelectedGameObject(null);
                gm.inputHandler.es.sendNavigationEvents = false;
                break;
            default:
                break;
        }

        animCont.Play(animName);
        yield return new WaitForEndOfFrame();

        float clipLength = animCont.GetCurrentAnimatorStateInfo(0).length;

        yield return new WaitForSeconds(clipLength);

        switch (uiState)
        {
            case UIState.OPEN:
                OnOpen();
                break;
            case UIState.CLOSE:
                OnClose();
                break;
            case UIState.SHOW:
                OnShow();
                break;
            case UIState.HIDE:
                OnHide();
                break;
            default:
                break;
        }

        if (clearEvents) ClearEvents();

        isAnimating = false;

        yield break;
    }

    public void ClearEvents()
    {
        onOpen = null;
        onClose = null;
        onShow = null;
        onHide = null;
    }

    public static void ReposCursor(GameObject obj = null)
    {
        if (obj)
        {
            gm.inputHandler.PositionCursor(GetCursorPos(obj.GetComponent<RectTransform>()));
        } else
        {
            if (gm.inputHandler.es.currentSelectedGameObject == null) return;

            gm.inputHandler.PositionCursor(GetCursorPos());
        }
    }

    public static Vector3 GetCursorPos(RectTransform rect = null)
    {
        RectTransform sRect = rect == null? gm.inputHandler.es.currentSelectedGameObject.GetComponent<RectTransform>() : rect;
        Vector3[] c = new Vector3[4];
        sRect.GetWorldCorners(c);

        return new Vector3(c[1].x, c[1].y);
    }

    public void Resume(GameObject selected)
    {
        gm.inputHandler.EnableCursor(false);
        gm.inputHandler.es.SetSelectedGameObject(selected);

        ReposCursor();
        gm.inputHandler.EnableCursor(true);
        gm.inputHandler.es.sendNavigationEvents = true;
        canInteract = true;
    }

    public void EnableChildren(bool active = true)
    {
        foreach (Transform t in transform)
        {
            t.gameObject.SetActive(active);
        }
    }
}