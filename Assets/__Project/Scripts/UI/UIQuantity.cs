using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static GameManager;

public class UIQuantity : UIBase
{
    [Header("UI")]
    public SuperTextMesh textQuantity;
    public SuperTextMesh textCurrency;
    public Image imageArrowUp;
    public Image imageArrowDown;

    [Header("Logic")]
    public int max;
    public int min;
    public int current;
    public int price;
    public bool showingCurrency;

    float currentTime;
    bool isTimerRunning;


    public void Start()
    {
        UIVariables.tempQuantity = -1;
        current = 0;
    }

    public void Update()
    {
        if (!canInteract || isAnimating) return;

        if (isTimerRunning)
        {
            if (currentTime <= 0.1f)
            {
                currentTime += Time.deltaTime;
                return;
            }
            else
            {
                currentTime -= 0.1f;
                isTimerRunning = false;
            }
        }

        if (gm.inputHandler.rPlayer.GetAxis("VerticalL") == 1)
        {
            current = Mathf.Clamp(current + 1, min, max);
            RefreshText();
            isTimerRunning = true;
        } else if (gm.inputHandler.rPlayer.GetAxis("VerticalL") == -1)
        {
            current = Mathf.Clamp(current - 1, min, max);
            RefreshText();
            isTimerRunning = true;
        }
        
        if (gm.inputHandler.rPlayer.GetAxis("HorizontalL") == 1)
        {
            current = Mathf.Clamp(current + 10, min, max);
            RefreshText();
            isTimerRunning = true;
        } else if (gm.inputHandler.rPlayer.GetAxis("HorizontalL") == -1)
        {
            current = Mathf.Clamp(current - 10, min, max);
            RefreshText();
            isTimerRunning = true;
        }

        if (gm.inputHandler.rPlayer.GetButtonDown("A"))
        {
            UIVariables.tempQuantity = current;
            Close();
        }

        if (gm.inputHandler.rPlayer.GetButtonDown("B"))
        {
            UIVariables.tempQuantity = -1;
            Close();
        }
    }

    public void Refresh(bool openAfter = true)
    {
        EnableChildren();
        RefreshText();

        textQuantity.alignment = showingCurrency ? SuperTextMesh.Alignment.Left : SuperTextMesh.Alignment.Center;
        textCurrency.gameObject.SetActive(showingCurrency);

        if (openAfter)
        {
            Open();
        }
    }

    public void RefreshText()
    {
        if (current < min) current = min;

        if (showingCurrency)
        {
            textCurrency.text = "G " + (current * price).ToString("N0");
        }

        textQuantity.text = "x" + current.ToString("N0");

        imageArrowDown.enabled = current > min;
        imageArrowUp.enabled = current < max;
    }

    public void SetPos(Vector3 _pos)
    {
        RectTransform rt = transform.GetChild(0).GetComponent<RectTransform>();

        rt.position = new Vector3(_pos.x, _pos.y, rt.position.z);
    }
}
