using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInventoryPartyButton : MonoBehaviour
{
    [Header("Images")]
    public Image imageCharmlingIcon;
    public Image imageCharmlingIconBG;
    public Image imageStatus;

    public Image imageHealthBG;
    public Image imageHealthBar;
    public Image imageItemIcon;

    [Header("Text")]
    public SuperTextMesh textCharmlingName;
    public SuperTextMesh textGender;
    public SuperTextMesh textLevel;

    public SuperTextMesh textHP;
    public SuperTextMesh textAccessory;
    public SuperTextMesh textCompatible;

    public Sprite[] spriteStatus;

    public Button button;
    public bool isCompatible;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    /* refreshState:
     * 0 = HP
     * 1 = Accessory
     * 2 = Compatibility
     */
    public void Refresh(Charmling charmling, int refreshState = 0)
    {
        RefreshInfo(charmling);
        RefreshState(charmling, refreshState);
    }

    public void RefreshInfo(Charmling charmling)
    {
        bool noCharmling = charmling == null;

        button.interactable = !noCharmling;

        imageCharmlingIcon.enabled = !noCharmling;
        imageCharmlingIconBG.enabled = !noCharmling;
        imageStatus.enabled = !noCharmling;
        textCharmlingName.enabled = !noCharmling;
        textGender.enabled = !noCharmling;
        textLevel.enabled = !noCharmling;

        if (!noCharmling)
        {
            imageCharmlingIcon.sprite = charmling.GetIcon();

            textCharmlingName.text = charmling.GetName();
            textGender.text = charmling.PrintGender();
            textLevel.text = $"Lv {charmling.level}";

            if (charmling.status != StatusID.KO && charmling.status != StatusID.NONE)
            {
                imageStatus.enabled = true;
                imageStatus.sprite = spriteStatus[(int)charmling.status];

            } else
            {
                imageStatus.enabled = false;
            }
        }
    }

    public void RefreshState(Charmling charmling, int refreshState)
    {
        bool noCharmling = charmling == null;

        switch (refreshState)
        {
            case 0:
                imageHealthBG.enabled = !noCharmling;
                imageHealthBar.enabled = !noCharmling;
                textHP.enabled = !noCharmling;

                imageItemIcon.enabled = false;
                textAccessory.enabled = false;

                textCompatible.enabled = false;

                if (!noCharmling)
                {
                    LeanTween.cancel(imageHealthBar.gameObject);
                    LeanTween.size(imageHealthBar.rectTransform, new Vector2(((float)charmling.currentHP / (float)charmling.stats[(int)StatID.HEALTH]) * 194, 24), 0.1f);
                    textHP.text = $"{charmling.currentHP}/{charmling.stats[(int)StatID.HEALTH]}";
                }
                break;
            case 1:
                imageHealthBG.enabled = false;
                imageHealthBar.enabled = false;
                textHP.enabled = false;

                imageItemIcon.enabled = false;
                textAccessory.enabled = false;

                textCompatible.enabled = !noCharmling;

                if (!noCharmling)
                {
                    textAccessory.text = charmling.accessory == null ? "--" : $"{charmling.accessory.GetName()}";
                }
                break;
            case 2:
                imageHealthBG.enabled = false;
                imageHealthBar.enabled = false;
                textHP.enabled = false;

                imageItemIcon.enabled = !noCharmling;
                textAccessory.enabled = !noCharmling;

                textCompatible.enabled = false;

                if (!noCharmling)
                {
                    textCompatible.text = isCompatible ? "Compatible!" : "--";
                }
                break;
            default:
                break;
        }

    }
}