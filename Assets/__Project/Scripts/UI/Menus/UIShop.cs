﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using static GameManager;

public class UIShop : UIBase
{
    [Header("UI")]
    public SuperTextMesh textPocket;
    public SuperTextMesh textCurrency;
    public SuperTextMesh textPage;
    public SuperTextMesh textDesc;

    public SuperTextMesh textInBag;
    public SuperTextMesh textInBagQuantity;

    // Left = 0, Right = 1
    public Image[] imagePages;
    public UIItemSlot[] itemSlots;

    [Header("Logic")]
    public int pageCurrent;
    public int pageTotal;

    int prevSelected;
    int selectedPrice;

    public List<ItemID> items;

    float currentTime;
    bool isTimerRunning;

    UIMessage msg;

    private void Start()
    {
        for (int i = 0; i < itemSlots.Length; i++)
        {
            int ii = i;
            /*itemSlots[i].button.onClick.AddListener(() =>
            {
                SelectItem(ii);
            });*/
        }
    }

    public void SelectItem(int index)
    {
        prevSelected = index;

        UIChoices ch = gm.uiHandler.LoadMenu(UIVariables.UICHOICES).GetComponent<UIChoices>();
        msg = gm.uiHandler.LoadMenu(UIVariables.UIMESSAGE).GetComponent<UIMessage>();

        RectTransform sRect = itemSlots[prevSelected].GetComponent<RectTransform>();
        Vector3[] c = new Vector3[4];
        sRect.GetWorldCorners(c);

        ch.SetPos(new Vector3(c[2].x, (c[2].y + c[3].y) * 0.5f));
        ch.AddChoice(0, GameManager.gm.GetText("shop_buy"));
        ch.AddChoice(1, GameManager.gm.GetText("sys_cancel"));
        ch.onChoiceSelected += Choice_Select;
        ch.Refresh(true);
        ch.HideCursor();
        Freeze();
    }

    public void Choice_Select()
    {
        int price = GameManager.gm.GetItemData(items[prevSelected + (itemSlots.Length * pageCurrent)]).price;

        int selected = UIVariables.tempChoiceIndex;
        UIQuantity q = gm.uiHandler.LoadMenu(UIVariables.UIQUANTITY).GetComponent<UIQuantity>();

        switch (selected)
        {
            case 0:
                q.referrer = msg;
                q.max = GameManager.MAXITEMSTACK;
                q.min = 1;
                q.SetPos(UIVariables.posQuantityMsg);
                q.showingCurrency = true;
                q.price = price;
                q.onClose += () =>
                {
                    msg.ClearMessageQueueAndEvents();
                    BuyItem();
                };

                msg.AddMessage("How many?");
                msg.referrer = this;
                msg.onMessageFinishReading += () =>
                {
                    msg.Freeze();
                    q.Refresh(true);
                };
                msg.Refresh(true);
                break;
            case -1:
            case 1:
                Resume(itemSlots[prevSelected].gameObject);
                break;
            default:
                break;
        }
    }

    public void BuyItem()
    {
        switch (ItemUtil.BuyItem(items[prevSelected], 1, 0))
        {
            case 1:
                msg.AddMessage("Thank you!");
                RefreshCurrency();
                break;
            case 2:
                msg.AddMessage("Not enough money!");
                break;
            case 3:
                msg.AddMessage("Not enough room!");
                break;
            default:
                break;
        }

        msg.onClose += () =>
        {
            Resume(itemSlots[prevSelected].gameObject);
        };
        msg.Freeze(false);
        msg.ResetMessageQueue();
        msg.msg.Read();
    }

    public void Update()
    {
        if (!canInteract || isAnimating) return;

        if (gm.inputHandler.rPlayer.GetButtonDown("Start") || gm.inputHandler.rPlayer.GetButtonDown("B"))
        {
            Close();
        }

        ReposCursor();

        if (isTimerRunning)
        {
            if (currentTime <= 0.2f)
            {
                currentTime += Time.deltaTime;
            }
            else
            {
                currentTime -= 0.2f;
                isTimerRunning = false;
            }
        }
        else
        {
            if (gm.inputHandler.rPlayer.GetAxisRaw("HorizontalL") == 1)
            {
                if (pageCurrent + 1 <= pageTotal)
                {
                    pageCurrent++;
                    RefreshItems();
                    isTimerRunning = true;
                }
            }
            else if (gm.inputHandler.rPlayer.GetAxisRaw("HorizontalL") == -1)
            {
                if (pageCurrent - 1 >= 0)
                {
                    pageCurrent--;
                    RefreshItems();
                    isTimerRunning = true;
                }
            }
            else if (gm.inputHandler.rPlayer.GetAxisRaw("HorizontalL") == 0)
            {
                currentTime = 0;
                isTimerRunning = false;
            }
        }
    }

    public void Refresh(bool openAfter)
    {
        EnableChildren();

        RefreshItems(!openAfter);
        RefreshCurrency();

        int currPage = pageCurrent + 1;
        int totPage = pageTotal + 1;

        textPage.text = $"{currPage}/{totPage}";
        imagePages[0].enabled = pageCurrent != 0;
        imagePages[1].enabled = pageCurrent != pageTotal;

        if (openAfter)
        {
            onOpen += () =>
            {
                Resume(itemSlots[0].gameObject);
                ReposCursor();

                onOpen = null;
            };

            Open();
        }
    }

    public void RefreshItems(bool selectItem = true)
    {
        int slotLength = itemSlots.Length;
        int currCount = items.Count;

        pageTotal = (int)Mathf.Ceil(currCount / slotLength);

        for (int i = 0; i < slotLength; i++)
        {
            int itemIndex = i + (slotLength * pageCurrent);

            if (itemIndex >= items.Count)
            {
                itemSlots[i].shopID = ItemID.NONE;
                itemSlots[i].Refresh(this);
            }
            else
            {

                itemSlots[i].shopID = items[itemIndex];
                itemSlots[i].Refresh(this);
            }
        }

        if (selectItem)
        {
            Resume(itemSlots[0].gameObject);
            itemSlots[0].RefreshDesc();
        }
    }

    public void RefreshCurrency()
    {
        textCurrency.text = $"G {string.Format("{0:n0}", SaveSystem.loaded.player.money)}";
    }
}