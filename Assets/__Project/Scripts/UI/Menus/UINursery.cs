using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static GameManager;

public class UINursery : UIBase
{
    [Header("UI")]
    public UINurseryDen[] uiDens;
    public SuperTextMesh textParentFInfo;
    public SuperTextMesh textParentMInfo;
    public SuperTextMesh textParentFItem;
    public SuperTextMesh textParentMItem;

    public Image imageParentF;
    public Image imageParentM;

    public SuperTextMesh textEggsMade;

    public UIPageIndicator pageIndicator;

    [Header("Logic")]
    public int pageCurrent;
    public int pageTotal;

    int prevSelectedDen;

    // Start is called before the first frame update
    void Start()
    {
        foreach (UINurseryDen den in uiDens)
        {
            den.uiNursery = this;
        }

        pageTotal = Mathf.CeilToInt(SaveSystem.loaded.game.nurseryDens.Length / 4);

        Refresh();
        Open();

        gm.inputHandler.es.SetSelectedGameObject(uiDens[0].bg.gameObject);
        ReposCursor();
        gm.inputHandler.EnableCursor(true);
        RefreshRightPanel(0);
    }

    public void Refresh()
    {
        for (int i = 0; i < uiDens.Length; i++)
        {
            uiDens[i].Refresh(i + (pageCurrent * uiDens.Length));
        }

        pageIndicator.Refresh(pageCurrent, pageTotal);
    }

    public void RefreshRightPanel(int denIndex)
    {
        int denPageIndex = denIndex + (pageCurrent * uiDens.Length);
        NurseryDen den = SaveSystem.loaded.game.nurseryDens[denPageIndex];

        if (!den.isUnlocked)
        {
            EraseRightPanel();
            return;
        }

        textParentFInfo.text = den.parentF == null? "--" : den.parentF.GetName() + "<br>Lv. " + den.parentF.level.ToString() + " " + den.parentF.PrintGender();
        textParentMInfo.text = den.parentM == null ? "--" : den.parentM.GetName() + "<br>Lv. " + den.parentM.level.ToString() + " " + den.parentM.PrintGender();

        textParentFItem.text = den.parentF.accessory == null? "--" : den.parentF.accessory.GetName();
        textParentMItem.text = den.parentM.accessory == null? "--" : den.parentM.accessory.GetName();

        imageParentF.sprite = den.parentF == null ? null : den.parentF.GetIcon();
        imageParentM.sprite = den.parentM == null ? null : den.parentM.GetIcon();

        imageParentF.enabled = den.parentF != null;
        imageParentM.enabled = den.parentM != null;

        textEggsMade.text = string.Format("Eggs Made<a=right>{0}<br><a=left>Started on <a=right>{1}</a>", den.eggStreak.ToString(), den.startDate == null? "--" : den.startDate.ToString());
    }

    public void EraseRightPanel()
    {
        textParentFInfo.text = "--";
        textParentMInfo.text = "--";

        textParentFItem.text = "--";
        textParentMItem.text = "--";

        imageParentF.enabled = false;
        imageParentM.enabled = false;
    }

    public void SelectDen(int index)
    {
        cGroup.interactable = false;

        prevSelectedDen = index;

        UIChoices c = gm.uiHandler.LoadMenu(UIVariables.UICHOICES).GetComponent<UIChoices>();
        c.onChoiceSelected += SelectDen_Action;

        c.AddChoice(0, "Add Parent");
        c.AddChoice(1, "Swap Parent");
        c.HideCursor();
        c.Refresh(true);
    }

    public void SelectDen_Action()
    {
        int choiceIndex = UIVariables.tempChoiceIndex;

        switch (choiceIndex)
        {
            case 0:
                Resume(uiDens[prevSelectedDen].gameObject);
                break;
            case 1:
                Resume(uiDens[prevSelectedDen].gameObject);
                break;
            default:
                break;
        }
    }

    public void SelectDen_MessageCompleteAction()
    {
        SelectDen(prevSelectedDen);
    }

    // Update is called once per frame
    void Update()
    {
        if (!cGroup.interactable) return;

        if (gm.inputHandler.es.currentSelectedGameObject == null)
        {
            gm.inputHandler.es.SetSelectedGameObject(uiDens[prevSelectedDen].gameObject);
            ReposCursor();
        }

        if (!gm.inputHandler.cursor.isActiveAndEnabled) gm.inputHandler.cursor.gameObject.SetActive(true);
        ReposCursor();
    }
}
