using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static GameManager;
using UnityEngine.EventSystems;
using System.Linq;
using EnhancedUI.EnhancedScroller;

public class UIItemSlot : UIDynamicSlot
{
    [Header("UI")]
    public Image imageItem;
    public Image imageBG;

    public Sprite spriteUnselected;
    public Sprite spriteSelected;

    public SuperTextMesh textItem;
    public SuperTextMesh textQuantity;

    [Header("Logic")]
    public UIInventory uiInventory;
    public UIShop uiShop;

    public BagPockets indexPocket;

    public ItemID shopID;


    public void Awake()
    {
        uiInventory = FindObjectOfType<UIInventory>();
    }

    public void RefreshDesc()
    {
        if (uiInventory != null)
        {
            uiInventory.textDesc.text = SaveSystem.loaded.player.GetInventoryList(indexPocket).ElementAtOrDefault(dataIndex) == null ? "" : SaveSystem.loaded.player.GetInventoryList(uiInventory.activePocket)[dataIndex].GetDesc();
        }
        else if (uiShop != null)
        {
            uiShop.textDesc.text = shopID == ItemID.NONE ? "" : ItemUtil.GetDesc(shopID);
            uiShop.textInBagQuantity.text = shopID == ItemID.NONE ? "--" : ItemUtil.CountItem(shopID).ToString();
        }
    }

    public override void Refresh(int _dataIndex)
    {
        dataIndex = _dataIndex;
        indexPocket = uiInventory.activePocket;
        Item item = SaveSystem.loaded.player.GetInventoryList(indexPocket).ElementAtOrDefault(dataIndex) == null ? null : SaveSystem.loaded.player.GetInventoryList(indexPocket)[dataIndex];

        if (item == null) return;

        ItemData itemData = gm.GetItemData(item.ID);

        imageItem.sprite = item.GetIcon();

        textItem.text = item.GetName();
        textQuantity.text = itemData.pocket == BagPockets.KEYITEMS ? "" : "x" + item.currQuantity;
    }

    public void Refresh(UIShop _shop)
    {
        uiShop = _shop;
        bool itemIsNull = shopID == ItemID.NONE;

        if (itemIsNull) return;

        ItemData itemData = gm.GetItemData(shopID);

        imageItem.sprite = ItemUtil.GetIcon(shopID, 0);

        textItem.text = ItemUtil.GetName(shopID);
        textQuantity.text = "G " + itemData.price.ToString("N0");
    }

    public override void ShowSelection(bool selected)
    {
        imageBG.sprite = selected ? spriteSelected : spriteUnselected;
        SaveSystem.loaded.player.GetInventoryList(indexPocket)[dataIndex].isSelected = selected;
    }
}
