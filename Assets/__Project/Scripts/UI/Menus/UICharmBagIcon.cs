using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.EventSystems;

public class UICharmBagIcon : MonoBehaviour, ISelectHandler
{
    [Header("UI")]
    public Image imageCharmling;
    public Image imageAccessory;
    public Canvas canvasCharmling;
    public Canvas canvasAccessory;

    public Button button;

    [Header("Logic")]
    public int charmlingIndex;
    public bool inBag;

    public UICharmBag bagReference;

    public void OnSelect(BaseEventData eventData)
    {
        bagReference.RefreshStatus(charmlingIndex, inBag);
    }

    public void Refresh(int _charmlingIndex, bool _inBag)
    {
        inBag = _inBag;
        charmlingIndex = _charmlingIndex;

        bool hasCharmling = inBag ? SaveSystem.loaded.player.bagCharmlings[charmlingIndex] != null : SaveSystem.loaded.player.party.Count > charmlingIndex;

        imageCharmling.enabled = hasCharmling;
        imageCharmling.color = Color.white;
        imageAccessory.enabled = hasCharmling;
        imageAccessory.color = Color.white;

        if (hasCharmling)
        {
            Charmling charmling = inBag ? SaveSystem.loaded.player.bagCharmlings[charmlingIndex] : SaveSystem.loaded.player.party[charmlingIndex];

            imageCharmling.sprite = charmling.GetIcon();
            imageAccessory.enabled = charmling.accessory != null;
            if (charmling.accessory != null) imageAccessory.sprite = charmling.accessory.GetIcon();
        }
    }
}
