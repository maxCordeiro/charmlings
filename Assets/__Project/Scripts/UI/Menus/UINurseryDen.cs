using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UINurseryDen : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [Header("UI")]
    public Button bg;

    public Image bgParentF;
    public Image bgParentM;
    public Image iconParentF;
    public Image iconParentM;

    public Image bgItemF;
    public Image bgItemM;
    public UIItemIcon iconItemF;
    public UIItemIcon iconItemM;

    public Image bgEgg;
    public Image spriteEgg;
    public Image spriteLock;

    [Header("Variables")]
    public int denIndex;

    public UINursery uiNursery;

    public void OnPointerEnter(PointerEventData eventData)
    {
        uiNursery.RefreshRightPanel(denIndex);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        uiNursery.RefreshRightPanel(denIndex);
    }

    public void Refresh(int _denIndex)
    {
        NurseryDen den = SaveSystem.loaded.game.nurseryDens[_denIndex];

        if (den.isUnlocked)
        {
            bg.image.color = new Color(1, 1, 1, 1f);

            bgParentF.enabled = true;
            bgParentM.enabled = true;
            bgItemF.enabled = true;
            bgItemM.enabled = true;
            bgEgg.enabled = true;

            spriteLock.enabled = false;

            //Parents
            iconParentF.sprite = den.parentF == null ? null : den.parentF.GetIcon();
            iconParentM.sprite = den.parentM == null ? null : den.parentM.GetIcon();

            iconParentF.enabled = den.parentF != null;
            iconParentM.enabled = den.parentF != null;

            // Accessories
            iconItemF.Refresh(den.parentF != null? den.parentF.accessory : null);
            iconItemM.Refresh(den.parentM != null ? den.parentM.accessory : null);

            // Egg
            spriteEgg.sprite = den.hasEgg? den.egg.GetIcon() : null;
            spriteEgg.enabled = den.hasEgg;
        } else
        {
            bg.image.color = new Color(1, 1, 1, 0.5f);

            bgParentF.enabled = false;
            bgParentM.enabled = false;
            bgItemF.enabled = false;
            bgItemM.enabled = false;
            bgEgg.enabled = false;

            iconParentF.enabled = false;
            iconParentM.enabled = false;

            iconItemF.Refresh(null);
            iconItemM.Refresh(null);
            spriteEgg.enabled = false;
            
            spriteLock.enabled = true;
        }
    }
}
