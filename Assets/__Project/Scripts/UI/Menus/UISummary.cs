using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static GameManager;

public class UISummary : UIBase
{
    public int index;
    public bool inParty;

    [Header("UI")]
    public SuperTextMesh textNickname;
    public SuperTextMesh textLevel;
    public SuperTextMesh textGender;
    public SuperTextMesh textHP;

    public SuperTextMesh textEXP;
    public Image imageEXPBar;

    public SuperTextMesh textAccessory;
    public SuperTextMesh textWeight;
    public SuperTextMesh textHeight;

    public SuperTextMesh textMet;
    public SuperTextMesh textPerk;

    public SuperTextMesh textSpecies;
    public SuperTextMesh textNumber;
    public SuperTextMesh textFamily;
    public SuperTextMesh textTrait;

    public UIAttribute[] uiAttribute;

    public SuperTextMesh[] textStatNames;
    public SuperTextMesh[] textStats;
    public SuperTextMesh[] textStatGrades;
    public Image[] imageStatBars;

    public Image imageCharmling;

    public UISkillButton[] buttonSkills;

    private void Start()
    {
        gm.inputHandler.EnableCursor(false);
    }

    public void Update()
    {
        if (!canInteract || isAnimating) return;

        if (gm.inputHandler.rPlayer.GetButtonDown("Start") || gm.inputHandler.rPlayer.GetButtonDown("B"))
        {
            Close();
        }
    }

    public void Refresh(bool openAfter)
    {
        EnableChildren();

        Charmling c = inParty ? SaveSystem.loaded.player.party[index] : SaveSystem.loaded.player.bagCharmlings[index];
        CharmlingData cD = gm.GetCharmlingData(c.ID);
        TraitData tD = gm.db.dbTrait[(int)c.trait.ID];

        textNickname.text = c.GetName();
        textLevel.text = c.level.ToString();
        textGender.text = c.PrintGender();
        textHP.text = $"{c.currentHP}/{c.stats[(int)StatID.HEALTH]}";

        textEXP.text = $"{c.expCurrent}/{c.expNext}";
        imageEXPBar.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, ((float)c.expCurrent / (float)c.expTotal) * 375);

        textAccessory.text = c.accessory == null ? "--" : gm.GetText(c.accessory.ID.ToString() + "_NAME");
        textHeight.text = c.GetHeight();
        textWeight.text = c.GetWeight();

        textMet.text = string.Format(gm.GetText("summary_tamed"), SaveSystem.loaded.player.playerName, c.obtainedDate.ToString(),
            gm.GetText($"{c.obtainedMap}_NAME"), c.obtainedLevel.ToString());
        textPerk.text = gm.GetText($"{c.perk}_NAME");

        textSpecies.text = c.GetSpeciesName();
        textFamily.text = $"{gm.GetText("fam_" + cD.family.ToString())} {gm.GetText("fam")}";
        textTrait.text = gm.GetText(tD.ID.ToString());

        uiAttribute[0].id = cD.attr;
        uiAttribute[0].Refresh();

        if (cD.secAttr != AttributeID.NONE)
        {
            uiAttribute[1].id = cD.secAttr;
            uiAttribute[1].Refresh();
            uiAttribute[1].gameObject.SetActive(true);
        } else
        {
            uiAttribute[1].gameObject.SetActive(false);
        }

        for (int i = 0; i < textStatNames.Length; i++)
        {
            textStatNames[i].text = gm.GetText("sys_" + (StatID)i) + (tD.statUp == (StatID)i ? (c.trait.plus ? "++" : "+") : tD.statDown == (StatID)i? "-" : "");
            textStats[i].text = c.stats[i].ToString();
            imageStatBars[i].rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,
                (float)((float)c.wonderPoints[i] / (float)250) * 206);

            string grade = "";

            if (c.baseStats[i] == 50)
            {
                grade = "<c=58c4c4>S</c>";
            } else if (c.baseStats[i] >= 41 && c.baseStats[i] <= 49)
            {
                grade = "<c=4ece3c>A</c>";
            } else if (c.baseStats[i] >= 31 && c.baseStats[i] <= 40)
            {
                grade = "<c=1e409e>B</c>";
            } else if (c.baseStats[i] >= 16 && c.baseStats[i] <= 30)
            {
                grade = "<c=efab48>C</c>";
            } else if (c.baseStats[i] >= 2 && c.baseStats[i] <= 15)
            {
                grade = "<c=561d1d>D</c>";
            } else if (c.baseStats[i] == 1)
            {
                grade = "<c=4c4c4c>F</c>";
            } else
            {
                grade = "?";
            }

            textStatGrades[i].text = grade;

        }

        for (int i = 0; i < buttonSkills.Length; i++)
        {
            buttonSkills[i].id = i == 5? c.exSkill.ID : c.skills[i].ID;
            buttonSkills[i].Refresh();
        }

        imageCharmling.sprite = c.GetSprite();

        if (openAfter) Open();
    }
}
