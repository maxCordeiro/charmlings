﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using static GameManager;

public class UIPause : UIBase
{
    public Button[] buttonsPause;

    int prevSelected;

    private void Start()
    {
        onOpen += () =>
        {
            gm.inputHandler.es.SetSelectedGameObject(buttonsPause[0].gameObject);
            ReposCursor();
            gm.inputHandler.EnableCursor(true);
        };

        for (int i = 0; i < buttonsPause.Length; i++)
        {
            int ii = i;
            buttonsPause[i].onClick.AddListener(() => {
                SelectButton(ii);
            });
        }
    }

    public void Refresh(bool openAfter = false)
    {
        EnableChildren();
        Open();
    }

    public void SelectButton(int index)
    {
        if (isAnimating || !canInteract) return;

        prevSelected = index;

        onHide -= OpenMenu;

        Hide();

        onHide += OpenMenu;
    }

    public void OpenMenu()
    {
        switch (prevSelected)
        {
            case 0:
                UIParty party = gm.uiHandler.LoadMenu(UIVariables.UIPARTY).GetComponent<UIParty>();
                party.referrer = this;
                party.Refresh(true);
                break;
            case 1:
                UIInventory inv = gm.uiHandler.LoadMenu(UIVariables.UIINVENTORY).GetComponent<UIInventory>();
                inv.referrer = this;
                inv.Refresh(true);
                break;
            case 3:
                UICharmBag bag = gm.uiHandler.LoadMenu(UIVariables.UICHARMBAG).GetComponent<UICharmBag>();
                bag.referrer = this;
                bag.Refresh(true);
                break;
            case 5:
                Close();
                break;
            default:
                Close();
                break;
        }
    }


    private void Update()
    {
        if (!canInteract || isAnimating) return;
        
        if (gm.inputHandler.rPlayer.GetButtonDown("Start") || gm.inputHandler.rPlayer.GetButtonDown("B"))
        {
            Close();
        }

        if (!gm.inputHandler.cursor.isActiveAndEnabled) gm.inputHandler.cursor.gameObject.SetActive(true);
        if (gm.inputHandler.es.currentSelectedGameObject == null) gm.inputHandler.es.SetSelectedGameObject(buttonsPause[prevSelected].gameObject);

        ReposCursor();
    }
}