using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static GameManager;

public class UICharmBag : UIBase
{
    [Header("UI")]
    public SuperTextMesh textPouch;
    public SuperTextMesh textPage;

    public SuperTextMesh textCharmlingName;
    public SuperTextMesh textCharmlingSpecies;
    public SuperTextMesh textCharmlingLevel;
    public SuperTextMesh textCharmlingGender;
    public SuperTextMesh textCharmlingAccessory;

    public Image imageCharmling;
    public Image imageCharmlingAccessory;

    public Image imageGrab;
    public Image imageGrabShadow;

    public Image[] imagePages;

    public UIAttribute[] uiAttribute;
    public UISkillButton[] uiSkills;

    public UICharmBagIcon[] uiBagIcons;
    public UICharmBagIcon[] uiPartyIcons;

    [Header("Logic")]
    public int pageCurrent;
    int pagePrev;
    public int pageTotal;

    public Vector3 grabbingOffset;
    
    int currSelected;
    bool currSelectedInBag;
    GameObject currSelectedObject;

    int prevSelected;
    bool prevSelectedInBag;
    GameObject prevSelectedObject;

    Item tempFromAccessory;
    Item tempToAccessory;
    bool isSwappingAccessory;

    BagPockets tempRequestingPocket;
    int tempRequestingItemIndex;

    float currenttime;
    bool isTimerRunning;

    bool isGrabbingCharmling;

    UIMessage msgAccessory;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < uiBagIcons.Length; i++)
        {
            int ii = i;
            uiBagIcons[i].bagReference = this;
            uiBagIcons[i].button.onClick.AddListener(() =>
            {
                OnCharmlingSelect(ii, true);
            });
        }

        for (int i = 0; i < uiPartyIcons.Length; i++)
        {
            int ii = i;
            uiPartyIcons[i].bagReference = this;
            uiPartyIcons[i].button.onClick.AddListener(() =>
            {
                OnCharmlingSelect(ii, false);
            });
        }

        imageGrab.enabled = false;
        imageGrabShadow.enabled = false;

        msgAccessory = gm.uiHandler.LoadMenu(UIVariables.UIMESSAGE).GetComponent<UIMessage>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!canInteract || isAnimating) return;

        ReposCursor();

        if (isGrabbingCharmling || isSwappingAccessory)
        {
            imageGrabShadow.transform.position = gm.inputHandler.es.currentSelectedGameObject.transform.position + grabbingOffset;
        }

        if (gm.inputHandler.rPlayer.GetButtonDown("Start"))
        {
            Close();
        }

        if (gm.inputHandler.rPlayer.GetButtonDown("B"))
        {
            if (isGrabbingCharmling)
            {
                isGrabbingCharmling = false;

                RefreshBagIcons(false);
                RefreshPartyIcons();

            } else if (isSwappingAccessory)
            {
                isSwappingAccessory = false;

                RefreshBagIcons(false);
                RefreshPartyIcons();
            } else
            {
                Close();
            }
        }

        if (gm.inputHandler.rPlayer.GetButtonDown("RB"))
        {
            if (pageCurrent == pageTotal)
            {
                pageCurrent = 0;
            } else
            {
                pageCurrent++;
            }

            Refresh(false);
        } else if (gm.inputHandler.rPlayer.GetButtonDown("LB"))
        {
            if (pageCurrent == 0)
            {
                pageCurrent = pageTotal;
            } else
            {
                pageCurrent--;
            }

            Refresh(false);
        }
    }

    public void Refresh(bool openAfter = true)
    {
        EnableChildren();

        string pouchName = SaveSystem.loaded.player.bagPouchNames[pageCurrent];

        textPouch.text = (pouchName != null && pouchName.Length > 0) ? SaveSystem.loaded.player.bagPouchNames[pageCurrent] : string.Format(gm.GetText("charmbag_pouch"), pageCurrent + 1);

        RefreshBagIcons();
        RefreshPartyIcons();

        textPage.text = $"{pageCurrent + 1}/{pageTotal + 1}";

        imagePages[0].enabled = pageCurrent != 0;
        imagePages[1].enabled = pageCurrent != pageTotal;
        

        if (openAfter)
        {
            imageCharmling.enabled = false;
            textCharmlingLevel.enabled = false;
            textCharmlingName.enabled = false;
            textCharmlingSpecies.enabled = false;
            textCharmlingGender.enabled = false;
            imageCharmlingAccessory.enabled = false;
            textCharmlingAccessory.enabled = false;

            for (int i = 0; i < 6; i++)
            {
                uiSkills[i].gameObject.SetActive(false);
            }

            uiAttribute[0].gameObject.SetActive(false);
            uiAttribute[1].gameObject.SetActive(false);

            onOpen += () =>
            {
                Resume(uiBagIcons[0].gameObject);
                onOpen = null;
            };

            Open();
        }
    }

    public void RefreshBagIcons(bool selectAfter = false)
    {
        //imageGrab.enabled = false;
        //imageGrabShadow.enabled = false;

        pageTotal = SaveSystem.loaded.player.bagCharmlings.Length / 20 - 1;

        for (int i = 0; i < uiBagIcons.Length; i++)
        {
            uiBagIcons[i].Refresh(i + (uiBagIcons.Length * pageCurrent), true);
        }

        if (selectAfter)
        {
            Resume(uiBagIcons[0].gameObject);
        }
    }

    public void RefreshPartyIcons()
    {
        for (int i = 0; i < uiPartyIcons.Length; i++)
        {
            uiPartyIcons[i].Refresh(i, false);
        }
    }

    public void RefreshStatus(int index, bool inBag)
    {
        bool hasCharmling = inBag ? SaveSystem.loaded.player.bagCharmlings[index] != null : SaveSystem.loaded.player.party.Count > index;

        imageCharmling.enabled = hasCharmling;
        textCharmlingLevel.enabled = hasCharmling;
        textCharmlingName.enabled = hasCharmling;
        textCharmlingSpecies.enabled = hasCharmling;
        textCharmlingGender.enabled = hasCharmling;
        imageCharmlingAccessory.enabled = hasCharmling;
        textCharmlingAccessory.enabled = hasCharmling;

        for (int i = 0; i < 6; i++)
        {
            uiSkills[i].gameObject.SetActive(hasCharmling);
        }

        uiAttribute[0].gameObject.SetActive(hasCharmling);
        uiAttribute[1].gameObject.SetActive(hasCharmling);

        if (!hasCharmling) return;

        Charmling charmling = inBag ? SaveSystem.loaded.player.bagCharmlings[index] : SaveSystem.loaded.player.party[index];
        CharmlingData cData = gm.GetCharmlingData(charmling.ID);

        imageCharmling.sprite = charmling.GetSprite();
        textCharmlingLevel.text = $"Lv {charmling.level}";
        textCharmlingName.text = charmling.GetName();
        textCharmlingSpecies.text = charmling.GetSpeciesName();
        textCharmlingGender.text = charmling.PrintGender();
        textCharmlingAccessory.text = charmling.accessory == null ? "--" : charmling.accessory.GetName();

        for (int i = 0; i < 5; i++)
        {
            uiSkills[i].id = charmling.skills[i].ID;
            uiSkills[i].Refresh();
        }

        uiSkills[5].id = SkillID.NONE;
        uiSkills[5].Refresh();

        uiAttribute[0].id = cData.attr;
        uiAttribute[0].Refresh();

        if (cData.secAttr != AttributeID.NONE)
        {
            uiAttribute[1].id = cData.secAttr;
            uiAttribute[1].Refresh();
            uiAttribute[1].gameObject.SetActive(true);
        } else
        {
            uiAttribute[1].gameObject.SetActive(false);
        }
    }

    public void OnCharmlingSelect(int index, bool inBag)
    {
        if (!canInteract) return;

        if (isGrabbingCharmling)
        {
            Charmling fromCharmling = GetPrevSelectedCharmling();

            currSelected = index;
            currSelectedInBag = inBag;
            currSelectedObject = gm.inputHandler.es.currentSelectedGameObject;

            Charmling toCharmling = GetSelectedCharmling();


            if (toCharmling == null)
            {
                if (currSelectedInBag)
                {
                    if (prevSelectedInBag)
                    {
                        SaveSystem.loaded.player.bagCharmlings[currSelected + (uiBagIcons.Length * pageCurrent)] = fromCharmling;
                        SaveSystem.loaded.player.bagCharmlings[prevSelected + (uiBagIcons.Length * pagePrev)] = null;
                    } else
                    {
                        SaveSystem.loaded.player.bagCharmlings[currSelected + (uiBagIcons.Length * pageCurrent)] = fromCharmling;
                        SaveSystem.loaded.player.party.RemoveAt(prevSelected);
                    }
                } else
                {
                    if (prevSelectedInBag)
                    {
                        SaveSystem.loaded.player.party.Add(fromCharmling);
                        SaveSystem.loaded.player.bagCharmlings[prevSelected + (uiBagIcons.Length * pagePrev)] = null;
                    } else
                    {
                        SaveSystem.loaded.player.party.Add(fromCharmling);
                        SaveSystem.loaded.player.party.RemoveAt(prevSelected);
                    }
                    
                }
            } else
            {
                // currSelected = toCharmling
                // prevSelected = fromCharmling

                if (currSelectedInBag) // If it's going to the bag
                {
                    if (prevSelectedInBag) // If it IS coming from the bag
                    {
                        SaveSystem.loaded.player.bagCharmlings[currSelected + (uiBagIcons.Length * pageCurrent)] = fromCharmling;
                        SaveSystem.loaded.player.bagCharmlings[prevSelected + (uiBagIcons.Length * pagePrev)] = toCharmling;
                    }
                    else // If it's NOT coming from the bag
                    {
                        SaveSystem.loaded.player.bagCharmlings[currSelected + (uiBagIcons.Length * pageCurrent)] = fromCharmling;
                        SaveSystem.loaded.player.party[prevSelected] = toCharmling;
                    }
                }
                else // If it's going to the party
                {
                    if (!prevSelectedInBag) // If it's from the party
                    {
                        SaveSystem.loaded.player.party[prevSelected] = toCharmling;
                        SaveSystem.loaded.player.party[currSelected] = fromCharmling;
                    }
                    else // If it's from the bag
                    {
                        SaveSystem.loaded.player.bagCharmlings[prevSelected + (uiBagIcons.Length * pagePrev)] = toCharmling;
                        SaveSystem.loaded.player.party[currSelected] = fromCharmling;
                    }
                }
            }

            GlobalFreeze();
            Vector3 prevCharmlingPos = GetSelectedCharmlingIcon().imageCharmling.transform.position;

            GetSelectedCharmlingIcon().canvasCharmling.overrideSorting = true;
            GetSelectedCharmlingIcon().canvasCharmling.sortingLayerName = "UI";
            GetSelectedCharmlingIcon().canvasCharmling.sortingOrder = 6;

            LeanTween.move(GetSelectedCharmlingIcon().imageCharmling.gameObject, GetPrevSelectedCharmlingIcon().transform.position, 0.1f).setEaseOutSine();

            LeanTween.moveLocal(imageGrab.gameObject, new Vector3(0, 0), 0.05f);
            LeanTween.scale(imageGrab.gameObject, new Vector3(1.4f, 1.4f), 0.1f).setDelay(0.05f).setEaseOutSine();
            LeanTween.scale(imageGrab.gameObject, new Vector3(1f, 1f), 0.1f).setDelay(0.15f).setEaseInSine().setOnComplete(() =>
            {
                GetSelectedCharmlingIcon().canvasCharmling.overrideSorting = false;
                GetSelectedCharmlingIcon().imageCharmling.transform.position = prevCharmlingPos;

                isGrabbingCharmling = false;

                imageGrab.enabled = false;
                imageGrabShadow.enabled = false;

                RefreshBagIcons(false);
                RefreshPartyIcons();

                GlobalFreeze(false);
                canInteract = true;
                Resume(currSelectedObject);
            });


        } else if (isSwappingAccessory && !(inBag && SaveSystem.loaded.player.bagCharmlings[index + (uiBagIcons.Length * pageCurrent)] == null) && !(!inBag && index >= SaveSystem.loaded.player.party.Count))
        {
            Freeze();
            currSelected = index;
            currSelectedInBag = inBag;
            currSelectedObject = gm.inputHandler.es.currentSelectedGameObject;

            tempToAccessory = GetSelectedCharmling().accessory;

            GetSelectedCharmling().accessory = tempFromAccessory;
            GetPrevSelectedCharmling().accessory = tempToAccessory;

            UIMessage msg = gm.uiHandler.LoadMenu(UIVariables.UIMESSAGE).GetComponent<UIMessage>();
            msg.referrer = this;
            msg.AddMessage(string.Format(gm.GetText("charmbag_accessorySwapped"), GetSelectedCharmling().GetName(), tempFromAccessory.GetName()));
            msg.onClose += () =>
            {
                imageGrab.enabled = false;
                imageGrabShadow.enabled = false;
                RefreshBagIcons();
                RefreshPartyIcons();
                Resume(currSelectedObject);
            };
            msg.Refresh(true);
            isSwappingAccessory = false;

        } else if (!(inBag && SaveSystem.loaded.player.bagCharmlings[index + (uiBagIcons.Length * pageCurrent)] == null) && !(!inBag && index >= SaveSystem.loaded.player.party.Count))
        {
                currSelected = index;
            currSelectedInBag = inBag;

            currSelectedObject = gm.inputHandler.es.currentSelectedGameObject;

            UIChoices ch = gm.uiHandler.LoadMenu(UIVariables.UICHOICES).GetComponent<UIChoices>();

            Freeze();
            ch.AddChoice(0, "Move");
            ch.AddChoice(1, "View");
            ch.AddChoice(2, "Accessory");
            ch.AddChoice(3, "Release");
            ch.AddChoice(4, "Cancel");
            ch.onChoiceSelected += Choice_Select;

            RectTransform sRect = gm.inputHandler.es.currentSelectedGameObject.GetComponent<RectTransform>();
            Vector3[] c = new Vector3[4];
            sRect.GetWorldCorners(c);

            ch.SetPos(new Vector3(c[2].x + UIVariables.CHOICEWIDTHOFFSET, (c[2].y + c[3].y) * 0.5f));
            ch.Refresh(true);
        }

        prevSelected = currSelected;
        pagePrev = pageCurrent;
        prevSelectedInBag = currSelectedInBag;
        prevSelectedObject = currSelectedObject;
    }

    public void Choice_Select()
    {
        int selected = UIVariables.tempChoiceIndex;

        switch (selected)
        {
            case 0:
                MovingCharmling();
                break;
            case 1:
                Hide();

                UISummary summary = gm.uiHandler.LoadMenu(UIVariables.UISUMMARY).GetComponent<UISummary>();
                summary.index = currSelected;
                summary.inParty = !currSelectedInBag;
                summary.referrer = this;
                clearEvents = false;

                onHide += () =>
                {
                    summary.Refresh(true);
                };

                onShow += () =>
                {
                    clearEvents = true;
                    Resume(currSelectedObject);
                };
                break;
            case 2:
                ViewAccessory();
                break;
            case 3:
                string charmlingName = GetSelectedCharmling().GetName();
                UIMessage msg = gm.uiHandler.LoadMenu(UIVariables.UIMESSAGE).GetComponent<UIMessage>();
                
                if (!currSelectedInBag && SaveSystem.loaded.player.party.Count == 1)
                {
                    msg.AddMessage(gm.GetText("charmbag_releaseLastParty"));
                    msg.onClose += () => {
                        Resume(currSelectedObject);
                    };
                    msg.Refresh(true);
                } else
                {
                    msg.AddMessage(string.Format(gm.GetText("charmbag_release"), charmlingName));
                    msg.referrer = this;
                    msg.KeepOpen(true);

                    UIChoices choice = gm.uiHandler.LoadMenu(UIVariables.UICHOICES).GetComponent<UIChoices>();
                    choice.AddChoice(0, gm.GetText("sys_no"));
                    choice.AddChoice(1, gm.GetText("sys_yes"));
                    choice.referrer = msg;
                    choice.onChoiceSelected += Choice_ReleaseSelected;
                    choice.SetPos(UIVariables.posChoicesMsg);

                    msg.onMessageComplete += () =>
                    {
                        choice.Refresh(true);
                    };
                    msg.Refresh(true);
                }                
                
                break;
            case -1:
            case 4:
                Resume(currSelectedObject);
                break;
            default:
                break;
        }

    }

    public void Choice_ReleaseSelected()
    {
        int selected = UIVariables.tempChoiceIndex;

        switch (selected)
        {
            case -1:
            case 0:
                Resume(currSelectedObject);
                break;
            case 1:
                ReleaseCharmling();
                break;
            default:
                break;
        }
    }

    public void ReleaseCharmling()
    {
        string charmlingName = GetSelectedCharmling().GetName();

        if (currSelectedInBag)
        {
            SaveSystem.loaded.player.bagCharmlings[currSelected + (uiBagIcons.Length * pageCurrent)] = null;

        } else
        {
            SaveSystem.loaded.player.party.RemoveAt(currSelected);
        }

        gm.inputHandler.es.sendNavigationEvents = false;

        UIMessage msg = gm.uiHandler.LoadMenu(UIVariables.UIMESSAGE).GetComponent<UIMessage>();
        msg.referrer = this;
        msg.AddMessage(string.Format(gm.GetText("charmbag_releaseConfirmed"), charmlingName));
        msg.onClose += () =>
        {
            Refresh(false);
            imageGrab.enabled = false;
            imageGrabShadow.enabled = false;
            GetSelectedCharmlingIcon().imageCharmling.transform.localScale = Vector3.one;
            GetSelectedCharmlingIcon().imageAccessory.transform.localScale = Vector3.one;
            gm.inputHandler.es.sendNavigationEvents = true;
            Resume(currSelectedObject);
        };
        LeanTween.scale(GetSelectedCharmlingIcon().imageAccessory.gameObject, Vector3.zero, 0.2f);
        LeanTween.scale(GetSelectedCharmlingIcon().imageCharmling.gameObject, Vector3.zero, 0.2f).setOnComplete(() =>
        {
            msg.Refresh(true);
        });
    }

    public void ViewAccessory()
    {
        Freeze();
        
        UIChoices ch = gm.uiHandler.LoadMenu(UIVariables.UICHOICES).GetComponent<UIChoices>();
        msgAccessory = gm.uiHandler.LoadMenu(UIVariables.UIMESSAGE).GetComponent<UIMessage>();
        msgAccessory.referrer = this;
        msgAccessory.KeepOpen(true);
        ch.referrer = msgAccessory;

        bool hasItem = GetSelectedCharmling().accessory != null;

        if (!hasItem)
        {
            msgAccessory.AddMessage(string.Format(gm.GetText("charmbag_noItem"), GetSelectedCharmling().GetName()));
            // Add item?
        } else
        {
            msgAccessory.AddMessage(string.Format(gm.GetText("charmbag_accessory"), GetSelectedCharmling().GetName(), GetSelectedCharmling().accessory.GetName()));
        }

        if (hasItem) ch.AddChoice(0, gm.GetText("charmbag_move"));
        if (!hasItem) ch.AddChoice(3, gm.GetText("charmbag_give"));
        if (hasItem) ch.AddChoice(1, gm.GetText("charmbag_take"));
        ch.AddChoice(2, "Cancel");
        ch.SetPos(UIVariables.posChoicesMsg);
        ch.onChoiceSelected += Choice_SelectAccessory;
        ch.messageRef = msgAccessory;

        msgAccessory.onMessageComplete += () =>
        {
            msgAccessory.Freeze();
            ch.Refresh(true);
        };

        msgAccessory.Refresh(true);
    }

    public void Choice_SelectAccessory()
    {
        int selected = UIVariables.tempChoiceIndex;

        switch (selected)
        {
            case 0:
                tempFromAccessory = GetSelectedCharmling().accessory;
                msgAccessory.onClose += () =>
                {
                    MovingAccessory();
                    Resume(currSelectedObject);
                };
                msgAccessory.Close();
                

                break;
            case 1:
                msgAccessory.referrer = this;
                msgAccessory.ClearMessageQueueAndEvents();
                msgAccessory.onClose += () =>
                {
                    RefreshBagIcons(false);
                    RefreshPartyIcons();
                    Resume(currSelectedObject);
                };

                BagPockets tempPocket = gm.GetItemData(GetSelectedCharmling().accessory.ID).pocket;
                string tempName = GetSelectedCharmling().accessory.GetName();

                if (ItemUtil.ObtainItem(GetSelectedCharmling().accessory, 1, 0))
                {
                    GetSelectedCharmling().accessory = null;
                    msgAccessory.AddMessage(string.Format(gm.GetText("charmbag_accessoryStored"), tempName, gm.GetText($"inv_{tempPocket}")));
                } else
                {
                    msgAccessory.AddMessage(gm.GetText("charmbag_accessoryNoRoom"));
                }

                msgAccessory.ResetMessageQueue(true);
                break;
            case 3:
                clearEvents = false;
                tempRequestingItemIndex = -1;

                UIInventory inv = gm.uiHandler.LoadMenu(UIVariables.UIINVENTORY).GetComponent<UIInventory>();
                inv.referrer = this;
                inv.requestingItem = true;
                inv.onClose += () =>
                {
                    tempRequestingItemIndex = inv.prevSelectedItemIndex;
                    tempRequestingPocket = inv.activePocket;
                };

                onShow += () =>
                {
                    GiveAccessoryToCharmling();
                    onShow = null;
                };

                onHide += () =>
                {
                    inv.Refresh(true);
                    onHide = null;
                };

                msgAccessory.onClose += () =>
                {
                    Hide();
                    onClose = null;
                };

                msgAccessory.Close();
                break;
            case 2:
            case -1:
                msgAccessory.Close();
                Resume(currSelectedObject);
                break;
            default:
                break;
        }
    }

    public void GiveAccessoryToCharmling()
    {
        Debug.Log("Give Accessory BEGIN");

        if (tempRequestingItemIndex > -1)
        {
            GetSelectedCharmling().accessory = new Item(SaveSystem.loaded.player.GetInventoryList(tempRequestingPocket)[tempRequestingItemIndex].ID);
            ItemUtil.ConsumeItem(tempRequestingPocket, tempRequestingItemIndex, 1);
        }

        RefreshBagIcons(false);
        RefreshPartyIcons();

        Resume(currSelectedObject);
        Debug.Log("Give Accessory END");
    }

    public void MovingCharmling()
    {
        GlobalFreeze();

        isGrabbingCharmling = true;

        imageGrabShadow.transform.position = currSelectedObject.transform.position;
        imageGrab.transform.localPosition = Vector3.zero;

        imageGrab.sprite = GetSelectedCharmling().GetIcon();
        imageGrabShadow.sprite = GetSelectedCharmling().GetIcon();

        imageGrab.SetNativeSize();
        imageGrabShadow.SetNativeSize();

        GetSelectedCharmlingIcon().imageCharmling.color = new Color(1, 1, 1, 0f);

        imageGrab.enabled = true;
        imageGrabShadow.enabled = true;

        LeanTween.moveLocal(imageGrab.gameObject, new Vector3(0, 32), 0.05f).setOnComplete(()=>
        {
            canInteract = true;

            GlobalFreeze(false);
            Resume(currSelectedObject);
        });
    }

    public void MovingAccessory()
    {
        isSwappingAccessory = true;

        imageGrab.sprite = GetSelectedCharmlingAccessorySprite();
        imageGrabShadow.sprite = GetSelectedCharmlingAccessorySprite();

        imageGrab.SetNativeSize();
        imageGrabShadow.SetNativeSize();

        canInteract = true;

        imageGrab.enabled = true;
        imageGrabShadow.enabled = true;

        GetSelectedCharmlingIcon().imageAccessory.color = new Color(1, 1, 1, 0f);
    }

    public Charmling GetSelectedCharmling()
    {
        if (currSelectedInBag && SaveSystem.loaded.player.bagCharmlings[currSelected + (uiBagIcons.Length * pageCurrent)] == null)
        {
            return null;
        }
        else if (!currSelectedInBag && currSelected >= SaveSystem.loaded.player.party.Count)
        {
            return null;
        }
        else
        {
            return currSelectedInBag ? SaveSystem.loaded.player.bagCharmlings[currSelected + (uiBagIcons.Length * pageCurrent)] : SaveSystem.loaded.player.party[currSelected];
        }
    }

    public Charmling GetPrevSelectedCharmling()
    {
        if (prevSelectedInBag && SaveSystem.loaded.player.bagCharmlings[prevSelected + (uiBagIcons.Length * pagePrev)] == null)
        {
            return null;
        }
        else if (!prevSelectedInBag && prevSelected >= SaveSystem.loaded.player.party.Count)
        {
            return null;
        }
        else
        {
            return prevSelectedInBag ? SaveSystem.loaded.player.bagCharmlings[prevSelected + (uiBagIcons.Length * pagePrev)] : SaveSystem.loaded.player.party[prevSelected];
        }
    }

    public UICharmBagIcon GetSelectedCharmlingIcon()
    {
        return currSelectedInBag ? uiBagIcons[currSelected] : uiPartyIcons[currSelected];
    }

    public UICharmBagIcon GetPrevSelectedCharmlingIcon()
    {
        return prevSelectedInBag ? uiBagIcons[prevSelected] : uiPartyIcons[prevSelected];
    }

    public Sprite GetSelectedCharmlingAccessorySprite()
    {
        return GetSelectedCharmling().accessory.GetIcon();
    }

    // Editor tool
    [ContextMenu("Assign Charm Bag Navigation")]
    public void AssignNavigation()
    {
        for (int i = 0; i < uiBagIcons.Length; i++)
        {
            Navigation nav = uiBagIcons[i].button.navigation;
            nav.mode = Navigation.Mode.Explicit;

            if (i % 5 == 0)
            {
                nav.selectOnLeft = uiPartyIcons[i / 5].button;
            } else
            {
                nav.selectOnLeft = uiBagIcons[i - 1].button;
            }

            if (i % 5 == 4)
            {
                int newIndex = Mathf.FloorToInt(i / 4) - 1;
                nav.selectOnRight = uiPartyIcons[newIndex].button; 
            } else
            {
                nav.selectOnRight = uiBagIcons[i + 1].button;
            }

            if (i < 5)
            {
                nav.selectOnUp = uiBagIcons[i + 15].button;
            } else
            {
                nav.selectOnUp = uiBagIcons[i - 5].button;
            }

            if (i > 14)
            {
                nav.selectOnDown = uiBagIcons[i - 15].button;
            } else
            {
                nav.selectOnDown = uiBagIcons[i + 5].button;
            }

            uiBagIcons[i].button.navigation = nav;

            UnityEditor.EditorUtility.SetDirty(uiBagIcons[i].gameObject);
        }
    }
}
