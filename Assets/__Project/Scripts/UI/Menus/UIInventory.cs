using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using static GameManager;
using EnhancedUI;
using EnhancedUI.EnhancedScroller;
using System.Reflection;

public class UIInventory : UIBase
{
    [Header("UI")]
    public SuperTextMesh textPocket;
    public SuperTextMesh textCurrency;
    public SuperTextMesh textPage;
    public SuperTextMesh textDesc;

    public Image[] imageTabs;
    public Sprite spriteTabActive, spriteTabInactive;
    public UIInventoryPartyButton[] partySlots;

    // Left = 0, Right = 1
    public Image[] imagePages;

    [Header("Logic")]
    public int pageCurrent;
    public int pageTotal;
    public UIItemSlot[] itemSlots;

    //Scroller
    public int selectedIndex = 0;
    //public EnhancedScroller scroller;
    public UIItemSlot cellViewPrefab;
    //public bool selectionChanged;

    public UIDynamicScroller view;
    public int offset;
    public int selectedSlot;

    // Inventory
    public BagPockets activePocket;

    int prevSelected;
    public Action onUseItemInBattle;
    public bool usingItemInBattle;
    public bool usingItemOnCharmling;

    public Action onRequestSelected;
    public bool requestingItem;

    public int prevSelectedItemIndex;
    public int prevSelectedCharmlingIndex;

    bool usingItemEffect;

    float currentTime;
    bool isTimerRunning;


    public void Start()
    {
        //scroller.Delegate = this;
        //scroller.ReloadData();

        // Add SelectCharmling listener to the party slots on the left
        for (int i = 0; i < partySlots.Length; i++)
        {
            int ii = i; // temp variable because Unity doesn't like passing i to the listener
            partySlots[i].button.onClick.AddListener(() =>
            {
                SelectCharmling(ii);
            });
        }

        onClose += () =>
        {
			//TODO: fix to work with the new battle system
            if (gm.uiHandler.inBattle && !usingItemInBattle)
            {
                //BattleController.instance.containerAction.SetActive(true);
                gm.inputHandler.es.SetSelectedGameObject(null);
                //gm.inputHandler.es.SetSelectedGameObject(BattleController.instance.buttonAction[1].gameObject);
            }
        };
        
        Refresh(false);
    }

    private void Update()
    {
        if (isAnimating || !canInteract) return;

        // Close the menu
        if (gm.inputHandler.rPlayer.GetButtonDown("Start"))
        {
            Close();
        }

        // Separate Close check if the player is using an item on a Charmling
        if (gm.inputHandler.rPlayer.GetButtonDown("B"))
        {
            if (usingItemOnCharmling)
            {
                usingItemOnCharmling = false;
                Resume(itemSlots[prevSelected].gameObject);
            }
            else
            {
                Close();
            }
        }

        // Reposition the cursor
        // If EventSystem is active, use that. If not, get the selected cell in the scroller
        if (gm.inputHandler.es.currentSelectedGameObject == null)
        {
            //ReposCursor(GetSelectedCellView());
        } else
        {
            //ReposCursor();
        }

        ChangePockets();
        ScrollInventory();
    }

    public void ScrollInventory()
    {
        bool selectionChanged = false;

        // Add a slight delay to input checks
        if (isTimerRunning)
        {
            if (currentTime <= 0.05f)
            {
                currentTime += Time.deltaTime;
            }
            else
            {
                currentTime -= 0.05f;
                isTimerRunning = false;
            }
        }
        else
        {
            if (gm.inputHandler.rPlayer.GetAxisRaw("VerticalL") == 1)
            {
                view.MoveUp();
                selectedIndex = Mathf.Clamp(selectedIndex - 1, 0, SaveSystem.loaded.player.GetInventoryList(activePocket).Count - 1);
                selectionChanged = true;
                isTimerRunning = true;
            }
            else if (gm.inputHandler.rPlayer.GetAxisRaw("VerticalL") == -1)
            {
                view.MoveDown();
                selectedIndex = Mathf.Clamp(selectedIndex + 1, 0, SaveSystem.loaded.player.GetInventoryList(activePocket).Count - 1);
                selectionChanged = true;
                isTimerRunning = true;
            }
            else if (gm.inputHandler.rPlayer.GetAxisRaw("VerticalL") == 0)
            {
                // Reset timer if no input is detected
                currentTime = 0;
                isTimerRunning = false;
            }
        }

        // If the selection has changed, scroll to the next cell
        if (selectionChanged)
        {
            
            /*
            if (selectedIndex >= scroller.EndCellViewIndex)
            {
                scroller.JumpToDataIndex(selectedIndex, 1.0f, 1.0f, false);
            }
            else if (selectedIndex <= scroller.StartCellViewIndex)
            {
                scroller.JumpToDataIndex(selectedIndex, 0.0f, 0.0f, false);
            }

            
            for (int i = 0; i < SaveSystem.loaded.player.GetInventoryList(activePocket).Count; i++)
            {
                SaveSystem.loaded.player.GetInventoryList(activePocket)[i].isSelected = i == selectedIndex;
            }
            
            // Update the inventory description
            // Called in UIItemSlot.RefreshCellView
            scroller.RefreshActiveCellViews();
            */
        }
    }

    public void ChangePockets()
    {
        // Temporary Input.GetKeyDown calls
        if ((Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.Tab)) || gm.inputHandler.rPlayer.GetButtonDown("LB"))
        {
            int bagMax = (int)Enum.GetValues(typeof(BagPockets)).Cast<BagPockets>().Max();

            if (activePocket == 0)
            {
                activePocket = (BagPockets)bagMax;
            }
            else
            {
                activePocket--;
            }
            pageCurrent = 0;

            Refresh(false);
        }
        else if (Input.GetKeyDown(KeyCode.Tab) || gm.inputHandler.rPlayer.GetButtonDown("RB"))
        {
            int bagMax = (int)Enum.GetValues(typeof(BagPockets)).Cast<BagPockets>().Max();

            if (activePocket == (BagPockets)bagMax)
            {
                activePocket = 0;
            }
            else
            {
                activePocket++;
            }
            pageCurrent = 0;

            Refresh(false);
        }
    }

    public void Refresh(bool openAfter)
    {
        EnableChildren();

        //RefreshItems(!openAfter);
        RefreshPocket();
        RefreshCurrency();
        RefreshParty();


        if (openAfter)
        {
            onOpen += () =>
            {
                selectedIndex = 0;
                //selectionChanged = true;
                ScrollInventory();
                /*ReposCursor(GetSelectedCellView());*/

                //gm.inputHandler.EnableCursor(true);

                onOpen = null;
            };

            Open();
        }
    }

    public void RefreshParty()
    {
        for (int i = 0; i < partySlots.Length; i++)
        {
            partySlots[i].Refresh(SaveSystem.loaded.player.party.ElementAtOrDefault(i) == null ? null : SaveSystem.loaded.player.party[i], 0);
        }
    }

    public void RefreshPocket()
    {
        textPocket.text = gm.GetText("inv_" + activePocket.ToString());

        for (int i = 0; i < imageTabs.Length; i++)
        {
            bool isActive = i == (int)activePocket;

            imageTabs[i].sprite = isActive ? spriteTabActive : spriteTabInactive;
            imageTabs[i].rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, isActive ? 80 : 70);
        }
        view.count = SaveSystem.loaded.player.GetInventoryList(activePocket).Count;
        view.Refresh(SaveSystem.loaded.player.GetInventoryList(activePocket).Count);
    }

    public void RefreshItems(bool selectItem = true)
    {
        /*int slotLength = itemSlots.Length;
        int currCount = SaveSystem.loaded.player.GetInventoryList(activePocket).Count;
        
        pageTotal = (int)Mathf.Ceil(currCount / slotLength);

        for (int i = 0; i < slotLength; i++)
        {
            itemSlots[i].indexPocket = activePocket;
            //itemSlots[i].indexItem = i + (slotLength * pageCurrent);
            itemSlots[i].uiInventory = this;
            itemSlots[i].Refresh();
        }

        if (selectItem)
        {
            //gm.inputHandler.es.SetSelectedGameObject(scroller.GetCellViewAtDataIndex(0).gameObject);
            itemSlots[0].RefreshDesc();
            gm.inputHandler.EnableCursor(true);
            ReposCursor();
            usingItemOnCharmling = false;
        }*/
    }

    public void RefreshCurrency()
    {
        textCurrency.text = $"G {string.Format("{0:n0}", SaveSystem.loaded.player.money)}";
    }

    public void SelectItem(int index)
    {
        prevSelected = index;

        ItemData iD = null;

        prevSelectedItemIndex = index + (itemSlots.Length * pageCurrent);


        if (SaveSystem.loaded.player.GetInventoryList(activePocket).ElementAtOrDefault(index + (itemSlots.Length * pageCurrent)) != null)
        {
            iD = gm.GetItemData(SaveSystem.loaded.player.GetInventoryList(activePocket)[index + (itemSlots.Length * pageCurrent)].ID);
        }

        UIChoices ch = gm.uiHandler.LoadMenu(UIVariables.UICHOICES).GetComponent<UIChoices>();

        if (iD != null)
        {
            if (gm.uiHandler.inBattle && iD.funcBattle != ItemEffects_Battle.NONE)
            {

                ch.AddChoice(4, gm.GetText("sys_use"));
            } else if (requestingItem)
            {
                if (iD.pocket != BagPockets.KEYITEMS)
                {
                    ch.AddChoice(5, gm.GetText("sys_select"));
                }
            } else
            {
                if (iD.funcMenu == ItemEffects_Menu.HEALING || iD.funcMenu == ItemEffects_Menu.WP)
                {
                    ch.AddChoice(0, gm.GetText("sys_use"));
                }

                if (iD.funcMenu == ItemEffects_Menu.EQUIP)
                {
                    ch.AddChoice(1, gm.GetText("sys_equip"));
                }

                ch.AddChoice(2, gm.GetText("sys_toss"));
            }

            ch.AddChoice(3, gm.GetText("sys_cancel"));
        } else
        {
            Debug.Log("None iD");
            return;
        }

        ch.onChoiceSelected += Choice_Select;

        RectTransform sRect = itemSlots[prevSelected].GetComponent<RectTransform>();
        Vector3[] c = new Vector3[4];
        sRect.GetWorldCorners(c);

        ch.SetPos(new Vector3(c[2].x, (c[2].y + c[3].y)*0.5f));
        ch.Refresh(true);
        Freeze();
    }

    public void SelectCharmling(int index)
    {
        if (!canInteract) return;

        StartCoroutine(SelectCharmlingItemEffect(index));
    }

    public IEnumerator SelectCharmlingItemEffect(int index)
    {
        Freeze();
        UIVariables.itemEffectReturn = 0;
        UIVariables.tempCharmlingInventoryIndex = index;

        ItemEffects.DoMenuItemEffect(SaveSystem.loaded.player.GetInventoryList(activePocket)[prevSelectedItemIndex], this);
        string itemName = SaveSystem.loaded.player.GetInventoryList(activePocket)[prevSelectedItemIndex].GetName();

        while (UIVariables.itemEffectReturn == 0)
        {
            yield return new WaitForEndOfFrame();
        }

        UIMessage msg = gm.uiHandler.LoadMenu(UIVariables.UIMESSAGE).GetComponent<UIMessage>();
        msg.referrer = this;
        msg.deselectObject = false;

        switch (UIVariables.itemEffectReturn)
        {
            case UIVariables.ITEMEFFECT_ERROR:
                msg.AddMessage(gm.GetText("inv_error"));
                msg.onClose += () =>
                {
                    Resume(partySlots[index].gameObject);
                };
                msg.Refresh(true);

                break;
            case UIVariables.ITEMEFFECT_SUCCESS:

                if (ItemUtil.ConsumeItem(activePocket, prevSelectedItemIndex + (itemSlots.Length * pageCurrent), 1) == 2)
                {
                    msg.AddMessage(string.Format(gm.GetText("inv_usedLast"), itemName));
                    msg.onClose += () =>
                    {
                        RefreshItems();
                        gm.inputHandler.es.SetSelectedGameObject(null);
                        Freeze(false);
                    };
                    msg.Refresh(true);
                    yield break;
                }
                break;
            default:
                break;
        }

        RefreshItems(false);
        Resume(partySlots[index].gameObject);
    }

    public void Choice_Select()
    {
        int selected = UIVariables.tempChoiceIndex;

        switch (selected)
        {
            case 0:
                Resume(partySlots[0].gameObject);
                usingItemOnCharmling = true;

                break;
            case 2:
                gm.inputHandler.EnableCursor(false);
                UIQuantity q = gm.uiHandler.LoadMenu(UIVariables.UIQUANTITY).GetComponent<UIQuantity>();
                UIMessage msg = gm.uiHandler.LoadMenu(UIVariables.UIMESSAGE).GetComponent<UIMessage>();
                
                q.referrer = msg;
                q.max = SaveSystem.loaded.player.GetInventoryList(activePocket)[prevSelected].currQuantity;
                q.min = 0;
                q.SetPos(UIVariables.posQuantityMsg);
                q.onClose += () =>
                {
                    msg.Close();

                    if (UIVariables.tempQuantity > 0)
                    {
                        ItemUtil.ConsumeItem(activePocket, prevSelected + (itemSlots.Length * pageCurrent), UIVariables.tempQuantity);
                        RefreshItems();
                    }
                    Freeze(false);
                    Resume(itemSlots[prevSelected].gameObject);
                };

                msg.AddMessage("How many?");
                msg.referrer = this;
                msg.KeepOpen(true);
                msg.onMessageFinishReading += () =>
                {
                    msg.Freeze();
                    q.Refresh(true);
                };
                msg.Refresh(true);
                
                break;
            case -1:
            case 3:
                Resume(itemSlots[prevSelected].gameObject);
                break;
            case 4:
                usingItemInBattle = true;

                onClose += onUseItemInBattle;
                Close();
                break;
            case 5:
                onClose += onRequestSelected;
                Close();
                break;
            default:
                break;
        }
    }

    private void OnDestroy()
    {
        onUseItemInBattle = null;
    }
/*
    public GameObject GetSelectedCellView()
    {
        return scroller.GetCellViewAtDataIndex(selectedIndex).gameObject;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return SaveSystem.loaded.player.GetInventoryList(activePocket).Count;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 58f;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        UIItemSlot cellView = scroller.GetCellView(cellViewPrefab) as UIItemSlot;

        cellView.name = "Item: " + SaveSystem.loaded.player.GetInventoryList(activePocket)[dataIndex].ID.ToString() + $" [{dataIndex}]";
        
        cellView.indexPocket = activePocket;
        cellView.uiInventory = this;
        cellView.Refresh(dataIndex);

        return cellView;
    }
    */
}
