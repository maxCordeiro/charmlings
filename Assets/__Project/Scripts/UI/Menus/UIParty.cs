using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static GameManager;

public class UIParty : UIBase
{
    [Header("UI")]
    public SuperTextMesh[] textName;
    public SuperTextMesh[] textLevel;
    public SuperTextMesh[] textGender;
    public SuperTextMesh[] textHP;
    public SuperTextMesh[] textAccessory;

    public Image[] imageCharmling;
    public Image[] imageCharmlingBG;
    public Image[] imageHPBar;
    public Image[] imageHPBG;
    public Image[] imageAccessory;

    public Button[] buttonCharmlings;

    public bool inBattle;
    public int prevSelected;

    public Action onSendOutSelected;
    bool sendingOutCharmling;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < buttonCharmlings.Length; i++)
        {
            int ii = i;
            buttonCharmlings[i].onClick.AddListener(() =>
            {
                ButtonSelect(ii);
            });
        }

        onClose += () => {
			//TODO: fix to work with the new battle system
            if (gm.uiHandler.inBattle && !sendingOutCharmling)
            {
                //BattleController.instance.containerAction.SetActive(true);
                gm.inputHandler.es.SetSelectedGameObject(null);
                //gm.inputHandler.es.SetSelectedGameObject(BattleController.instance.buttonAction[2].gameObject);
            }
        };
    }

    private void OnDisable()
    {
        for (int i = 0; i < buttonCharmlings.Length; i++)
        {
            buttonCharmlings[i].onClick.RemoveAllListeners();
        }
    }

    public void Refresh(bool openAfter = false)
    {
        EnableChildren();

        for (int i = 0; i < buttonCharmlings.Length; i++)
        {
            bool hasCharmling = SaveSystem.loaded.player.party.ElementAtOrDefault(i) != null;

            textName[i].enabled = hasCharmling;
            textLevel[i].enabled = hasCharmling;
            textGender[i].enabled = hasCharmling;
            textHP[i].enabled = hasCharmling;
            textAccessory[i].enabled = hasCharmling;

            imageCharmling[i].enabled = hasCharmling;
            imageCharmlingBG[i].enabled = hasCharmling;
            imageHPBar[i].enabled = hasCharmling;
            imageHPBG[i].enabled = hasCharmling;
            imageAccessory[i].enabled = hasCharmling;

            buttonCharmlings[i].interactable = hasCharmling;

            if (hasCharmling)
            {
                Charmling c = SaveSystem.loaded.player.party[i];

                textName[i].text = c.GetName();
                textLevel[i].text = $"Lv {c.level}";
                textGender[i].text = $"{c.PrintGender()}";
                textHP[i].text = $"{c.currentHP}/{c.stats[(int)StatID.HEALTH]}";
                textAccessory[i].text = c.accessory == null ? "--" : $"{c.accessory.GetName()}";

                imageCharmling[i].sprite = c.GetIcon();
                imageHPBar[i].rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, ((float)c.currentHP / (float)c.stats[(int)StatID.HEALTH]) * 194);
            }
        }

        if (SaveSystem.loaded.player.party.Count > 0)
        {
            onOpen += () =>
            {
                gm.inputHandler.es.SetSelectedGameObject(buttonCharmlings[0].gameObject);
                gm.inputHandler.EnableCursor(true);
                ReposCursor();
            };
        }

        if (openAfter) Open();
    }

    // Update is called once per frame
    void Update()
    {
        if (!canInteract || isAnimating) return;

        if (gm.inputHandler.rPlayer.GetButtonDown("Start") || gm.inputHandler.rPlayer.GetButtonDown("B"))
        {
            Close();
        }

        ReposCursor();

        if (gm.inputHandler.es.currentSelectedGameObject == null)
        {
            //Resume(buttonCharmlings[prevSelected].gameObject);
        }
    }

    public void ButtonSelect(int index)
    {
        prevSelected = index;

        UIChoices ch = gm.uiHandler.LoadMenu(UIVariables.UICHOICES).GetComponent<UIChoices>();
        ch.AddChoice(0, "View");
        
        if (!inBattle)
        {
            ch.AddChoice(1, "Switch");
            ch.AddChoice(2, "Accessory");
            ch.AddChoice(3, "Send out");
        } else
        {
            ch.AddChoice(5, "Send out");
        }
        ch.AddChoice(4, "Cancel");
        ch.onChoiceSelected += Choice_Select;
        ch.Refresh(true);
        Freeze();
    }

    public void Choice_Select()
    {
        int selected = UIVariables.tempChoiceIndex;

        switch (selected)
        {
            case -1:
                Resume(buttonCharmlings[prevSelected].gameObject);
                break;
            case 0:
                Hide();

                UISummary summary = gm.uiHandler.LoadMenu(UIVariables.UISUMMARY).GetComponent<UISummary>();
                clearEvents = false;

                onHide += () =>
                {
                    summary.index = prevSelected;
                    summary.inParty = true;
                    summary.referrer = this;
                    summary.Refresh(true);
                };

                onShow += () =>
                {
                    clearEvents = true;
                    Resume(buttonCharmlings[prevSelected].gameObject);
                };
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                gm.inputHandler.es.sendNavigationEvents = true;
                Resume(buttonCharmlings[prevSelected].gameObject);
                break;
            case 5:
                sendingOutCharmling = true;

                onClose += onSendOutSelected;
                Close();
                break;
            default:
                break;
        }
    }

    private void OnDestroy()
    {
        onSendOutSelected = null;
    }
}
