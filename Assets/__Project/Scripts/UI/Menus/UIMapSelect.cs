using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMapSelect : MonoBehaviour
{
    [Header("UI")]
    // Buttons
    public Button[] buttonMaps;
    public SuperTextMesh[] textMapButtons;

    public SuperTextMesh textDungeons;

    public Image imageMapPreview;

    // Page Indicator
    public SuperTextMesh textPage;
    public Image imagePageL, imagePageR;

    // Panel Info

    [Header("UI - Panel Info")]
    public SuperTextMesh textMapName;
    public SuperTextMesh textMapDesc;

    public Image[] imageTabHighlight;
    public Image[] imageTabIcon;

    // Sub Panel Notes
    [Header("UI - Sub Panel Charmlings")]
    public Image[] imageCharmlings;
    public Image[] imageCharmlingsIcon;


    [Header("Panel Objects")]
    public GameObject panelButtons;
    public GameObject panelInfo;

    public GameObject subPanelNotes;
    public GameObject subPanelCharmlings;
    public GameObject subPanelBosses;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
