using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static GameManager;

public class UIChoices : UIBase
{
    public GameObject container;
    public GameObject choiceButton;

    public bool hideCursor, setRef, closeMessage;

    public List<GameObject> choiceObjs;

    public event Action onChoiceSelected;
    public void OnChoiceSelected() => onChoiceSelected?.Invoke();

    public UIMessage messageRef;

    // Start is called before the first frame update
    void Start()
    {
        onClose += () =>
        {
            OnChoiceSelected();
            if (hideCursor) gm.inputHandler.EnableCursor(false);
            if (messageRef != null) messageRef.canInteract = true;
        };
    }

    // Update is called once per frame
    void Update()
    {
        if (isAnimating || !canInteract) return;

        if (gm.inputHandler.rPlayer.GetButtonDown("B"))
        {
            ButtonClick(-1);
        }

        ReposCursor();
    }

    public void HideCursor() => hideCursor = true;

    public void Refresh(bool openAfter = false)
    {
        gm.inputHandler.EnableCursor(false);
        EnableChildren();

        if (messageRef != null) messageRef.canInteract = false;

        for (int i = 0; i < choiceObjs.Count; i++)
        {
            Button b = choiceObjs[i].GetComponent<Button>();
            Navigation n = new Navigation
            {
                mode = Navigation.Mode.Explicit
            };

            if (choiceObjs.Count > 1)
            {
                if (i == 0)
                {
                    n.selectOnDown = choiceObjs[i + 1].GetComponent<Button>();
                    n.selectOnUp = choiceObjs[choiceObjs.Count - 1].GetComponent<Button>();
                }
                else if (i == choiceObjs.Count - 1)
                {
                    n.selectOnUp = choiceObjs[i - 1].GetComponent<Button>();
                    n.selectOnDown = choiceObjs[0].GetComponent<Button>();
                }
                else
                {
                    n.selectOnUp = choiceObjs[i - 1].GetComponent<Button>();
                    n.selectOnDown = choiceObjs[i + 1].GetComponent<Button>();
                }
            }

            b.navigation = n;
        }

        UIVariables.tempChoiceIndex = -100;
        transform.SetAsLastSibling();

        if (openAfter)
        {
            onOpen += () =>
            {
                gm.inputHandler.es.SetSelectedGameObject(choiceObjs[0]);
                gm.inputHandler.EnableCursor(true);
                ReposCursor();

                onOpen = null;
            };

            Open();
        }
    }

    public void SetPos(Vector3 _pos)
    {
        RectTransform rt = transform.GetChild(0).GetComponent<RectTransform>();

        rt.position = new Vector3(_pos.x, _pos.y, rt.position.z);
    }

    public void AddChoice(int _choiceIndex, string _text, bool _closeAfter = true)
    {
        GameObject tempButton = Instantiate(choiceButton, container.transform);
        tempButton.name = "Button: " + _text;
        tempButton.transform.GetChild(0).GetComponent<SuperTextMesh>().text = _text;

        Button b = tempButton.GetComponent<Button>();
        b.onClick.AddListener(() => { ButtonClick(_choiceIndex); });

        choiceObjs.Add(tempButton);
    }

    public void ButtonClick(int _index, bool _closeAfter = true)
    {
        UIVariables.tempChoiceIndex = _index;
        
        if (_closeAfter) Close();
    }

    public void CloseMessage()
    {
        foreach (GameObject o in choiceObjs)
        {
            o.GetComponent<Button>().onClick.AddListener(() =>
            {
                messageRef.Close();
            });
        }
    }

    private void OnDestroy()
    {
        foreach (GameObject o in choiceObjs)
        {
            o.GetComponent<Button>().onClick.RemoveAllListeners();
        }

        onChoiceSelected = null;
    }
}
