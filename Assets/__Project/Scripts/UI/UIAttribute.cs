using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAttribute : MonoBehaviour
{
    public AttributeID id;

    public Image imageIcon;
    public Image imageBG;
    public SuperTextMesh textAttribute;

    public Sprite[] spriteWindow;

    public void Refresh()
    {
        imageIcon.sprite = Resources.LoadAll<Sprite>("attributes_icons")[(int)id];
        imageBG.sprite = spriteWindow[(int)id];

        textAttribute.text = GameManager.gm.GetText("attr_" + id.ToString());
        textAttribute.color = UIVariables.attributeTextColors[id];
    }
}
