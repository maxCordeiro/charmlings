using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static GameManager;

public class UIItemIcon : MonoBehaviour
{
    [Header("UI")]
    public Image imageItem;
    public Image imageDurBG;
    public Image imageDurBar;

    public void Refresh(Item item)
    {
        imageItem.enabled = item != null;
        imageDurBG.enabled = item != null;
        imageDurBar.enabled = item != null;
        
        if (item == null) return;
        
        ItemData itemData = gm.GetItemData(item.ID);

        imageItem.sprite = item.GetIcon();

        bool hasDurability = item.currUses != -1;

        imageDurBG.enabled = hasDurability;
        imageDurBar.enabled = hasDurability;

        imageDurBar.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, ((float)item.currUses / (float)itemData.uses) * 48);
    }

}
