using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPageIndicator : MonoBehaviour
{
    [Header("UI")]
    public Color color;

    public SuperTextMesh textPage;
    public Image arrowLeft, arrowRight;

    [Header("Code")]
    public int pageCurrent;
    public int pageTotal;

    private void Awake()
    {
        textPage = GetComponent<SuperTextMesh>();
        arrowLeft = transform.GetChild(0).GetComponent<Image>();
        arrowRight = transform.GetChild(1).GetComponent<Image>();
    }

    public void Refresh(int _pageCurrent, int _pageTotal)
    {
        pageCurrent = _pageCurrent;
        pageTotal = _pageTotal;

        textPage.text = string.Format("{0}/{1}", pageCurrent, pageTotal);
        textPage.color = color;
           
        arrowRight.color = new Color(color.r, color.g, color.b, pageCurrent == pageTotal? 0.5f : 1f);
        arrowLeft.color = new Color(color.r, color.g, color.b, pageCurrent == 0? 0.5f : 1f);
    }
}
