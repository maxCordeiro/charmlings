using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UIVariables
{
    public static int tempChoiceIndex;
    public static int tempCharmlingInventoryIndex;
    public static int tempQuantity;

    /* -1 = Cancel
     * 0 = Nothing
     * 1 = Success
     * 2 = Error
     */
    public static int itemEffectReturn;

    public static Dictionary<AttributeID, Color> attributeTextColors = new Dictionary<AttributeID, Color>()
    {
        {AttributeID.WATER, new Color(0.0549f, 0.2509f, 0.3490f)},
        {AttributeID.PLANT, new Color(0.1411f, 0.4274f, 0.0823f)},
        {AttributeID.FIRE, new Color(0.4588f, 0.1019f, 0.0901f)},
        {AttributeID.COSMIC, new Color(0.4039f, 0.1176f, 0.5333f)},
        {AttributeID.ELECTRIC, new Color(0.6745f, 0.3568f, 0.066f)},
        {AttributeID.WIND, new Color(0.3764f, 0.5058f, 0.5215f)},
        {AttributeID.SHADOW, new Color(0.1843f, 0.1607f, 0.2509f)},
        {AttributeID.TECH, new Color(0.2078f, 0.2627f, 0.3137f)},
        {AttributeID.EARTH, new Color(0.4823f, 0.2352f, 0.1568f)},
        {AttributeID.NORMAL, new Color(0.4078f, 0.2980f, 0.2901f)}
    };

    public static Dictionary<StatusID, Color> statusColors = new Dictionary<StatusID, Color>()
    {
        {StatusID.NONE, Color.black },
        {StatusID.BURN, new Color(0.32941f, 0.0156f, 0.0980f) },
        {StatusID.DROWSY, new Color(0.3529f, 0.2f, 0.3822f) },
        {StatusID.CURSE, new Color(0.1058f, 0.4f, 0.4666f) },
        {StatusID.TRAP, new Color(0.0862f, 0.1411f, 0.2784f) }
    };

    public const int ITEMEFFECT_CANCEL = -1;
    public const int ITEMEFFECT_NOTHING = 0;
    public const int ITEMEFFECT_SUCCESS = 1;
    public const int ITEMEFFECT_ERROR = 2;

    public const string UICHOICES = "UIChoices";
    public const string UIMESSAGE = "UIMessage";
    public const string UIQUANTITY = "UIQuantity";
    public const string UIINVENTORY = "UIInventory";
    public const string UINURSERY = "UINursery";
    public const string UIPARTY = "UIParty";
    public const string UIPAUSE = "UIPause";
    public const string UISHOP = "UIShop";
    public const string UISUMMARY = "UISummary";
    public const string UICHARMBAG = "UICharmBag";

    public static readonly Vector2 posChoicesMsg = new Vector2(4.6f, -2.5f);
    public static readonly Vector2 posQuantityMsg = new Vector2(0f, -3.25f);

    public const float CHOICEWIDTHOFFSET = 4.75f;
}