﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperTiled2Unity;
using SuperTiled2Unity.Editor;
using UnityEngine;
using UnityEditor;

[AutoCustomTmxImporter]
public class CharmlingsMapImporter : CustomTmxImporter
{
    public readonly Vector3 offsetNPC = new Vector3(0.5f, -1);

    public override void TmxAssetImported(TmxAssetImportedArgs args)
    {
        var map = args.ImportedSuperMap;

        ImportNPCs(args);
    }

    public void ImportNPCs(TmxAssetImportedArgs args)
    {
        var map = args.ImportedSuperMap;
        var npcs = map.GetComponentsInChildren<SuperObject>().Where(o => o.m_Type == "NPC");

        foreach (var npc in npcs)
        {
            var tempNPC = Resources.Load($"NPC/NPC{npc.m_TiledName}");
            
            if (tempNPC == null)
            {
                Debug.LogError($"NPC{npc.m_TiledName} doesn't exist!");
                continue;
            }

            GameObject npcObj = PrefabUtility.InstantiatePrefab(tempNPC as GameObject) as GameObject;
            npcObj.transform.SetParent(npc.transform);
            npcObj.transform.position = npc.transform.position;
            npcObj.transform.position += offsetNPC;
        }
    }
}